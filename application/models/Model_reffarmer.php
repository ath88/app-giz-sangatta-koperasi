
<?php
class Model_reffarmer extends Model_Master
{
    protected $table = 'ref_farmer';


    public function __construct()
    {
        parent::__construct();
    }
    function all()
    {
        $this->db->select($this->table . '.*,lr.NAMA_WILAYAH lrWIL,dm.NAMA_WILAYAH dmWIL,');
        $this->db->from($this->table);
        $this->db->join('ref_wilayah lr', 'farmerCityBirth = lr.ID_WILAYAH', 'LEFT');
        $this->db->join('ref_wilayah dm', 'farmerCityAddress = dm.ID_WILAYAH', 'LEFT');
        $qr = $this->db->get();
        if ($qr->num_rows() > 0)
            return $qr->result();
        else
            return false;
    }

    function by_id($id)
    {
        $this->db->select($this->table . '.*,lr.NAMA_WILAYAH,dm.NAMA_WILAYAH,');
        $this->db->from($this->table);
        $this->db->join('ref_wilayah lr', 'farmerCityBirth = lr.ID_WILAYAH', 'LEFT');
        $this->db->join('ref_wilayah dm', 'farmerCityAddress = dm.ID_WILAYAH', 'LEFT');
        $this->db->where($id);
        $qr = $this->db->get();
        if ($qr->num_rows() == 1)
            return $qr->row();
        else
            return false;
    }
}
