
<?php
class Model_dfarmerparcel extends Model_Master
{
    protected $table = 'd_farmer_parcel';


    public function __construct()
    {
        parent::__construct();
    }
    function all()
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('ref_wilayah', 'parcelVillage = ID_WILAYAH', 'LEFT');
        $this->db->join('ref_landstatus', 'parcelLsId = lsId', 'LEFT');
        $this->db->join('ref_landbeforeplanting', 'parcelLbpId = lbpId', 'LEFT');
        $this->db->join('ref_species', 'parcelSpecId = specId', 'LEFT');
        $this->db->join('ref_cropping', 'parcelCropId = cropId', 'LEFT');
        $this->db->join('ref_soiltype', 'parcelStId = stId', 'LEFT');
        $this->db->join('ref_farmer', 'parcelFarmerId = farmerId', 'LEFT');
        $qr = $this->db->get();
        if ($qr->num_rows() > 0)
            return $qr->result();
        else
            return false;
    }

    function by_id($id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('ref_wilayah', 'parcelVillage = ID_WILAYAH', 'LEFT');
        $this->db->join('ref_landstatus', 'parcelLsId = lsId', 'LEFT');
        $this->db->join('ref_landbeforeplanting', 'parcelLbpId = lbpId', 'LEFT');
        $this->db->join('ref_species', 'parcelSpecId = specId', 'LEFT');
        $this->db->join('ref_cropping', 'parcelCropId = cropId', 'LEFT');
        $this->db->join('ref_soiltype', 'parcelStId = stId', 'LEFT');
        $this->db->join('ref_farmer', 'parcelFarmerId = farmerId', 'LEFT');
        $this->db->where($id);
        $qr = $this->db->get();
        if ($qr->num_rows() == 1)
            return $qr->row();
        else
            return false;
    }
}
