
<?php
class Model_dharvestsale extends Model_Master
{
    protected $table = 'd_harvest_sale';


    public function __construct()
    {
        parent::__construct();
    }
    function all()
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('d_harvest', 'hsaleHarvestId = harvestId', 'LEFT');
        $this->db->join('d_farmer_parcel', 'parcelId = harvestParcelId', 'LEFT');
        $this->db->join('ref_farmer', 'farmerId = parcelFarmerId', 'LEFT');
        $this->db->join('ref_truck', 'hsaleTruckId = truckId', 'LEFT');
        $this->db->join('d_contract', 'hsaleContId = contId', 'LEFT');
        $this->db->join('ref_mill', 'millId = contMillId', 'LEFT');
        $qr = $this->db->get();
        if ($qr->num_rows() > 0)
            return $qr->result();
        else
            return false;
    }

    function by_id($id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('d_harvest', 'hsaleHarvestId = harvestId', 'LEFT');
        $this->db->join('d_farmer_parcel', 'parcelId = harvestParcelId', 'LEFT');
        $this->db->join('ref_farmer', 'farmerId = parcelFarmerId', 'LEFT');
        $this->db->join('ref_truck', 'hsaleTruckId = truckId', 'LEFT');
        $this->db->join('d_contract', 'hsaleContId = contId', 'LEFT');
        $this->db->join('ref_mill', 'millId = contMillId', 'LEFT');
        $this->db->where($id);
        $qr = $this->db->get();
        if ($qr->num_rows() == 1)
            return $qr->row();
        else
            return false;
    }
}
