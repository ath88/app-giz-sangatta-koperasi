
<?php
class Model_dsaprodisale extends Model_Master
{
    protected $table = 'd_saprodisale';

    public function __construct()
    {
        parent::__construct();
    }

    function all($where)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('ref_saprodi_harga', 'saprodisHsaprodiId = hsaprodiId', 'LEFT');
        $this->db->join('ref_saprodi_jenis', 'hsaprodiJId = jsaprodiId', 'LEFT');
        $this->db->where($where);
        $qr = $this->db->get();
        if ($qr->num_rows() > 0)
            return $qr->result();
        else
            return false;
    }

    function by_id($where)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('ref_saprodi_harga', 'saprodisHsaprodiId = hsaprodiId', 'LEFT');
        $this->db->join('ref_saprodi_jenis', 'hsaprodiJId = jsaprodiId', 'LEFT');
        $this->db->where($where);
        $qr = $this->db->get();
        if ($qr->num_rows() == 1)
            return $qr->row();
        else
            return false;
    }
}
