
<?php
class Model_dcontract extends Model_Master
{
    protected $table = 'd_contract';


    public function __construct()
    {
        parent::__construct();
    }
    function all()
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('ref_mill', 'contMillId = millId', 'LEFT');
        $this->db->join('ref_price', 'contPriceId = priceId', 'LEFT');
        $qr = $this->db->get();
        if ($qr->num_rows() > 0)
            return $qr->result();
        else
            return false;
    }

    function by_id($id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('ref_mill', 'contMillId = millId', 'LEFT');
        $this->db->join('ref_price', 'contPriceId = priceId', 'LEFT');
        $this->db->where($id);
        $qr = $this->db->get();
        if ($qr->num_rows() == 1)
            return $qr->row();
        else
            return false;
    }
}
