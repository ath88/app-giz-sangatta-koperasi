
<?php
class Model_lapkeu extends Model_Master
{
    protected $table = 'rw_jabatan';

    public function __construct()
    {
        parent::__construct();
    }

    function get_saldo_awal($id, $year, $month)
    {
        $date = date('Y-m-d', strtotime($year . '-' . $month . '-01'));
        $qr = $this->db->query("
                        SELECT DK,SUM(CASE WHEN DK = 'D' THEN (tsaleB*-1) ELSE tsaleB END) sumGroupSale
                        FROM (
                            SELECT 'Penjualan Panen Bruto' Ket,'K' AS 'DK',tsaleDate,tsaleBruto tsaleB
                            FROM d_transportsale
                            LEFT JOIN d_harvest ON tsaleHarvestId = harvestId
                            LEFT JOIN d_farmer_parcel ON parcelId = harvestParcelId
                            LEFT JOIN ref_farmer ON farmerId = parcelFarmerId
                            WHERE tsaleDate < '" . $date . "' AND farmerId = '$id'
                            UNION
                            SELECT 'Potongan 0.25%' Ket,'D' AS 'DK',tsaleDate,tsalePot025 tsaleB
                            FROM d_transportsale
                            LEFT JOIN d_harvest ON tsaleHarvestId = harvestId
                            LEFT JOIN d_farmer_parcel ON parcelId = harvestParcelId
                            LEFT JOIN ref_farmer ON farmerId = parcelFarmerId
                            WHERE tsaleDate < '" . $date . "' AND farmerId = '$id'
                            UNION
                            SELECT 'Potongan Transport' Ket,'D' AS 'DK',tsaleDate,tsalePotTranport tsaleB
                            FROM d_transportsale
                            LEFT JOIN d_harvest ON tsaleHarvestId = harvestId
                            LEFT JOIN d_farmer_parcel ON parcelId = harvestParcelId
                            LEFT JOIN ref_farmer ON farmerId = parcelFarmerId
                            WHERE tsaleDate < '" . $date . "' AND farmerId = '$id'
                            UNION
                            SELECT 'Potongan Koperasi' Ket,'D' AS 'DK',tsaleDate,tsalePotKoperasi tsaleB
                            FROM d_transportsale
                            LEFT JOIN d_harvest ON tsaleHarvestId = harvestId
                            LEFT JOIN d_farmer_parcel ON parcelId = harvestParcelId
                            LEFT JOIN ref_farmer ON farmerId = parcelFarmerId
                            WHERE tsaleDate < '" . $date . "' AND farmerId = '$id'
                            UNION
                            SELECT 'Potongan Administrasi' Ket,'D' AS 'DK',tsaleDate,tsalePotAdministrasi tsaleB
                            FROM d_transportsale
                            LEFT JOIN d_harvest ON tsaleHarvestId = harvestId
                            LEFT JOIN d_farmer_parcel ON parcelId = harvestParcelId
                            LEFT JOIN ref_farmer ON farmerId = parcelFarmerId
                            WHERE tsaleDate < '" . $date . "' AND farmerId = '$id'
                            UNION
                            SELECT 'Potongan Timbang' Ket,'D' AS 'DK',tsaleDate,tsalePotTimbang tsaleB
                            FROM d_transportsale
                            LEFT JOIN d_harvest ON tsaleHarvestId = harvestId
                            LEFT JOIN d_farmer_parcel ON parcelId = harvestParcelId
                            LEFT JOIN ref_farmer ON farmerId = parcelFarmerId
                            WHERE tsaleDate < '" . $date . "' AND farmerId = '$id'
                            UNION
                            SELECT 'Potongan Zakat' Ket,'D' AS 'DK',tsaleDate,tsaleZakat tsaleB
                            FROM d_transportsale
                            LEFT JOIN d_harvest ON tsaleHarvestId = harvestId
                            LEFT JOIN d_farmer_parcel ON parcelId = harvestParcelId
                            LEFT JOIN ref_farmer ON farmerId = parcelFarmerId
                            WHERE tsaleDate < '" . $date . "' AND farmerId = '$id'
                            UNION
                            SELECT ftName AS Ket,finDK AS 'DK',finDate tsaleDate,finAmount tsaleB FROM
                            d_finance
                            LEFT JOIN ref_finance_type ON finFtId = ftId
                            LEFT JOIN ref_farmer ON farmerId = finFarmerId
                            WHERE finDate < '" . $date . "' AND farmerId = '$id'
                        ) datas 
                        GROUP BY DK
						");

        if ($qr->num_rows() > 0)
            return $qr->result();
        else
            return false;
    }

    function all($id, $year, $month)
    {
        $qr = $this->db->query("
                        SELECT Ket,DK,tsaleDate,SUM(tsaleB) sumTsale
                        FROM (
                            SELECT 'Penjualan Panen Bruto' Ket,'K' AS 'DK',tsaleDate,tsaleBruto tsaleB
                            FROM d_transportsale
                            LEFT JOIN d_harvest ON tsaleHarvestId = harvestId
                            LEFT JOIN d_farmer_parcel ON parcelId = harvestParcelId
                            LEFT JOIN ref_farmer ON farmerId = parcelFarmerId
                            WHERE YEAR(tsaleDate) = '$year' AND MONTH(tsaleDate) = '$month' AND farmerId = '$id'
                            UNION
                            SELECT 'Potongan 0.25%' Ket,'D' AS 'DK',tsaleDate,tsalePot025 tsaleB
                            FROM d_transportsale
                            LEFT JOIN d_harvest ON tsaleHarvestId = harvestId
                            LEFT JOIN d_farmer_parcel ON parcelId = harvestParcelId
                            LEFT JOIN ref_farmer ON farmerId = parcelFarmerId
                            WHERE YEAR(tsaleDate) = '$year' AND MONTH(tsaleDate) = '$month' AND farmerId = '$id'
                            UNION
                            SELECT 'Potongan Transport' Ket,'D' AS 'DK',tsaleDate,tsalePotTranport tsaleB
                            FROM d_transportsale
                            LEFT JOIN d_harvest ON tsaleHarvestId = harvestId
                            LEFT JOIN d_farmer_parcel ON parcelId = harvestParcelId
                            LEFT JOIN ref_farmer ON farmerId = parcelFarmerId
                            WHERE YEAR(tsaleDate) = '$year' AND MONTH(tsaleDate) = '$month' AND farmerId = '$id'
                            UNION
                            SELECT 'Potongan Koperasi' Ket,'D' AS 'DK',tsaleDate,tsalePotKoperasi tsaleB
                            FROM d_transportsale
                            LEFT JOIN d_harvest ON tsaleHarvestId = harvestId
                            LEFT JOIN d_farmer_parcel ON parcelId = harvestParcelId
                            LEFT JOIN ref_farmer ON farmerId = parcelFarmerId
                            WHERE YEAR(tsaleDate) = '$year' AND MONTH(tsaleDate) = '$month' AND farmerId = '$id'
                            UNION
                            SELECT 'Potongan Administrasi' Ket,'D' AS 'DK',tsaleDate,tsalePotAdministrasi tsaleB
                            FROM d_transportsale
                            LEFT JOIN d_harvest ON tsaleHarvestId = harvestId
                            LEFT JOIN d_farmer_parcel ON parcelId = harvestParcelId
                            LEFT JOIN ref_farmer ON farmerId = parcelFarmerId
                            WHERE YEAR(tsaleDate) = '$year' AND MONTH(tsaleDate) = '$month' AND farmerId = '$id'
                            UNION
                            SELECT 'Potongan Timbang' Ket,'D' AS 'DK',tsaleDate,tsalePotTimbang tsaleB
                            FROM d_transportsale
                            LEFT JOIN d_harvest ON tsaleHarvestId = harvestId
                            LEFT JOIN d_farmer_parcel ON parcelId = harvestParcelId
                            LEFT JOIN ref_farmer ON farmerId = parcelFarmerId
                            WHERE YEAR(tsaleDate) = '$year' AND MONTH(tsaleDate) = '$month' AND farmerId = '$id'
                            UNION
                            SELECT 'Potongan Zakat' Ket,'D' AS 'DK',tsaleDate,tsaleZakat tsaleB
                            FROM d_transportsale
                            LEFT JOIN d_harvest ON tsaleHarvestId = harvestId
                            LEFT JOIN d_farmer_parcel ON parcelId = harvestParcelId
                            LEFT JOIN ref_farmer ON farmerId = parcelFarmerId
                            WHERE YEAR(tsaleDate) = '$year' AND MONTH(tsaleDate) = '$month' AND farmerId = '$id'
                            UNION
                            SELECT ftName AS Ket,finDK AS 'DK',finDate tsaleDate,finAmount tsaleB FROM
                            d_finance
                            LEFT JOIN ref_finance_type ON finFtId = ftId
                            LEFT JOIN ref_farmer ON farmerId = finFarmerId
                            WHERE YEAR(finDate) = '$year' AND MONTH(finDate) = '$month' AND farmerId = '$id'
                        ) datas 
                        GROUP BY Ket, DK, tsaleDate
                        ORDER BY tsaleDate, DK DESC, Ket
						");

        if ($qr->num_rows() > 0)
            return $qr->result();
        else
            return false;
    }

    function row_datas($id, $year, $month)
    {
        $qr = $this->db->query("
                        SELECT * FROM d_transportsale
                        LEFT JOIN d_harvest ON tsaleHarvestId = harvestId
                        LEFT JOIN d_farmer_parcel ON parcelId = harvestParcelId
                        LEFT JOIN ref_farmer ON farmerId = parcelFarmerId
                        WHERE YEAR(tsaleDate) = '$year' AND MONTH(tsaleDate) = '$month' AND farmerId = '$id'
                        ORDER BY tsaleDate
        ");

        if ($qr->num_rows() > 0)
            return $qr->result();
        else
            return false;
    }

    function row_sum($id, $year, $month)
    {
        $qr = $this->db->query("
                        SELECT 
                        SUM(tsalePotAdministrasi) sumAdm,
                        SUM(tsalePotTimbang) sumTimbang, 
                        SUM(tsaleZakat) sumZakat
                        FROM d_transportsale
                        LEFT JOIN d_harvest ON tsaleHarvestId = harvestId
                        LEFT JOIN d_farmer_parcel ON parcelId = harvestParcelId
                        LEFT JOIN ref_farmer ON farmerId = parcelFarmerId
                        WHERE YEAR(tsaleDate) = '$year' AND MONTH(tsaleDate) = '$month' AND farmerId = '$id'
                        ORDER BY tsaleDate
        ");

        if ($qr->num_rows() > 0)
            return $qr->row();
        else
            return false;
    }

    function row_finance($id, $year, $month)
    {
        $qr = $this->db->query("
                    SELECT ftName AS Ket,finDK AS 'DK',SUM(finAmount) tsaleB 
                    FROM d_finance
                    LEFT JOIN ref_finance_type ON finFtId = ftId
                    LEFT JOIN ref_farmer ON farmerId = finFarmerId
                    WHERE YEAR(finDate) = '$year' AND MONTH(finDate) = '$month' AND farmerId = '$id' AND finDK = 'K'
                    GROUP BY ftId,finDK
        ");

        if ($qr->num_rows() > 0)
            return $qr->result();
        else
            return false;
    }
}
