
            <?php
            class Model_dfinance extends Model_Master
            {
                    protected $table = 'd_finance';


                public function __construct()
                {
                    parent::__construct();
                }       
                    function all()
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('ref_finance_type','finFtId = ftId','LEFT');
        $this->db->join('ref_farmer','finFarmerId = farmerId','LEFT');
        $this->db->join('ref_loan_type','finLtId = ltId','LEFT');
        $this->db->join('ref_saving_type','finSavetId = savetId','LEFT');
        $this->db->join('ref_revenue_type','finRevtId = revtId','LEFT');
        $this->db->join('d_transportsale','finTsaleId = tsaleId','LEFT');
        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }

    function by_id($id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('ref_finance_type','finFtId = ftId','LEFT');
        $this->db->join('ref_farmer','finFarmerId = farmerId','LEFT');
        $this->db->join('ref_loan_type','finLtId = ltId','LEFT');
        $this->db->join('ref_saving_type','finSavetId = savetId','LEFT');
        $this->db->join('ref_revenue_type','finRevtId = revtId','LEFT');
        $this->db->join('d_transportsale','finTsaleId = tsaleId','LEFT');
        $this->db->where($id);
        $qr=$this->db->get();
        if($qr->num_rows()==1)
            return $qr->row();
        else
            return false;
    }



            }
            