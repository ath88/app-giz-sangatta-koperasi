
<?php
class Model_dkryngaji extends Model_Master
{
    protected $table = 'd_karyawan_gaji';


    public function __construct()
    {
        parent::__construct();
    }
    function all()
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('ref_karyawan', 'gajikKrynId = krynId', 'LEFT');
        $this->db->order_by('gajikDate DESC, krynId');
        $qr = $this->db->get();
        if ($qr->num_rows() > 0)
            return $qr->result();
        else
            return false;
    }

    function by_id($id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('ref_karyawan', 'gajikKrynId = krynId', 'LEFT');
        $this->db->where($id);
        $qr = $this->db->get();
        if ($qr->num_rows() == 1)
            return $qr->row();
        else
            return false;
    }
}
