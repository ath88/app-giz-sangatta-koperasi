
<?php
class Model_refworker extends Model_Master
{
    protected $table = 'ref_worker';

    public function __construct()
    {
        parent::__construct();
    }
    function all()
    {
        $this->db->select($this->table . '.*,lr.NAMA_WILAYAH lrWIL');
        $this->db->from($this->table);
        $this->db->join('ref_wilayah lr', 'workerKotaLahir = lr.ID_WILAYAH', 'LEFT');
        $qr = $this->db->get();
        if ($qr->num_rows() > 0)
            return $qr->result();
        else
            return false;
    }

    function by_id($id)
    {
        $this->db->select($this->table . '.*,lr.NAMA_WILAYAH');
        $this->db->from($this->table);
        $this->db->join('ref_wilayah lr', 'workerKotaLahir = lr.ID_WILAYAH', 'LEFT');
        $this->db->where($id);
        $qr = $this->db->get();
        if ($qr->num_rows() == 1)
            return $qr->row();
        else
            return false;
    }
}
