
<?php
class Model_dtransportsale extends Model_Master
{
    protected $table = 'd_transportsale';


    public function __construct()
    {
        parent::__construct();
    }

    function all($year = '', $month = '', $farmerId = '')
    {
        $qr = $this->db->query("
        SELECT hsaleDate,MONTH(hsaleDate) bulan, YEAR(hsaleDate) tahun,
            farmerName,parcelTotalArea,
            truckName,truckNumber,
            millName,contDate,
            harvestJJG,harvestBrondol,(harvestJJG+harvestBrondol) jumPanen,
            hsaleTJJG,hsaleTBrondol,(hsaleTJJG+hsaleTBrondol) jumTimbang,
            hsaleNJJG,hsaleNBrondol,(hsaleNJJG+hsaleNBrondol) jumNPanen,
            contPrice,
            ((hsaleNJJG+hsaleNBrondol)*contPrice) jumJualKotor,
            (((hsaleNJJG+hsaleNBrondol)*contPrice)*(2.5/100)) tPPH,
            hsaleTransport,
            ((hsaleNJJG+hsaleNBrondol)*30) tPotong,
            ((hsaleNJJG+hsaleNBrondol)*1) tAdmin,
            ((harvestJJG+harvestBrondol)*20) tTimbang
        FROM d_harvest_sale
        LEFT JOIN d_harvest ON hsaleHarvestId = harvestId
        LEFT JOIN d_farmer_parcel ON parcelId = harvestParcelId
        LEFT JOIN ref_farmer ON farmerId = parcelFarmerId
        LEFT JOIN ref_truck ON hsaleTruckId = truckId
        LEFT JOIN d_contract ON contId = hsaleContId
        LEFT JOIN ref_mill ON millId = contMillId
        WHERE YEAR(hsaleDate) = '$year' AND MONTH(hsaleDate) LIKE '$month' AND farmerId = '$farmerId'
        ORDER BY hsaleDate
        ");

        if ($qr->num_rows() > 0)
            return $qr->result();
        else
            return false;
    }

    function get_potongan($year = '', $month = '', $farmerId = '')
    {
        $qr = $this->db->query("
        SELECT ftId,ftName,SUM(finAmount) jumAmount FROM d_finance
        LEFT JOIN ref_finance_type ON finFtId = ftId
        WHERE finFarmerId = '$farmerId' AND MONTH(finDate) LIKE '$month' AND YEAR(finDate) = '$year' AND finDK = 'D'
        GROUP BY ftId
        ");

        if ($qr->num_rows() > 0)
            return $qr->result();
        else
            return false;
    }
}
