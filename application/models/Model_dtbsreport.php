
<?php
class Model_dtbsreport extends Model_Master
{
    protected $table = 'd_harvest';


    public function __construct()
    {
        parent::__construct();
    }
    function all($year = '')
    {
        $qr = $this->db->query("
                SELECT nBulan,naBulan,tahun,AVG(contPrice) contPrice, SUM(jumNPanen) jumNPanen,
                (SUM(jumNPanen)*30) tPotong,
                (SUM(jumNPanen)*7) tPendapatan,
                (SUM(jumNPanen)*6) tInfra,
                (SUM(jumNPanen)*7) tKop,
                (SUM(jumNPanen)*7) tSSWJ,
                (SUM(jumNPanen)*3) tMandiri,
                (SUM(jumNPanen)*10) tKelompok
                FROM ref_bulan
                LEFT JOIN(
                    SELECT hsaleDate,MONTH(hsaleDate) bulan, YEAR(hsaleDate) tahun,
                    hsaleNJJG,hsaleNBrondol,contPrice,
                    (hsaleNJJG+hsaleNBrondol) jumNPanen,
                    ((hsaleNJJG+hsaleNBrondol)*contPrice) jumPenjualan
                    FROM d_harvest_sale
                    LEFT JOIN d_contract ON contId = hsaleContId
                    WHERE YEAR(hsaleDate) = '$year'
                ) datas ON nBulan = bulan
                GROUP BY nBulan
                ORDER BY nBulan
        ");

        if ($qr->num_rows() > 0)
            return $qr->result();
        else
            return false;
    }
}
