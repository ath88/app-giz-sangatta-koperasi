
<?php
class Model_refsaprodiharga extends Model_Master
{
    protected $table = 'ref_saprodi_harga';


    public function __construct()
    {
        parent::__construct();
    }
    function all()
    {
        $this->db->select($this->table.'.*,ref_saprodi_jenis.*,SUM(saprodibJum)-SUM(saprodisJumlah) AS stokBarang');
        $this->db->from($this->table);
        $this->db->join('ref_saprodi_jenis', 'hsaprodiJId = jsaprodiId', 'LEFT');
        $this->db->join('d_saprodibuy', 'saprodibHsaprodiId = hsaprodiId', 'LEFT');
        $this->db->join('d_saprodisale', 'saprodisHsaprodiId = hsaprodiId', 'LEFT');
        $this->db->group_by('hsaprodiId');
        $qr = $this->db->get();
        if ($qr->num_rows() > 0)
            return $qr->result();
        else
            return false;
    }

    function by_id($id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('ref_saprodi_jenis', 'hsaprodiJId = jsaprodiId', 'LEFT');
        $this->db->where($id);
        $qr = $this->db->get();
        if ($qr->num_rows() == 1)
            return $qr->row();
        else
            return false;
    }
}
