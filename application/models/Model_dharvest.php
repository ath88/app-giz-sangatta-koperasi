
<?php
class Model_dharvest extends Model_Master
{
    protected $table = 'd_harvest';


    public function __construct()
    {
        parent::__construct();
    }
    function all($where='')
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('d_farmer_parcel', 'harvestParcelId = parcelId', 'LEFT');
        $this->db->join('ref_farmer', 'farmerId = parcelFarmerId', 'LEFT');
        $this->db->join('ref_worker', 'workerNIK = harvestWorkerNIK', 'LEFT');
        if(!empty($where))
            $this->db->where($where);
        $qr = $this->db->get();
        if ($qr->num_rows() > 0)
            return $qr->result();
        else
            return false;
    }

    function by_id($id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('d_farmer_parcel', 'harvestParcelId = parcelId', 'LEFT');
        $this->db->join('ref_farmer', 'farmerId = parcelFarmerId', 'LEFT');
        $this->db->join('ref_worker', 'workerNIK = harvestWorkerNIK', 'LEFT');
        $this->db->where($id);
        $qr = $this->db->get();
        if ($qr->num_rows() == 1)
            return $qr->row();
        else
            return false;
    }
}
