
<?php
class Model_dtraining extends Model_Master
{
    protected $table = 'd_training';

    public function __construct()
    {
        parent::__construct();
    }

    function all()
    {
        $this->db->select($this->table.'.*,COUNT(trainId) jumPeserta');
        $this->db->from($this->table);
        $this->db->join('d_training_participant', 'trainpTrainId = trainId', 'LEFT');
        $this->db->group_by('trainId');
        $qr = $this->db->get();
        if ($qr->num_rows() > 0)
            return $qr->result();
        else
            return false;
    }
}
