
<?php
class Model_dtrainingparticipant extends Model_Master
{
    protected $table = 'd_training_participant';


    public function __construct()
    {
        parent::__construct();
    }
    function all()
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('ref_farmer', 'trainpFarmerId = farmerId', 'LEFT');
        $this->db->join('d_training', 'trainpTrainId = trainId', 'LEFT');
        $qr = $this->db->get();
        if ($qr->num_rows() > 0)
            return $qr->result();
        else
            return false;
    }

    function by_id($id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('ref_farmer', 'trainpFarmerId = farmerId', 'LEFT');
        $this->db->join('d_training', 'trainpTrainId = trainId', 'LEFT');
        $this->db->where($id);
        $qr = $this->db->get();
        if ($qr->num_rows() == 1)
            return $qr->row();
        else
            return false;
    }
}
