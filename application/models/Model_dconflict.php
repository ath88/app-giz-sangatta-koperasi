
<?php
class Model_dconflict extends Model_Master
{
    protected $table = 'd_conflict';


    public function __construct()
    {
        parent::__construct();
    }
    function all()
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('d_farmer_parcel', 'conflictParcelId = parcelId', 'LEFT');
        $this->db->join('ref_farmer', 'farmerId = parcelFarmerId', 'LEFT');
        $qr = $this->db->get();
        if ($qr->num_rows() > 0)
            return $qr->result();
        else
            return false;
    }

    function by_id($id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('d_farmer_parcel', 'conflictParcelId = parcelId', 'LEFT');
        $this->db->join('ref_farmer', 'farmerId = parcelFarmerId', 'LEFT');
        $this->db->where($id);
        $qr = $this->db->get();
        if ($qr->num_rows() == 1)
            return $qr->row();
        else
            return false;
    }
}
