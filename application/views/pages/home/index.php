<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

    <!--begin::Portlet-->
    <div class="kt-portlet kt-faq-v1">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    <?= $datas != false ? $datas[0]->organizationName : '' ?>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="accordion accordion-solid accordion-toggle-plus" id="accordionExample1">
                        <div class="card">
                            <div class="card-header" id="struktur_organisasi">
                                <div class="card-title" data-toggle="collapse" data-target="#struktur_organisasi" aria-expanded="false" aria-controls="struktur_organisasi">
                                    Struktur Organisasi
                                </div>
                            </div>
                            <div id="struktur_organisasi" class="collapse show" aria-labelledby="struktur_organisasi" data-parent="#accordionExample1">
                                <div class="card-body">
                                    <img src="<?= base_url() ?>home/loadimage/<?= $datas != false ? $datas[0]->organizationSO : '' ?>" width="100%">
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="visi">
                                <div class="card-title" data-toggle="collapse" data-target="#visi" aria-expanded="false" aria-controls="visi">
                                    Visi
                                </div>
                            </div>
                            <div id="visi" class="collapse show" aria-labelledby="visi" data-parent="#accordionExample1">
                                <div class="card-body">
                                    <p class="lead"><?= $datas != false ? $datas[0]->organizationVision : '' ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="misi">
                                <div class="card-title" data-toggle="collapse" data-target="#misi" aria-expanded="false" aria-controls="misi">
                                    Misi
                                </div>
                            </div>
                            <div id="misi" class="collapse show" aria-labelledby="misi" data-parent="#accordionExample1">
                                <div class="card-body">
                                    <p class="lead"><?= $datas != false ? $datas[0]->organizationMision : '' ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="alamat">
                                <div class="card-title" data-toggle="collapse" data-target="#alamat" aria-expanded="false" aria-controls="alamat">
                                    Misi
                                </div>
                            </div>
                            <div id="alamat" class="collapse show" aria-labelledby="alamat" data-parent="#accordionExample1">
                                <div class="card-body">
                                    <p class="lead"><?= $datas != false ? $datas[0]->organizationAddress : '' ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--end::Portlet-->
</div>

<!-- end:: Content -->