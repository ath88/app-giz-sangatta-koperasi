<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?= $save_url ?>" method="post" id="form_form">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="gajikIdOld" value="<?= $datas != false ? $datas->gajikId : '' ?>">

                        <div class="form-group">
                            <label>Karyawan</label>
                            <select class="form-control m-select2" name="gajikKrynId">
                                <option value=""></option>
                                <?php
                                foreach ($ref_karyawan as $row) :
                                    echo '<option value="' . $row->krynId . '" ' . ($datas != false ? $row->krynId == $datas->gajikKrynId ? 'selected' : '' : '') . '>' . $row->krynNama . '</option>';
                                endforeach;
                                ?>
                            </select>

                        </div>

                        <div class="form-group">
                            <label>Tanggal Bayar</label>
                            <input type="text" class="form-control" name="gajikDate" id="kt_datepicker_1" placeholder="Tanggal" aria-describedby="Tanggal" value="<?= $datas != false ? $datas->gajikDate : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Gaji Pokok</label>
                            <input type="text" class="form-control" name="gajikGapok" placeholder="Gaji Pokok" aria-describedby="Gaji Pokok" value="<?= $datas != false ? $datas->gajikGapok : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Tunjangan Jabatan</label>
                            <input type="text" class="form-control" name="gajikTjabatan" placeholder="Tunjangan Jabatan" aria-describedby="Tunjangan Jabatan" value="<?= $datas != false ? $datas->gajikTjabatan : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Tunjangan Konsumsi</label>
                            <input type="text" class="form-control" name="gajikTkonsumsi" placeholder="Tunjangan Konsumsi" aria-describedby="Tunjangan Konsumsi" value="<?= $datas != false ? $datas->gajikTkonsumsi : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Tunjangan Harian</label>
                            <input type="text" class="form-control" name="gajikTHarian" placeholder="Tunjangan Harian" aria-describedby="Tunjangan Harian" value="<?= $datas != false ? $datas->gajikTHarian : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Bonus/Lembur</label>
                            <input type="text" class="form-control" name="gajikTBonus" placeholder="Bonus/Lembur" aria-describedby="Bonus/Lembur" value="<?= $datas != false ? $datas->gajikTBonus : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Pajak</label>
                            <input type="text" class="form-control" name="gajikPPajak" placeholder="Pajak" aria-describedby="Pajak" value="<?= $datas != false ? $datas->gajikPPajak : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Asuransi</label>
                            <input type="text" class="form-control" name="gajikPAsuransi" placeholder="Asuransi" aria-describedby="Asuransi" value="<?= $datas != false ? $datas->gajikPAsuransi : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Hutang</label>
                            <input type="text" class="form-control" name="gajikPHutang" placeholder="Hutang" aria-describedby="Hutang" value="<?= $datas != false ? $datas->gajikPHutang : '' ?>">
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->