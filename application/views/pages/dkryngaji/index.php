<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-actions">
                            <a href="<?= $create_url ?>" class="btn btn-outline-primary">
                                <span>
                                    <i class="flaticon2-plus"></i>
                                    <span>Create</span>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="kt-portlet__body">

                    <!--begin::Section-->
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>No</th>
                                            <th>Karyawan</th>
                                            <th>Tanggal</th>
                                            <th>Jabatan</th>
                                            <th>Status</th>
                                            <th>Gaji Pokok</th>
                                            <th>T. Jabatan</th>
                                            <th>T. Konsumsi</th>
                                            <th>T. Harian</th>
                                            <th>Bonus/Lembur</th>
                                            <th>Pajak</th>
                                            <th>Asuransi</th>
                                            <th>Hutang</th>
                                            <th>Diterima</th>

                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if ($datas != false) {
                                            $i = 1;
                                            foreach ($datas as $row) {
                                                $key = $this->encryptions->encode($row->gajikId, $this->config->item('encryption_key'));
                                                $total = $row->gajikGapok+$row->gajikTjabatan+$row->gajikTkonsumsi+$row->gajikTHarian+$row->gajikTBonus-
                                                $row->gajikPPajak-$row->gajikPAsuransi-$row->gajikPHutang;
                                        ?>
                                                <tr>
                                                    <th scope="row"><?= $i++ ?></th>
                                                    <td><?= $row->krynNama ?></td>
                                                    <td><?= $row->gajikDate ?></td>
                                                    <td><?= $row->gajikJabatan ?></td>
                                                    <td><?= $row->gajikStatus ?></td>
                                                    <td align="right"><?= number_format($row->gajikGapok,0) ?></td>
                                                    <td align="right"><?= number_format($row->gajikTjabatan,0) ?></td>
                                                    <td align="right"><?= number_format($row->gajikTkonsumsi,0) ?></td>
                                                    <td align="right"><?= number_format($row->gajikTHarian,0) ?></td>
                                                    <td align="right"><?= number_format($row->gajikTBonus,0) ?></td>
                                                    <td align="right"><?= number_format($row->gajikPPajak,0) ?></td>
                                                    <td align="right"><?= number_format($row->gajikPAsuransi,0) ?></td>
                                                    <td align="right"><?= number_format($row->gajikPHutang,0) ?></td>
                                                    <td align="right"><?= number_format($total,0) ?></td>
                                                    <td>
                                                        <a href="<?= $update_url . $key ?>" title="Update" class="btn btn-sm btn-outline-primary btn-elevate btn-circle btn-icon">
                                                            <span>
                                                                <i class="fa fa-pencil-alt"></i>
                                                            </span>
                                                        </a>
                                                        <a href="<?= $delete_url . $key ?>" title="Delete" id='ts_remove_row<?= $i; ?>' class="ts_remove_row btn btn-sm btn-outline-danger btn-elevate btn-circle btn-icon">
                                                            <span>
                                                                <i class="fa fa-trash-alt"></i>
                                                            </span>
                                                        </a>
                                                    </td>
                                                </tr>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <!--end::Section-->
                </div>
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->