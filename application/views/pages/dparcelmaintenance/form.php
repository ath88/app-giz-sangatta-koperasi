<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?= $save_url ?>" method="post" id="form_form">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="pmIdOld" value="<?= $datas != false ? $datas->pmId : '' ?>">

                        <div class="form-group">
                            <label>Tipe Pemeliharaan</label>
                            <select class="form-control m-select2" name="pmMaintainId">
                                <option value=""></option>
                                <?php
                                foreach ($ref_maintenance as $row) :
                                    echo '<option value="' . $row->maintainId . '" ' . ($datas != false ? $datas->pmMaintainId == $row->maintainId ? 'selected' : '' : '') . '>' . $row->maintainName . '</option>';
                                endforeach;
                                ?>
                            </select>

                        </div>

                        <div class="form-group">
                            <label>Tanggal</label>
                            <div class="input-group date">
                                <input type="text" class="form-control" name="pmDate" placeholder="Tanggal" id="kt_datepicker_1" aria-describedby="Tanggal" value="<?= $datas != false ? $datas->pmDate : '' ?>">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Persil</label>
                            <select class="form-control m-select2" name="pmParcelId">
                                <option value=""></option>
                                <?php
                                foreach ($d_farmer_parcel as $row) :
                                    echo '<option value="' . $row->parcelId . '" ' . ($datas != false ? $datas->pmParcelId == $row->parcelId ? 'selected' : '' : '') . '>' . $row->farmerName . ' - ' . $row->parcelTotalArea . '</option>';
                                endforeach;
                                ?>
                            </select>

                        </div>

                        <div class="form-group">
                            <label>Aktivitas</label>
                            <textarea class="form-control" name="pmActivity" placeholder="Aktivitas" aria-describedby="Aktivitas"><?= $datas != false ? $datas->pmActivity : '' ?></textarea>
                        </div>

                        <div class="form-group">
                            <label>Jumlah Area</label>
                            <input type="text" class="form-control" name="pmArea" placeholder="Jumlah Area" aria-describedby="Jumlah Area" value="<?= $datas != false ? $datas->pmArea : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>SOP</label>
                            <textarea class="form-control" name="pmSOP" placeholder="SOP" aria-describedby="SOP"><?= $datas != false ? $datas->pmSOP : '' ?></textarea>
                        </div>

                        <div class="form-group">
                            <label>Jumlah Pohon</label>
                            <input type="text" class="form-control" name="pmPalms" placeholder="Jumlah Pohon" aria-describedby="Jumlah Pohon" value="<?= $datas != false ? $datas->pmPalms : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Jenis Biokimia</label>
                            <select class="form-control m-select2" name="pmFertId">
                                <option value=""></option>
                                <?php
                                foreach ($ref_fertilizer as $row) :
                                    echo '<option value="' . $row->fertId . '" ' . ($datas != false ? $datas->pmFertId == $row->fertId ? 'selected' : '' : '') . '>' . $row->fertNama . '</option>';
                                endforeach;
                                ?>
                            </select>

                        </div>

                        <div class="form-group">
                            <label>Rotasi</label>
                            <input type="text" class="form-control" name="pmRotate" placeholder="Rotasi" aria-describedby="Rotasi" value="<?= $datas != false ? $datas->pmRotate : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Pekerja HK</label>
                            <input type="text" class="form-control" name="pmHK" placeholder="Pekerja HK" aria-describedby="Pekerja HK" value="<?= $datas != false ? $datas->pmHK : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Total Upah Pekerja</label>
                            <input type="text" class="form-control" name="pmGaji" placeholder="Upah Pekerja" aria-describedby="Upah Pekerja" value="<?= $datas != false ? $datas->pmGaji : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Merk Bahan</label>
                            <input type="text" class="form-control" name="pmBrand" placeholder="Merk" aria-describedby="Merk" value="<?= $datas != false ? $datas->pmBrand : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Volume Bahan</label>
                            <input type="text" class="form-control" name="pmVolume" placeholder="Volume" aria-describedby="Volume" value="<?= $datas != false ? $datas->pmVolume : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Satuan Bahan</label>
                            <input type="text" class="form-control" name="pmSatuan" placeholder="Satuan" aria-describedby="Satuan" value="<?= $datas != false ? $datas->pmSatuan : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Total Harga Bahan</label>
                            <input type="text" class="form-control" name="pmNilaiTotal" placeholder="Total Harga Bahan" aria-describedby="Total Harga Bahan" value="<?= $datas != false ? $datas->pmNilaiTotal : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Nilai Transport</label>
                            <input type="text" class="form-control" name="pmTransport" placeholder="Nilai Transport" aria-describedby="Nilai Transport" value="<?= $datas != false ? $datas->pmTransport : '' ?>">
                        </div>
                        
                        <div class="form-group">
                            <label>Penggunaan APD</label>
                            <div class="m-radio-inline">
                                <label class="m-radio">
                                    <input type="radio" name="pmIsAPD" value="1" <?= $datas !== false ? $datas->pmIsAPD == '1' ? 'checked' : '' : '' ?>>
                                    YA
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="pmIsAPD" value="0" <?= $datas !== false ? $datas->pmIsAPD == '0' ? 'checked' : '' : '' ?>>
                                    TIDAK
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->