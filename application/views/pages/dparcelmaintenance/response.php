<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-actions">
                            <a href="<?= $export_url ?>" class="btn btn-outline-success">
                                <span>
                                    <i class="flaticon2-file"></i>
                                    <span>Excel</span>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="kt-portlet__body">

                    <!--begin::Section-->
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>No</th>
                                            <th>Tipe Pemeliharaan</th>
                                            <th>Persil</th>
                                            <th>Jenis Biokimia</th>
                                            <th>Tanggal</th>
                                            <th>Aktivitas</th>
                                            <th>Jumlah Area</th>
                                            <th>Merk</th>
                                            <th>Volume</th>
                                            <th>SOP</th>
                                            <th>Jumlah Pohon</th>
                                            <th>APD</th>

                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if ($datas != false) {
                                            $i = 1;
                                            foreach ($datas as $row) {
                                                $key = $this->encryptions->encode($row->pmId, $this->config->item('encryption_key'));
                                        ?>
                                                <tr>
                                                    <th scope="row"><?= $i++ ?></th>
                                                    <td><?= $row->maintainName ?></td>
                                                    <td><?= $row->farmerName . ' - ' . $row->parcelTotalArea ?></td>
                                                    <td><?= $row->fertNama ?></td>
                                                    <td><?= $row->pmDate ?></td>
                                                    <td><?= $row->pmActivity ?></td>
                                                    <td><?= $row->pmArea ?></td>
                                                    <td><?= $row->pmBrand ?></td>
                                                    <td><?= $row->pmVolume ?></td>
                                                    <td><?= $row->pmSOP ?></td>
                                                    <td><?= $row->pmPalms ?></td>
                                                    <td><?= ($row->pmIsAPD == 1 ? 'YA' : 'TIDAK') ?></td>
                                                    <td>
                                                        <a href="<?= $update_url . $key ?>" title="Update" class="btn btn-sm btn-outline-primary btn-elevate btn-circle btn-icon">
                                                            <span>
                                                                <i class="fa fa-pencil-alt"></i>
                                                            </span>
                                                        </a>
                                                        <a href="<?= $delete_url . $key ?>" title="Delete" id='ts_remove_row<?= $i; ?>' class="ts_remove_row btn btn-sm btn-outline-danger btn-elevate btn-circle btn-icon">
                                                            <span>
                                                                <i class="fa fa-trash-alt"></i>
                                                            </span>
                                                        </a>
                                                    </td>
                                                </tr>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <!--end::Section-->
                </div>
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->