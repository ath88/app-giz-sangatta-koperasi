<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?= $save_url ?>" method="post" id="form_form">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="asetIdOld" value="<?= $datas != false ? $datas->asetId : '' ?>">

                        <div class="form-group">
                            <label>Nomor Aset</label>
                            <input type="text" class="form-control" name="asetNo" placeholder="Nomor Aset" aria-describedby="Nomor Aset" value="<?= $datas != false ? $datas->asetNo : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Nama Barang</label>
                            <input type="text" class="form-control" name="asetNama" placeholder="Nama Barang" aria-describedby="Nama Barang" value="<?= $datas != false ? $datas->asetNama : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Tahun Perolehan</label>
                            <input type="text" class="form-control" name="asetTahun" placeholder="Tahun Perolehan" aria-describedby="Tahun Perolehan" value="<?= $datas != false ? $datas->asetTahun : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Nilai Awal</label>
                            <input type="text" class="form-control" name="asetNilaiAwal" placeholder="Nilai Awal" aria-describedby="Nilai Awal" value="<?= $datas != false ? $datas->asetNilaiAwal : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Nilai Saat ini</label>
                            <input type="text" class="form-control" name="asetNilaiAkhir" placeholder="Nilai Saat ini" aria-describedby="Nilai Saat ini" value="<?= $datas != false ? $datas->asetNilaiAkhir : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Kondisi</label>
                            <div class="m-radio-inline">
                                <label class="m-radio">
                                    <input type="radio" name="asetKondisi" value="BAIK" <?= $datas !== false ? $datas->asetKondisi == 'BAIK' ? 'checked' : '' : '' ?>>
                                    BAIK
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="asetKondisi" value="RUSAK RINGAN" <?= $datas !== false ? $datas->asetKondisi == 'RUSAK RINGAN' ? 'checked' : '' : '' ?>>
                                    RUSAK RINGAN
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="asetKondisi" value="RUSAK BERAT" <?= $datas !== false ? $datas->asetKondisi == 'RUSAK BERAT' ? 'checked' : '' : '' ?>>
                                    RUSAK BERAT
                                    <span></span>
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Pemakai</label>
                            <input type="text" class="form-control" name="asetPIC" placeholder="Pemakai" aria-describedby="Pemakai" value="<?= $datas != false ? $datas->asetPIC : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Keterangan</label>
                            <input type="text" class="form-control" name="asetKet" placeholder="Keterangan" aria-describedby="Keterangan" value="<?= $datas != false ? $datas->asetKet : '' ?>">
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->