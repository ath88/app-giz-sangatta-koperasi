<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?= $save_url ?>" method="post" id="form_form">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="harvestIdOld" value="<?= $datas != false ? $datas->harvestId : '' ?>">

                        <div class="form-group">
                            <label>Persil</label>
                            <select class="form-control m-select2" name="harvestParcelId">
                                <option value=""></option>
                                <?php
                                foreach ($d_farmer_parcel as $row) :
                                    echo '<option value="' . $row->parcelId . '" ' . ($datas != false ? $datas->harvestParcelId == $row->parcelId ? 'selected' : '' : '') . '>' . $row->farmerName . ' - ' . $row->parcelTotalArea . '</option>';
                                endforeach;
                                ?>
                            </select>

                        </div>

                        <div class="form-group">
                            <label>Tanggal</label>
                            <div class="input-group date">
                                <input type="text" class="form-control" name="harvestDate" placeholder="Tanggal" id="kt_datepicker_1" aria-describedby="Tanggal" value="<?= $datas != false ? $datas->harvestDate : '' ?>">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Volume</label>
                            <input type="text" class="form-control" name="harvestVolume" placeholder="Volume" aria-describedby="Volume" value="<?= $datas != false ? $datas->harvestVolume : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Pekerja</label>
                            <select class="form-control m-select2" name="harvestWorkerNIK">
                                <option value=""></option>
                                <?php
                                foreach ($d_worker as $row) :
                                    echo '<option value="' . $row->workerNIK . '" ' . ($datas != false ? $datas->harvestWorkerNIK == $row->workerNIK ? 'selected' : '' : '') . '>' . $row->workerNIK . ' - ' . $row->workerName . '</option>';
                                endforeach;
                                ?>
                            </select>

                        </div>

                        <div class="form-group">
                            <label>Rotasi</label>
                            <input type="text" class="form-control" name="harvestRotasi" placeholder="Rotasi" aria-describedby="Rotasi" value="<?= $datas != false ? $datas->harvestRotasi : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Hari Kerja</label>
                            <input type="text" class="form-control" name="harvestWorkerHK" placeholder="Hari Kerja" aria-describedby="Hari Kerja" value="<?= $datas != false ? $datas->harvestWorkerHK : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Berat JJG</label>
                            <input type="text" class="form-control" name="harvestJJG" placeholder="JJG" aria-describedby="JJG" value="<?= $datas != false ? $datas->harvestJJG : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Berat Brondol</label>
                            <input type="text" class="form-control" name="harvestBrondol" placeholder="Brondol" aria-describedby="Brondol" value="<?= $datas != false ? $datas->harvestBrondol : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Berat Kirim</label>
                            <input type="text" class="form-control" name="harvestKirim" placeholder="Kirim" aria-describedby="Kirim" value="<?= $datas != false ? $datas->harvestKirim : '' ?>">
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->