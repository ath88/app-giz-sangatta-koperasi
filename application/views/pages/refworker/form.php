<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?= $save_url ?>" method="post" id="form_form">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="workerNIKOld" value="<?= $datas != false ? $datas->workerNIK : '' ?>">

                        <div class="form-group">
                            <label>NIK</label>
                            <input type="text" class="form-control" name="workerNIK" placeholder="NIK" aria-describedby="NIK" value="<?= $datas != false ? $datas->workerNIK : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" class="form-control" name="workerName" placeholder="Nama" aria-describedby="Nama" value="<?= $datas != false ? $datas->workerName : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Alamat</label>
                            <input type="text" class="form-control" name="workerAddress" placeholder="Alamat" aria-describedby="Alamat" value="<?= $datas != false ? $datas->workerAddress : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Jenis Kelamin</label>
                            <div class="m-radio-inline">
                                <label class="m-radio">
                                    <input type="radio" name="workerGender" value="L" <?= $datas !== false ? $datas->workerGender == 'L' ? 'checked' : '' : '' ?>>
                                    Laki-laki
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="workerGender" value="P" <?= $datas !== false ? $datas->workerGender == 'P' ? 'checked' : '' : '' ?>>
                                    Perempuan
                                    <span></span>
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Tanggal Lahir</label>
                            <div class="input-group date">
                                <input type="text" class="form-control" placeholder="Pilih tanggal" id="kt_datepicker_1" name="workerTglLahir" value="<?= $datas != false ? $datas->workerTglLahir : '' ?>" />
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Kota Lahir</label>
                            <select class="form-control m-select2" name="workerKotaLahir">
                                <option value=""></option>
                                <?php
                                foreach ($ref_wilayah as $row) :
                                    echo '<option value="' . $row->ID_WILAYAH . '" ' . ($datas != false ? $datas->workerKotaLahir == $row->ID_WILAYAH ? 'selected' : '' : '') . '>' . $row->NAMA_WILAYAH . ' (' . $row->NAMA_WILAYAH_2 . ')</option>';
                                endforeach;
                                ?>
                            </select>

                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->