<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-actions">
                            <a href="<?= $export_url ?>" class="btn btn-outline-success">
                                <span>
                                    <i class="flaticon2-file"></i>
                                    <span>Excel</span>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="kt-portlet__body">

                    <!--begin::Section-->
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>No</th>
                                            <th>Bulan</th>
                                            <th>Harga</th>
                                            <th>Berat Bersih</th>
                                            <th>Total Potongan (Rp.30)</th>
                                            <th>Pendapatan Koperasi Desa (Rp.7)</th>
                                            <th>Infrastruktur (Rp.6)</th>
                                            <th>KOP. Induk (Rp.7)</th>
                                            <th>KOP. SSWJ (Rp.7)</th>
                                            <th>KTH Mandiri (Rp.3)</th>
                                            <th>Kelompok Tani (Rp.10)</th>
                                            <th>Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if ($datas != false) {
                                            $i = 1;
                                            foreach ($datas as $row) {
                                                $jumPot = $row->tPotong + $row->tPendapatan + $row->tInfra + $row->tKop + $row->tSSWJ + $row->tMandiri + $row->tKelompok;

                                        ?>
                                                <tr>
                                                    <th scope="row"><?= $i++ ?></th>
                                                    <td><?= $row->naBulan ?></td>
                                                    <td align="right"><?= number_format($row->contPrice, 2) ?></td>
                                                    <td align="right"><?= number_format($row->jumNPanen, 2) ?></td>
                                                    <td align="right"><?= number_format($row->tPotong, 2) ?></td>
                                                    <td align="right"><?= number_format($row->tPendapatan, 2) ?></td>
                                                    <td align="right"><?= number_format($row->tInfra, 2) ?></td>
                                                    <td align="right"><?= number_format($row->tKop, 2) ?></td>
                                                    <td align="right"><?= number_format($row->tSSWJ, 2) ?></td>
                                                    <td align="right"><?= number_format($row->tMandiri, 2) ?></td>
                                                    <td align="right"><?= number_format($row->tKelompok, 2) ?></td>
                                                    <td align="right"><?= number_format($jumPot, 2) ?></td>
                                                </tr>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <!--end::Section-->
                </div>
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->