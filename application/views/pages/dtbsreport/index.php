<?php $this->load->view('layouts/subheader'); ?>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="kt-portlet">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title"> <?= strtoupper($page_judul) ?></h3>
					</div>	
				</div>
				<div class="kt-portlet__body">
					<form class="kt-form" action="<?= $show_url ?>" method="post" id="form_show">
						<div class="form-group">
							<label>Tahun</label>
							<select class="form-control m-select2" name="tahun" id="tahun">
								<option value="">Pilih</option>
								<?php
								for($i=date('Y');$i>=2020;$i--)
								{
									echo '<option value="' . $i . '">' . $i . '</option>'; 
								}
								?>
							</select>
						</div>
				</div>
				<div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<button type="submit" id="btn_save" class="btn btn-primary">Tampil</button>
					</div>
				</div>
				</form>

			</div>
		</div>
	</div>
</div>
<div id="response"></div>