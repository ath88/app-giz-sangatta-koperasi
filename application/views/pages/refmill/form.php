<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?= $save_url ?>" method="post" id="form_form">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="millIdOld" value="<?= $datas != false ? $datas->millId : '' ?>">

                        <div class="form-group">
                            <label>Nama Perusahaan</label>
                            <input type="text" class="form-control" name="millName" placeholder="Nama Perusahaan" aria-describedby="Nama Perusahaan" value="<?= $datas != false ? $datas->millName : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Perusahaan Utama</label>
                            <input type="text" class="form-control" name="millParent" placeholder="Perusahaan Utama" aria-describedby="Perusahaan Utama" value="<?= $datas != false ? $datas->millParent : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Alamat</label>
                            <input type="text" class="form-control" name="millAddress" placeholder="Alamat" aria-describedby="Alamat" value="<?= $datas != false ? $datas->millAddress : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Koordinat</label>
                            <input type="text" class="form-control" name="millCoordinate" placeholder="Koordinat" aria-describedby="Koordinat" value="<?= $datas != false ? $datas->millCoordinate : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Total Kapasitas</label>
                            <input type="text" class="form-control" name="millCapacity" placeholder="Total Kapasitas" aria-describedby="Total Kapasitas" value="<?= $datas != false ? $datas->millCapacity : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Kapasitas Digunakan</label>
                            <input type="text" class="form-control" name="millUsedCapacity" placeholder="Kapasitas Digunakan" aria-describedby="Kapasitas Digunakan" value="<?= $datas != false ? $datas->millUsedCapacity : '' ?>">
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->