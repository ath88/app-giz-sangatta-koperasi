<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?= $save_url ?>" method="post" id="form_form">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="urusIdOld" value="<?= $datas != false ? $datas->urusId : '' ?>">

                        <div class="form-group">
                            <label>Nomor Induk Kependudukan</label>
                            <input type="text" class="form-control" name="urusNIK" placeholder="Nomor Induk Kependudukan" aria-describedby="Nomor Induk Kependudukan" value="<?= $datas != false ? $datas->urusNIK : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" class="form-control" name="urusNama" placeholder="Nama" aria-describedby="Nama" value="<?= $datas != false ? $datas->urusNama : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Jenis Kelamin</label>
                            <div class="m-radio-inline">
                                <label class="m-radio">
                                    <input type="radio" name="urusGender" value="L" <?= $datas !== false ? $datas->urusGender == 'L' ? 'checked' : '' : '' ?>>
                                    Laki-laki
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="urusGender" value="P" <?= $datas !== false ? $datas->urusGender == 'P' ? 'checked' : '' : '' ?>>
                                    Perempuan
                                    <span></span>
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Tempat Lahir</label>
                            <select class="form-control m-select2" name="urusTempatLahir">
                                <option value=""></option>
                                <?php
                                foreach ($ref_wilayah as $row) :
                                    echo '<option value="' . $row->ID_WILAYAH . '" ' . ($datas != false ? $row->ID_WILAYAH == $datas->urusTempatLahir ? 'selected' : '' : '') . '>' . $row->NAMA_WILAYAH . '</option>';
                                endforeach;
                                ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Tanggal Lahir</label>
                            <input type="text" class="form-control" name="urusTanggalLahir" id="kt_datepicker_1" placeholder="Tanggal Lahir" aria-describedby="Tanggal Lahir" value="<?= $datas != false ? $datas->urusTanggalLahir : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Alamat</label>
                            <input type="text" class="form-control" name="urusAlamat" placeholder="Alamat" aria-describedby="Alamat" value="<?= $datas != false ? $datas->urusAlamat : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Jabatan</label>
                            <input type="text" class="form-control" name="urusJabatan" placeholder="Jabatan" aria-describedby="Jabatan" value="<?= $datas != false ? $datas->urusJabatan : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Tanggal Masuk</label>
                            <input type="text" class="form-control" name="urusTanggalMasuk" id="kt_datepicker_2" placeholder="Tanggal Masuk" aria-describedby="Tanggal Masuk" value="<?= $datas != false ? $datas->urusTanggalMasuk : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Tanggal Keluar</label>
                            <input type="text" class="form-control" name="urusTanggalKeluar" id="kt_datepicker_3" placeholder="Tanggal Keluar" aria-describedby="Tanggal Keluar" value="<?= $datas != false ? $datas->urusTanggalKeluar : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Status Kawin</label>
                            <div class="m-radio-inline">
                                <label class="m-radio">
                                    <input type="radio" name="urusStatusKawin" value="BELUM KAWIN" <?= $datas !== false ? $datas->urusStatusKawin == 'BELUM KAWIN' ? 'checked' : '' : '' ?>>
                                    BELUM KAWIN
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="urusStatusKawin" value="KAWIN" <?= $datas !== false ? $datas->urusStatusKawin == 'KAWIN' ? 'checked' : '' : '' ?>>
                                    KAWIN
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="urusStatusKawin" value="DUDA" <?= $datas !== false ? $datas->urusStatusKawin == 'DUDA' ? 'checked' : '' : '' ?>>
                                    DUDA
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="urusStatusKawin" value="JANDA" <?= $datas !== false ? $datas->urusStatusKawin == 'JANDA' ? 'checked' : '' : '' ?>>
                                    JANDA
                                    <span></span>
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Keterangan</label>
                            <input type="text" class="form-control" name="urusKeterangan" placeholder="Keterangan" aria-describedby="Keterangan" value="<?= $datas != false ? $datas->urusKeterangan : '' ?>">
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->