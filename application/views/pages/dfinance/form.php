<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?= $save_url ?>" method="post" id="form_form">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="finIdOld" value="<?= $datas != false ? $datas->finId : '' ?>">

                        <div class="form-group">
                            <label>Transaksi</label>
                            <select class="form-control m-select2" name="finFtId">
                                <option value=""></option>
                                <?php
                                foreach ($ref_finance_type as $row) :
                                    echo '<option value="' . $row->ftId . '" ' . ($datas != false ? $datas->finFtId == $row->ftId ? 'selected' : '' : '') . '>' . $row->ftName . '</option>';
                                endforeach;
                                ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Petani</label>
                            <select class="form-control m-select2" name="finFarmerId">
                                <option value=""></option>
                                <?php
                                foreach ($ref_farmer as $row) :
                                    echo '<option value="' . $row->farmerId . '" ' . ($datas != false ? $datas->finFarmerId == $row->farmerId ? 'selected' : '' : '') . '>' . $row->farmerName . '</option>';
                                endforeach;
                                ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Tanggal</label>
                            <div class="input-group date">
                                <input type="text" class="form-control" name="finDate" placeholder="Tanggal" id="kt_datepicker_1" aria-describedby="Tanggal" value="<?= $datas != false ? $datas->finDate : '' ?>">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Debit/Kredit</label>
                            <div class="m-radio-inline">
                                <label class="m-radio">
                                    <input type="radio" name="finDK" value="D" <?= $datas !== false ? $datas->finDK == 'D' ? 'checked' : '' : '' ?>>
                                    Debit
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="finDK" value="K" <?= $datas !== false ? $datas->finDK == 'K' ? 'checked' : '' : '' ?>>
                                    Kredit
                                    <span></span>
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Nominal (Rp)</label>
                            <input type="text" class="form-control" name="finAmount" placeholder="Nominal" aria-describedby="Nominal" value="<?= $datas != false ? $datas->finAmount : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>
                                File Kontrak Peminjaman
                            </label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="finLoanFile" id="custom-file-input">
                                <label class="custom-file-label" for="customFile">
                                    Pilih Berkas
                                </label>
                                <span class="m-form__help">
                                    Filetipe: *.pdf,*.jpg | Maks. Size: 15MB <?= $datas != false ? ' | File: '. $datas->finLoanFile : '' ?>
                                </span>
                            </div>
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->