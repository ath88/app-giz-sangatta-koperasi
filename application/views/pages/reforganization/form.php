<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?= $save_url ?>" method="post" id="form_form" enctype="multipart/form-data">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="organizationIdOld" value="<?= $datas != false ? $datas->organizationId : '' ?>">
                        
                        <div class="form-group">
                            <label>Nomor Induk Organisasi</label>
                            <input type="text" class="form-control" name="organizationNIK" placeholder="Nomor Organisasi" aria-describedby="Nomor Organisasi" value="<?= $datas != false ? $datas->organizationNIK : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Nama Organisasi</label>
                            <input type="text" class="form-control" name="organizationName" placeholder="Organisasi" aria-describedby="Organisasi" value="<?= $datas != false ? $datas->organizationName : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Alamat</label>
                            <textarea class="form-control" name="organizationAddress" placeholder="Alamat" aria-describedby="Alamat" ><?= $datas != false ? $datas->organizationAddress : '' ?></textarea>
                        </div>

                        <div class="form-group">
                            <label>Visi</label>
                            <textarea class="form-control" name="organizationVision" placeholder="Visi" aria-describedby="Visi" ><?= $datas != false ? $datas->organizationVision : '' ?></textarea>
                        </div>

                        <div class="form-group">
                            <label>Misi</label>
                            <textarea class="form-control" name="organizationMision" placeholder="Misi" aria-describedby="Misi" ><?= $datas != false ? $datas->organizationMision : '' ?></textarea>
                        </div>

                        <div class="form-group">
                            <label>
                                Dokumen Organisasi
                            </label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="berkas" id="custom-file-input">
                                <label class="custom-file-label" for="customFile">
                                    Pilih Berkas
                                </label>
                                <span class="m-form__help">
                                    Filetipe: *.pdf,*.jpg | Maks. Size: 15MB <?= $datas != false ? ' | File: '. $datas->organizationAkta : '' ?>
                                </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>
                                Struktur Organisasi
                            </label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="berkasSO" id="custom-file-input">
                                <label class="custom-file-label" for="customFile">
                                    Pilih Berkas
                                </label>
                                <span class="m-form__help">
                                    Filetipe: *.jpg | Maks. Size: 15MB <?= $datas != false ? ' | File: '. $datas->organizationSO : '' ?>
                                </span>
                            </div>
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->