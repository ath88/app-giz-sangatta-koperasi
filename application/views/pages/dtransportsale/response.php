<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-actions">
                            <a href="<?= $export_url ?>" class="btn btn-outline-success">
                                <span>
                                    <i class="flaticon2-file"></i>
                                    <span>Excel</span>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="kt-portlet__body">

                    <!--begin::Section-->
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>Petani</th>
                                            <th>:</th>
                                            <th><?= $datas != false ? $datas[0]->farmerName : '' ?></th>
                                        </tr>
                                    </thead>
                                </table>
                                <hr />
                                <table class="table table-hover">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>No</th>
                                            <th>Tanggal</th>
                                            <th>B. Kirim</th>
                                            <th>B. Timbang</th>
                                            <th>B. Bersih</th>
                                            <th>Harga</th>
                                            <th>Terima Bruto</th>
                                            <th>Pot. PPH</th>
                                            <th>Pot. Transport</th>
                                            <th>Pot. Koperasi</th>
                                            <th>Jum. Potongan</th>
                                            <th>Terima Netto</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if ($datas != false) {
                                            $i = 1;
                                            $jPanen = $jTimbang = $jNPanen = $jJualKotor = $jtPPH = $jtTransport = $jPotong = $jPot = $jTerima = $jAdmin = $jTimbang = 0;
                                            foreach ($datas as $row) {
                                                $jumPot = $row->tPPH + $row->hsaleTransport + $row->tPotong;
                                                $jumTerima = $row->jumJualKotor - $jumPot;

                                                $jPanen += $row->jumPanen;
                                                $jTimbang += $row->jumTimbang;
                                                $jNPanen += $row->jumNPanen;
                                                $jJualKotor += $row->jumJualKotor;
                                                $jtPPH += $row->tPPH;
                                                $jtTransport += $row->hsaleTransport;
                                                $jPotong += $row->tPotong;
                                                $jPot += $jumPot;
                                                $jTerima += $jumTerima;
                                                $jAdmin += $row->tAdmin;
                                                $jTimbang += $row->tTimbang;


                                        ?>
                                                <tr>
                                                    <th scope="row"><?= $i++ ?></th>
                                                    <td><?= $row->hsaleDate ?></td>
                                                    <td align="right"><?= number_format($row->jumPanen, 2) ?></td>
                                                    <td align="right"><?= number_format($row->jumTimbang, 2) ?></td>
                                                    <td align="right"><?= number_format($row->jumNPanen, 2) ?></td>
                                                    <td align="right"><?= number_format($row->contPrice, 2) ?></td>
                                                    <td align="right"><?= number_format($row->jumJualKotor, 2) ?></td>
                                                    <td align="right"><?= number_format($row->tPPH, 2) ?></td>
                                                    <td align="right"><?= number_format($row->hsaleTransport, 2) ?></td>
                                                    <td align="right"><?= number_format($row->tPotong, 2) ?></td>
                                                    <td align="right"><?= number_format($jumPot, 2) ?></td>
                                                    <td align="right"><?= number_format($jumTerima, 2) ?></td>
                                                </tr>
                                            <?php
                                            }
                                            ?>
                                            <tr>
                                                <th colspan="2">&nbsp;</th>
                                                <td align="right"><?= number_format($jPanen, 2) ?></td>
                                                <td align="right"><?= number_format($jTimbang, 2) ?></td>
                                                <td align="right"><?= number_format($jNPanen, 2) ?></td>
                                                <th>&nbsp;</th>
                                                <td align="right"><?= number_format($jJualKotor, 2) ?></td>
                                                <td align="right"><?= number_format($jtPPH, 2) ?></td>
                                                <td align="right"><?= number_format($jtTransport, 2) ?></td>
                                                <td align="right"><?= number_format($jPotong, 2) ?></td>
                                                <td align="right"><?= number_format($jPot, 2) ?></td>
                                                <td align="right"><?= number_format($jTerima, 2) ?></td>
                                            </tr>
                                        <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                                <hr />
                                <table class="table table-hover">
                                    <thead class="thead-light">
                                        <tr>
                                            <th colspan="3">Pemotongan Lain-Lain:</th>
                                        </tr>
                                        <tr>
                                            <td>Administrasi</td>
                                            <td>:</td>
                                            <td align="right" width="80%"><?= number_format($jAdmin, 2) ?></td>
                                        </tr>
                                        <tr>
                                            <td>Biaya Timbang</td>
                                            <td>:</td>
                                            <td align="right"><?= number_format($jTimbang, 2) ?></td>
                                        </tr>
                                        <?php
                                        $tSaprodi = ($angsuran1 != false ? $angsuran1->saprodisAngsuran1 : 0) +
                                            ($angsuran2 != false ? $angsuran2->saprodisAngsuran2 : 0) +
                                            ($angsuran3 != false ? $angsuran3->saprodisAngsuran3 : 0);
                                        ?>
                                        <tr>
                                            <td>Kredit Saprodi</td>
                                            <td>:</td>
                                            <td align="right"><?= number_format($tSaprodi, 2) ?></td>
                                        </tr>
                                        <?php
                                        $jtPot = $jAdmin + $jTimbang + $tSaprodi;
                                        if ($potongan != false) {
                                            foreach ($potongan as $row) {
                                                $jtPot += $row->jumAmount;
                                        ?>
                                                <tr>
                                                    <td><?= $row->ftName ?></td>
                                                    <td>:</td>
                                                    <td align="right"><?= number_format($row->jumAmount, 2) ?></td>
                                                </tr>
                                        <?php
                                            }
                                        }
                                        ?>
                                        <tr>
                                            <td>Total Potongan</td>
                                            <td>:</td>
                                            <td align="right"><?= number_format($jtPot, 2) ?></td>
                                        </tr>
                                        <tr>
                                            <th>Pendapatan Bersih</th>
                                            <th>:</th>
                                            <th align="right"><?= number_format($jTerima - $jtPot, 2) ?></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>

                    <!--end::Section-->
                </div>
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->