<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?= $save_url ?>" method="post" id="form_form">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="tsaleIdOld" value="<?= $datas != false ? $datas->tsaleId : '' ?>">

                        <div class="form-group">
                            <label>Panen</label>
                            <select class="form-control m-select2" name="tsaleHarvestId">
                                <option value=""></option>
                                <?php
                                foreach ($d_harvest as $row) :
                                    echo '<option value="' . $row->harvestId . '" ' . ($datas != false ? $datas->tsaleHarvestId == $row->harvestId ? 'selected' : '' : '') . '>' . $row->farmerName . ' - ' . $row->parcelTotalArea . ' (' . $row->harvestDate . ' - ' . $row->harvestVolume . ')</option>';
                                endforeach;
                                ?>
                            </select>

                        </div>

                        <div class="form-group">
                            <label>Kontrak</label>
                            <select class="form-control m-select2" id="tsaleContractId" name="tsaleContractId">
                                <option value=""></option>
                                <?php
                                foreach ($d_contract as $row) :
                                    echo '<option value="' . $row->contId . '" ' . ($datas != false ? $datas->tsaleContractId == $row->contId ? 'selected' : '' : '') . '>' . $row->millName . ' - ' . $row->contDate . '</option>';
                                endforeach;
                                ?>
                            </select>

                        </div>

                        <div class="form-group">
                            <label>Truk</label>
                            <select class="form-control m-select2" name="tsaleTruckId">
                                <option value=""></option>
                                <?php
                                foreach ($ref_truck as $row) :
                                    echo '<option value="' . $row->truckId . '" ' . ($datas != false ? $datas->tsaleTruckId == $row->truckId ? 'selected' : '' : '') . '>' . $row->truckName . '</option>';
                                endforeach;
                                ?>
                            </select>

                        </div>

                        <div class="form-group">
                            <label>Tanggal</label>
                            <div class="input-group date">
                                <input type="text" class="form-control" name="tsaleDate" placeholder="Tanggal" id="kt_datepicker_1" aria-describedby="Tanggal" value="<?= $datas != false ? $datas->tsaleDate : '' ?>">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Volume Angkut</label>
                            <input type="text" class="form-control" id="tsaleDelVolume" name="tsaleDelVolume" placeholder="Volume Angkut" aria-describedby="Volume Angkut" value="<?= $datas != false ? $datas->tsaleDelVolume : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Volume Diterima</label>
                            <input type="text" class="form-control" id="tsaleRecVolume" name="tsaleRecVolume" placeholder="Volume Diterima" aria-describedby="Volume Diterima" value="<?= $datas != false ? $datas->tsaleRecVolume : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Volume Standar</label>
                            <input type="text" class="form-control" id="tsaleStaVolume" name="tsaleStaVolume" placeholder="Volume Standar" aria-describedby="Volume Standar" value="<?= $datas != false ? $datas->tsaleStaVolume : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Harga/Kg</label>
                            <input type="text" readonly class="form-control" id="tsaleHarga" name="tsaleHarga" placeholder="Harga" aria-describedby="Harga" value="<?= $datas != false ? $datas->tsaleHarga : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Bruto (Rp)</label>
                            <input type="text" readonly class="form-control" id="tsaleBruto" name="tsaleBruto" placeholder="Bruto" aria-describedby="Bruto" value="<?= $datas != false ? $datas->tsaleBruto : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>PPH 0.25%</label>
                            <input type="text" readonly class="form-control" id="tsalePot025" name="tsalePot025" placeholder="Pot025" aria-describedby="Pot025" value="<?= $datas != false ? $datas->tsalePot025 : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Transport</label>
                            <input type="text" readonly class="form-control" id="tsalePotTranport" name="tsalePotTranport" placeholder="Transport" aria-describedby="Transport" value="<?= $datas != false ? $datas->tsalePotTranport : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Pot. Koperasi</label>
                            <input type="text" readonly class="form-control" id="tsalePotKoperasi" name="tsalePotKoperasi" placeholder="Koperasi" aria-describedby="Koperasi" value="<?= $datas != false ? $datas->tsalePotKoperasi : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Administrasi</label>
                            <input type="text" readonly class="form-control" id="tsalePotAdministrasi" name="tsalePotAdministrasi" placeholder="Administrasi" aria-describedby="Administrasi" value="<?= $datas != false ? $datas->tsalePotAdministrasi : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Biaya Timbang</label>
                            <input type="text" class="form-control" id="tsalePotTimbang" name="tsalePotTimbang" placeholder="Timbang" aria-describedby="Timbang" value="<?= $datas != false ? $datas->tsalePotTimbang : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Zakat</label>
                            <input type="text" class="form-control" id="tsaleZakat" name="tsaleZakat" placeholder="Zakat" aria-describedby="Zakat" value="<?= $datas != false ? $datas->tsaleZakat : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Netto</label>
                            <input type="text" readonly class="form-control" id="tsaleNetto" name="tsaleNetto" placeholder="Netto" aria-describedby="Netto" value="<?= $datas != false ? $datas->tsaleNetto : '' ?>">
                        </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->