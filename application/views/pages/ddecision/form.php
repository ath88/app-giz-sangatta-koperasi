<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?= $save_url ?>" method="post" id="form_form">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="doIdOld" value="<?= $datas != false ? $datas->doId : '' ?>">

                        <div class="form-group">
                            <label>Nama Organisasi</label>
                            <select class="form-control m-select2" name="doOrganizationId">
                                <option value=""></option>
                                <?php
                                foreach ($ref_organization as $row) :
                                    echo '<option value="' . $row->organizationId . '" ' . ($datas != false ? $datas->doOrganizationId == $row->organizationId ? 'selected' : '' : '') . '>' . $row->organizationName . '</option>';
                                endforeach;
                                ?>
                            </select>

                        </div>

                        <div class="form-group">
                            <label>Tanggal</label>
                            <div class="input-group date">
                                <input type="text" class="form-control" name="doDate" placeholder="Tanggal" id="kt_datepicker_1" aria-describedby="Tanggal" value="<?= $datas != false ? $datas->doDate : '' ?>">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label>Nomor Keputusan</label>
                            <input type="text" class="form-control" name="doNumber" placeholder="Nomor Keputusan" aria-describedby="Nomor Keputusan" value="<?= $datas != false ? $datas->doNumber : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Deskripsi</label>
                            <input type="text" class="form-control" name="doDescription" placeholder="Deskripsi" aria-describedby="Deskripsi" value="<?= $datas != false ? $datas->doDescription : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>
                                File Keputusan
                            </label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="doFile" id="custom-file-input">
                                <label class="custom-file-label" for="customFile">
                                    Pilih Berkas
                                </label>
                                <span class="m-form__help">
                                    Filetipe: *.pdf,*.jpg | Maks. Size: 15MB <?= $datas != false ? ' | File: '. $datas->doFile : '' ?>
                                </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>
                                File Dokumentasi
                            </label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="doDocumentation" id="custom-file-input">
                                <label class="custom-file-label" for="customFile">
                                    Pilih Berkas
                                </label>
                                <span class="m-form__help">
                                    Filetipe: *.pdf,*.jpg | Maks. Size: 15MB <?= $datas != false ? ' | File: '. $datas->doDocumentation : '' ?>
                                </span>
                            </div>
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->