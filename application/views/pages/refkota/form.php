<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?= $save_url ?>" method="post" id="form_form">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="kotaIdOld" value="<?= $datas != false ? $datas->kotaId : '' ?>">

                        <div class="form-group">
                            <label>Provinsi</label>
                            <select class="form-control m-select2" name="kotaProvId">
                                <option value=""></option>
                                <?php
                                foreach ($ref_provinsi as $row) :
                                    echo '<option value="' . $row->provId . '" ' . ($datas != false ? $datas->provId == $row->kotaProvId ? 'selected' : '' : '') . '>' . $row->provId . '</option>';
                                endforeach;
                                ?>
                            </select>

                        </div>

                        <div class="form-group">
                            <label>Kota</label>
                            <input type="text" class="form-control" name="kotaNama" placeholder="Kota" aria-describedby="Kota" value="<?= $datas != false ? $datas->kotaNama : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Ibu Kota Provinsi ?</label>
                            <input type="text" class="form-control" name="kotaIsIbuProv" placeholder="Ibu Kota Provinsi ?" aria-describedby="Ibu Kota Provinsi ?" value="<?= $datas != false ? $datas->kotaIsIbuProv : '' ?>">
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->