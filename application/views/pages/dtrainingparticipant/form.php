<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?= $save_url ?>" method="post" id="form_form">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="trainpIdOld" value="<?= $datas != false ? $datas->trainpId : '' ?>">

                        <div class="form-group">
                            <label>Petani</label>
                            <select class="form-control m-select2" name="trainpFarmerId">
                                <option value=""></option>
                                <?php
                                foreach ($ref_farmer as $row) :
                                    echo '<option value="' . $row->farmerId . '" ' . ($datas != false ? $datas->trainpFarmerId == $row->farmerId ? 'selected' : '' : '') . '>' . $row->farmerName . '</option>';
                                endforeach;
                                ?>
                            </select>
                            <span class="m-form__help">
                                Pilih jika peserta petani
                            </span>
                        </div>

                        <div class="form-group">
                            <label>Nama Peserta</label>
                            <input type="text" class="form-control" name="trainpNama" placeholder="Nama Peserta" aria-describedby="Nama Peserta" value="<?= $datas != false ? $datas->trainpNama : '' ?>">
                            <span class="m-form__help">
                                Diisi jika peserta bukan petani
                            </span>
                        </div>

                        <div class="form-group">
                            <label>Organisasi Peserta</label>
                            <input type="text" class="form-control" name="trainpOrganization" placeholder="Organisasi / Unit Kerja" aria-describedby="Organisasi / Unit Kerja" value="<?= $datas != false ? $datas->trainpOrganization : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Training</label>
                            <select class="form-control m-select2" name="trainpTrainId">
                                <option value=""></option>
                                <?php
                                foreach ($d_training as $row) :
                                    echo '<option value="' . $row->trainId . '" ' . ($datas != false ? $datas->trainpTrainId == $row->trainId ? 'selected' : '' : '') . '>' . $row->trainTopics . ' (' . $row->trainDate . ')</option>';
                                endforeach;
                                ?>
                            </select>

                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->