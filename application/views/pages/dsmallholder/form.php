<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?= $save_url ?>" method="post" id="form_form">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="screditIdOld" value="<?= $datas != false ? $datas->screditId : '' ?>">

                        <div class="form-group">
                            <label>Tanggal</label>
                            <div class="input-group date">
                                <input type="text" class="form-control" name="screditDate" placeholder="Tanggal" id="kt_datepicker_1" aria-describedby="Tanggal" value="<?= $datas != false ? $datas->screditDate : '' ?>">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Volume</label>
                            <input type="text" class="form-control" name="screditVolume" placeholder="Volume" aria-describedby="Volume" value="<?= $datas != false ? $datas->screditVolume : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Debit/Kredit</label>
                            <div class="m-radio-inline">
                                <label class="m-radio">
                                    <input type="radio" name="screditDK" value="D" <?= $datas !== false ? $datas->screditDK == 'D' ? 'checked' : '' : '' ?>>
                                    Debit
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="screditDK" value="K" <?= $datas !== false ? $datas->screditDK == 'K' ? 'checked' : '' : '' ?>>
                                    Kredit
                                    <span></span>
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Pembeli</label>
                            <input type="text" class="form-control" name="screditBuyer" placeholder="Pembeli" aria-describedby="Pembeli" value="<?= $datas != false ? $datas->screditBuyer : '' ?>">
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->