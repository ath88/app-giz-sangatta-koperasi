<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?= $save_url ?>" method="post" id="form_form" enctype="multipart/form-data">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="hsaprodiIdOld" value="<?= $datas != false ? $datas->hsaprodiId : '' ?>">

                        <div class="form-group">
                            <label>Jenis Saprodi</label>
                            <select class="form-control m-select2" name="hsaprodiJId">
                                <option value=""></option>
                                <?php
                                foreach ($ref_saprodi_jenis as $row) :
                                    echo '<option value="' . $row->jsaprodiId . '" ' . ($datas != false ? $datas->hsaprodiJId == $row->jsaprodiId ? 'selected' : '' : '') . '>' . $row->jsaprodiNama . '</option>';
                                endforeach;
                                ?>
                            </select>

                        </div>
                        
                        <div class="form-group">
                            <label>Nama Barang</label>
                            <input type="text" class="form-control" name="hsaprodiNama" placeholder="Nama Barang" aria-describedby="Nama Barang" value="<?= $datas != false ? $datas->hsaprodiNama : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Satuan</label>
                            <input type="text" class="form-control" name="hsaprodiSatuan" placeholder="Satuan" aria-describedby="Satuan" value="<?= $datas != false ? $datas->hsaprodiSatuan : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Tanggal</label>
                            <div class="input-group date">
                                <input type="text" class="form-control" name="hsaprodiDate" placeholder="Tanggal" id="kt_datepicker_1" aria-describedby="Tanggal" value="<?= $datas != false ? $datas->hsaprodiDate : '' ?>">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Harga Satuan</label>
                            <input type="text" class="form-control" name="hsaprodiHarga" placeholder="Harga Satuan" aria-describedby="Harga Satuan" value="<?= $datas != false ? $datas->hsaprodiHarga : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Keterangan</label>
                            <input type="text" class="form-control" name="hsaprodiKet" placeholder="Keterangan" aria-describedby="Keterangan" value="<?= $datas != false ? $datas->hsaprodiKet : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>File</label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="hsaprodiFile" id="custom-file-input">
                                <label class="custom-file-label" for="customFile">
                                    Pilih Berkas
                                </label>
                                <span class="m-form__help">
                                    Filetipe: *.pdf,*.jpg | Maks. Size: 15MB <?= $datas != false ? ' | File: '. $datas->hsaprodiFile : '' ?>
                                </span>
                            </div>
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->