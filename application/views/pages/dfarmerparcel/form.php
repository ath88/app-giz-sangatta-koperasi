<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?= $save_url ?>" method="post" id="form_form">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="parcelIdOld" value="<?= $datas != false ? $datas->parcelId : '' ?>">

                        <div class="form-group">
                            <label>Petani</label>
                            <select class="form-control m-select2" name="parcelFarmerId">
                                <option value=""></option>
                                <?php
                                foreach ($ref_farmer as $row) :
                                    echo '<option value="' . $row->farmerId . '" ' . ($datas != false ? $datas->parcelFarmerId == $row->farmerId ? 'selected' : '' : '') . '>' . $row->farmerName . '</option>';
                                endforeach;
                                ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>NumberXY</label>
                            <input type="text" class="form-control" name="parcelNumberXY" placeholder="NumberXY" aria-describedby="NumberXY" value="<?= $datas != false ? $datas->parcelNumberXY : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Geo-Langitude</label>
                            <input type="text" class="form-control" name="parcelGeoLang" placeholder="Geo-Langitude" aria-describedby="Geo-Langitude" value="<?= $datas != false ? $datas->parcelGeoLang : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Geo-Latitude</label>
                            <input type="text" class="form-control" name="parcelGeoLat" placeholder="Geo-Latitude" aria-describedby="Geo-Latitude" value="<?= $datas != false ? $datas->parcelGeoLat : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Alamat</label>
                            <input type="text" class="form-control" name="parcelAddress" placeholder="Alamat" aria-describedby="Alamat" value="<?= $datas != false ? $datas->parcelAddress : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Desa</label>
                            <select class="form-control m-select2" name="parcelVillage">
                                <option value=""></option>
                                <?php
                                foreach ($ref_wilayah as $row) :
                                    echo '<option value="' . $row->ID_WILAYAH . '" ' . ($datas != false ? $datas->parcelVillage == $row->ID_WILAYAH ? 'selected' : '' : '') . '>' . $row->NAMA_WILAYAH . ' (' . $row->NAMA_WILAYAH_2 . ')</option>';
                                endforeach;
                                ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Status kepemilikan</label>
                            <select class="form-control m-select2" name="parcelLsId">
                                <option value=""></option>
                                <?php
                                foreach ($ref_landstatus as $row) :
                                    echo '<option value="' . $row->lsId . '" ' . ($datas != false ? $datas->parcelLsId == $row->lsId ? 'selected' : '' : '') . '>' . $row->lsName . '</option>';
                                endforeach;
                                ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Total area</label>
                            <input type="text" class="form-control" name="parcelTotalArea" placeholder="Total area" aria-describedby="Total area" value="<?= $datas != false ? $datas->parcelTotalArea : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Total area tanaman</label>
                            <input type="text" class="form-control" name="parcelPlantedArea" placeholder="Total area tanaman" aria-describedby="Total area tanaman" value="<?= $datas != false ? $datas->parcelPlantedArea : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Tahun tanam</label>
                            <select class="form-control m-select2" name="parcelPlatingYear">
                                <option value=""></option>
                                <?php
                                for ($i = date('Y'); $i >= 1970; $i--) :
                                    echo '<option value="' . $i . '" ' . ($datas != false ? $datas->parcelPlatingYear == $i ? 'selected' : '' : '') . '>' . $i . '</option>';
                                endfor;
                                ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Tutupan Lahan</label>
                            <select class="form-control m-select2" name="parcelLbpId">
                                <option value=""></option>
                                <?php
                                foreach ($ref_landbeforeplanting as $row) :
                                    echo '<option value="' . $row->lbpId . '" ' . ($datas != false ? $datas->parcelLbpId == $row->lbpId ? 'selected' : '' : '') . '>' . $row->lbpName . '</option>';
                                endforeach;
                                ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Jenis tanaman utama</label>
                            <select class="form-control m-select2" name="parcelSpecId">
                                <option value=""></option>
                                <?php
                                foreach ($ref_species as $row) :
                                    echo '<option value="' . $row->specId . '" ' . ($datas != false ? $datas->parcelSpecId == $row->specId ? 'selected' : '' : '') . '>' . $row->specName . '</option>';
                                endforeach;
                                ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Jumlah produksi</label>
                            <input type="text" class="form-control" name="parcelProductivity" placeholder="Jumlah produksi" aria-describedby="Jumlah produksi" value="<?= $datas != false ? $datas->parcelProductivity : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Produsen benih</label>
                            <input type="text" class="form-control" name="parcelOriginMaterial" placeholder="Produsen benih" aria-describedby="Produsen benih" value="<?= $datas != false ? $datas->parcelOriginMaterial : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Jumlah pohon/tanaman</label>
                            <input type="text" class="form-control" name="parcelTree" placeholder="Jumlah pohon/tanaman" aria-describedby="Jumlah pohon/tanaman" value="<?= $datas != false ? $datas->parcelTree : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Model tanam</label>
                            <select class="form-control m-select2" name="parcelCropId">
                                <option value=""></option>
                                <?php
                                foreach ($ref_cropping as $row) :
                                    echo '<option value="' . $row->cropId . '" ' . ($datas != false ? $datas->parcelCropId == $row->cropId ? 'selected' : '' : '') . '>' . $row->cropName . '</option>';
                                endforeach;
                                ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Tipe tanah</label>
                            <select class="form-control m-select2" name="parcelStId">
                                <option value=""></option>
                                <?php
                                foreach ($ref_soiltype as $row) :
                                    echo '<option value="' . $row->stId . '" ' . ($datas != false ? $datas->parcelStId == $row->stId ? 'selected' : '' : '') . '>' . $row->stName . '</option>';
                                endforeach;
                                ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Tanaman Lainnya</label>
                            <div class="checkbox-inline">
                                <label class="checkbox">
                                    <input type="checkbox" name="parcelOtherPangan" value="1" <?= $datas != false ? $datas->parcelOtherPangan == 1 ? 'checked' : '' : '' ?> /> Tanaman Pangan
                                    <span></span>
                                </label>
                                <label class="checkbox">
                                    <input type="checkbox" name="parcelOtherSawit" value="1" <?= $datas != false ? $datas->parcelOtherSawit == 1 ? 'checked' : '' : '' ?> /> Sawit
                                    <span></span>
                                </label>
                                <label class="checkbox">
                                    <input type="checkbox" name="parcelOtherKaret" value="1" <?= $datas != false ? $datas->parcelOtherKaret == 1 ? 'checked' : '' : '' ?> /> Karet
                                    <span></span>
                                </label>
                                <label class="checkbox">
                                    <input type="checkbox" name="parcelOtherKakao" value="1" <?= $datas != false ? $datas->parcelOtherKakao == 1 ? 'checked' : '' : '' ?> /> Kakao
                                    <span></span>
                                </label>
                                <label class="checkbox">
                                    <input type="checkbox" name="parcelOtherLada" value="1" <?= $datas != false ? $datas->parcelOtherLada == 1 ? 'checked' : '' : '' ?> /> Lada
                                    <span></span>
                                </label>
                                <label class="checkbox">
                                    <input type="checkbox" name="parcelOtherKelapa" value="1" <?= $datas != false ? $datas->parcelOtherKelapa == 1 ? 'checked' : '' : '' ?> /> Kelapa
                                    <span></span>
                                </label>
                                <label class="checkbox">
                                    <input type="checkbox" name="parcelOtherAren" value="1" <?= $datas != false ? $datas->parcelOtherAren == 1 ? 'checked' : '' : '' ?> /> Aren
                                    <span></span>
                                </label>
                                <label class="checkbox">
                                    <input type="checkbox" name="parcelOtherOther" value="1" <?= $datas != false ? $datas->parcelOtherOther == 1 ? 'checked' : '' : '' ?> /> Lainnya
                                    <span></span>
                                </label>
                            </div>
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->