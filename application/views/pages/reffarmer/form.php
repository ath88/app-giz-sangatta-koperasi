<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?= $save_url ?>" method="post" id="form_form" enctype="multipart/form-data">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="farmerIdOld" value="<?= $datas != false ? $datas->farmerId : '' ?>">

                        <div class="form-group">
                            <label>Nomor Induk Kependudukan</label>
                            <input type="text" class="form-control" name="farmerNIK" placeholder="Nomor Induk Kependudukan" aria-describedby="Nomor Induk Kependudukan" value="<?= $datas != false ? $datas->farmerNIK : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Nama Petani (Nama KTP)</label>
                            <input type="text" class="form-control" name="farmerName" placeholder="Nama" aria-describedby="Nama" value="<?= $datas != false ? $datas->farmerName : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Jenis Kelamin</label>
                            <div class="m-radio-inline">
                                <label class="m-radio">
                                    <input type="radio" name="farmerGender" value="L" <?= $datas !== false ? $datas->farmerGender == 'L' ? 'checked' : '' : '' ?>>
                                    Laki-laki
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="farmerGender" value="P" <?= $datas !== false ? $datas->farmerGender == 'P' ? 'checked' : '' : '' ?>>
                                    Perempuan
                                    <span></span>
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Status Keluarga</label>
                            <div class="m-radio-inline">
                                <label class="m-radio">
                                    <input type="radio" name="farmerStatusKeluarga" value="AYAH" <?= $datas !== false ? $datas->farmerStatusKeluarga == 'AYAH' ? 'checked' : '' : '' ?>>
                                    AYAH
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="farmerStatusKeluarga" value="IBU" <?= $datas !== false ? $datas->farmerStatusKeluarga == 'IBU' ? 'checked' : '' : '' ?>>
                                    IBU
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="farmerStatusKeluarga" value="ANAK" <?= $datas !== false ? $datas->farmerStatusKeluarga == 'ANAK' ? 'checked' : '' : '' ?>>
                                    ANAK
                                    <span></span>
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Jumlah Keluarga</label>
                            <input type="text" class="form-control" name="farmetJumKeluarga" placeholder="Jumlah Keluarga" aria-describedby="Jumlah Keluarga" value="<?= $datas != false ? $datas->farmetJumKeluarga : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Jenjang Pendidikan</label>
                            <div class="m-radio-inline">
                                <label class="m-radio">
                                    <input type="radio" name="farmerJenjangPendidikan" value="SD" <?= $datas !== false ? $datas->farmerJenjangPendidikan == 'SD' ? 'checked' : '' : '' ?>>
                                    SD
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="farmerJenjangPendidikan" value="SMP" <?= $datas !== false ? $datas->farmerJenjangPendidikan == 'SMP' ? 'checked' : '' : '' ?>>
                                    SMP
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="farmerJenjangPendidikan" value="SMA" <?= $datas !== false ? $datas->farmerJenjangPendidikan == 'SMA' ? 'checked' : '' : '' ?>>
                                    SMA
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="farmerJenjangPendidikan" value="D1" <?= $datas !== false ? $datas->farmerJenjangPendidikan == 'D1' ? 'checked' : '' : '' ?>>
                                    D1
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="farmerJenjangPendidikan" value="D2" <?= $datas !== false ? $datas->farmerJenjangPendidikan == 'D2' ? 'checked' : '' : '' ?>>
                                    D2
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="farmerJenjangPendidikan" value="D3" <?= $datas !== false ? $datas->farmerJenjangPendidikan == 'D3' ? 'checked' : '' : '' ?>>
                                    D3
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="farmerJenjangPendidikan" value="S1/D4" <?= $datas !== false ? $datas->farmerJenjangPendidikan == 'S1/D4' ? 'checked' : '' : '' ?>>
                                    S1/D4
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="farmerJenjangPendidikan" value="S2" <?= $datas !== false ? $datas->farmerJenjangPendidikan == 'S2' ? 'checked' : '' : '' ?>>
                                    S2
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="farmerJenjangPendidikan" value="S3" <?= $datas !== false ? $datas->farmerJenjangPendidikan == 'S3' ? 'checked' : '' : '' ?>>
                                    S3
                                    <span></span>
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Tanggal Lahir</label>
                            <div class="input-group date">
                                <input type="text" class="form-control" placeholder="Pilih tanggal" id="kt_datepicker_1" name="farmerDateBirth" value="<?= $datas != false ? $datas->farmerDateBirth : '' ?>" />
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Tempat Lahir</label>
                            <input type="text" class="form-control" name="farmerPlaceBirth" placeholder="Tempat Lahir" aria-describedby="Tempat Lahir" value="<?= $datas != false ? $datas->farmerPlaceBirth : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Kota Lahir</label>
                            <select class="form-control m-select2" name="farmerCityBirth">
                                <option value=""></option>
                                <?php
                                foreach ($ref_wilayah as $row) :
                                    echo '<option value="' . $row->ID_WILAYAH . '" ' . ($datas != false ? $datas->farmerCityBirth == $row->ID_WILAYAH ? 'selected' : '' : '') . '>' . $row->NAMA_WILAYAH . ' (' . $row->NAMA_WILAYAH_2 . ')</option>';
                                endforeach;
                                ?>
                            </select>

                        </div>

                        <div class="form-group">
                            <label>Alamat Domisili</label>
                            <input type="text" class="form-control" name="farmerAddress" placeholder="Alamat Domisili" aria-describedby="Alamat Domisili" value="<?= $datas != false ? $datas->farmerAddress : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Kota Domisili</label>
                            <select class="form-control m-select2" name="farmerCityAddress">
                                <option value=""></option>
                                <?php
                                foreach ($ref_wilayah as $row) :
                                    echo '<option value="' . $row->ID_WILAYAH . '" ' . ($datas != false ? $datas->farmerCityAddress == $row->ID_WILAYAH ? 'selected' : '' : '') . '>' . $row->NAMA_WILAYAH . ' (' . $row->NAMA_WILAYAH_2 . ')</option>';
                                endforeach;
                                ?>
                            </select>

                        </div>

                        <div class="form-group">
                            <label>
                                File Komitment
                            </label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="berkas" id="custom-file-input">
                                <label class="custom-file-label" for="customFile">
                                    Pilih Berkas
                                </label>
                                <span class="m-form__help">
                                    Filetipe: *.pdf,*.jpg | Maks. Size: 15MB <?= $datas != false ? ' | File: '. $datas->farmerCommitment : '' ?>
                                </span>
                            </div>
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->