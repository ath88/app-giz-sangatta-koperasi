<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?= $save_url ?>" method="post" id="form_form">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="envmIdOld" value="<?= $datas != false ? $datas->envmId : '' ?>">

                        <div class="form-group">
                            <label>Persil</label>
                            <select class="form-control m-select2" name="envmParcelId">
                                <option value=""></option>
                                <?php
                                foreach ($d_farmer_parcel as $row) :
                                    echo '<option value="' . $row->parcelId . '" ' . ($datas != false ? $datas->envmParcelId == $row->parcelId ? 'selected' : '' : '') . '>' . $row->farmerName . ' - ' . $row->parcelTotalArea . '</option>';
                                endforeach;
                                ?>
                            </select>

                        </div>

                        <div class="form-group">
                            <label>
                                File SPPL
                            </label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="envmFile" id="custom-file-input">
                                <label class="custom-file-label" for="customFile">
                                    Pilih Berkas
                                </label>
                                <span class="m-form__help">
                                    Filetipe: *.pdf,*.jpg | Maks. Size: 15MB <?= $datas != false ? ' | File: ' . $datas->envmFile : '' ?>
                                </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Tanggal</label>
                            <div class="input-group date">
                                <input type="text" class="form-control" name="envmDate" placeholder="Tanggal" id="kt_datepicker_1" aria-describedby="Tanggal" value="<?= $datas != false ? $datas->envmDate : '' ?>">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Aktivitas</label>
                            <input type="text" class="form-control" name="envmActivity" placeholder="Aktivitas" aria-describedby="Aktivitas" value="<?= $datas != false ? $datas->envmActivity : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Ordinat Bujur</label>
                            <input type="text" class="form-control" name="envmLocLang" placeholder="Langitude" aria-describedby="Langitude" value="<?= $datas != false ? $datas->envmLocLang : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Ordinat Lintang</label>
                            <input type="text" class="form-control" name="envmLocLat" placeholder="Latitude" aria-describedby="Latitude" value="<?= $datas != false ? $datas->envmLocLat : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Organisasi terlibat</label>
                            <select class="form-control m-select2" name="envmOrganization">
                                <option value=""></option>
                                <option value="Pemerintah" <?= $datas != false ? $datas->envmOrganization == 'Pemerintah' ? 'selected' : '' : '' ?>>Pemerintah</option>
                                <option value="LSM" <?= $datas != false ? $datas->envmOrganization == 'LSM' ? 'selected' : '' : '' ?>>LSM</option>
                                <option value="NGO" <?= $datas != false ? $datas->envmOrganization == 'NGO' ? 'selected' : '' : '' ?>>NGO</option>
                                <option value="Dll" <?= $datas != false ? $datas->envmOrganization == 'Dll' ? 'selected' : '' : '' ?>>dll</option>
                            </select>

                        </div>

                        <div class="form-group">
                            <label>Penanganan Lingkungan ?</label>
                            <div class="m-radio-inline">
                                <label class="m-radio">
                                    <input type="radio" name="envmIsFirePrevention" value="1" <?= $datas !== false ? $datas->envmIsFirePrevention == '1' ? 'checked' : '' : '' ?>>
                                    Ya
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="envmIsFirePrevention" value="0" <?= $datas !== false ? $datas->envmIsFirePrevention == '0' ? 'checked' : '' : '' ?>>
                                    Lainnya
                                    <span></span>
                                </label>
                            </div>
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->