<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?= $save_url ?>" method="post" id="form_form">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="parcelwIdOld" value="<?= $datas != false ? $datas->parcelwId : '' ?>">

                        <div class="form-group">
                            <label>Persil</label>
                            <select class="form-control m-select2" name="parcelwParcelId">
                                <option value=""></option>
                                <?php
                                foreach ($d_farmer_parcel as $row) :
                                    echo '<option value="' . $row->parcelId . '" ' . ($datas != false ? $datas->parcelwParcelId == $row->parcelId ? 'selected' : '' : '') . '>' . $row->farmerName . ' - ' . $row->parcelTotalArea . '</option>';
                                endforeach;
                                ?>
                            </select>

                        </div>

                        <div class="form-group">
                            <label>Pekerja</label>
                            <select class="form-control m-select2" name="parcelwWorkerNIK">
                                <option value=""></option>
                                <?php
                                foreach ($ref_worker as $row) :
                                    echo '<option value="' . $row->workerNIK . '" ' . ($datas != false ? $datas->parcelwWorkerNIK == $row->workerNIK ? 'selected' : '' : '') . '>' . $row->workerNIK . ' - ' . $row->workerName . '</option>';
                                endforeach;
                                ?>
                            </select>

                        </div>

                        <div class="form-group">
                            <label>Jenis Pekerja</label>
                            <select class="form-control m-select2" name="parcelwCatId">
                                <option value=""></option>
                                <?php
                                foreach ($ref_worker_category as $row) :
                                    echo '<option value="' . $row->wcId . '" ' . ($datas != false ? $datas->parcelwCatId == $row->wcId ? 'selected' : '' : '') . '>' . $row->wcName . '</option>';
                                endforeach;
                                ?>
                            </select>

                        </div>

                        <div class="form-group">
                            <label>Jenis Gaji</label>
                            <select class="form-control m-select2" name="parcelwWageId">
                                <option value=""></option>
                                <?php
                                foreach ($ref_worker_wage as $row) :
                                    echo '<option value="' . $row->wwId . '" ' . ($datas != false ? $datas->parcelwWageId == $row->wwId ? 'selected' : '' : '') . '>' . $row->wwName . '</option>';
                                endforeach;
                                ?>
                            </select>

                        </div>

                        <div class="form-group">
                            <label>TMT Kerja</label>
                            <div class="input-group date">
                                <input type="text" class="form-control" placeholder="Pilih tanggal" id="kt_datepicker_1" name="parcelwTglAwal" value="<?= $datas != false ? $datas->parcelwTglAwal : '' ?>" />
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>TMT Status</label>
                            <div class="input-group date">
                                <input type="text" class="form-control" placeholder="Pilih tanggal" id="kt_datepicker_2" name="parcelwTglStatus" value="<?= $datas != false ? $datas->parcelwTglStatus : '' ?>" />
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Gaji</label>
                            <input type="text" class="form-control" name="parcelwWage" placeholder="Gaji" aria-describedby="Gaji" value="<?= $datas != false ? $datas->parcelwWage : '' ?>">
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->