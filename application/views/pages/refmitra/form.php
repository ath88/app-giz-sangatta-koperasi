<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?= $save_url ?>" method="post" id="form_form">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="mitraIdOld" value="<?= $datas != false ? $datas->mitraId : '' ?>">

                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" class="form-control" name="mitraNama" placeholder="Nama" aria-describedby="Nama" value="<?= $datas != false ? $datas->mitraNama : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Jenis Mitra</label>
                            <div class="m-radio-inline">
                                <label class="m-radio">
                                    <input type="radio" name="mitraJenis" value="PKS" <?= $datas !== false ? $datas->mitraJenis == 'PKS' ? 'checked' : '' : '' ?>>
                                    PKS
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="mitraJenis" value="SUPPLIER" <?= $datas !== false ? $datas->mitraJenis == 'SUPPLIER' ? 'checked' : '' : '' ?>>
                                    SUPPLIER
                                    <span></span>
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Alamat</label>
                            <input type="text" class="form-control" name="mitraAlamat" placeholder="Alamat" aria-describedby="Alamat" value="<?= $datas != false ? $datas->mitraAlamat : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Kontak</label>
                            <input type="text" class="form-control" name="mitraKontak" placeholder="Kontak" aria-describedby="Kontak" value="<?= $datas != false ? $datas->mitraKontak : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" class="form-control" name="mitraEmail" placeholder="Email" aria-describedby="Email" value="<?= $datas != false ? $datas->mitraEmail : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Status</label>
                            <input type="text" class="form-control" name="mitraStatus" placeholder="Status" aria-describedby="Status" value="<?= $datas != false ? $datas->mitraStatus : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Keterangan</label>
                            <input type="text" class="form-control" name="mitraKet" placeholder="Keterangan" aria-describedby="Keterangan" value="<?= $datas != false ? $datas->mitraKet : '' ?>">
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->