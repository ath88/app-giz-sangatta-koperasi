<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-actions">
                            <a href="<?= $slip_url ?>" class="btn btn-outline-success">
                                <span>
                                    <i class="flaticon2-file"></i>
                                    <span>Slip</span>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">

                    <!--begin::Section-->
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>No</th>
                                            <th>Keterangan</th>
                                            <th>Tanggal</th>
                                            <th>D/K</th>
                                            <th>Nominal</th>
                                            <th>Saldo</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if ($datas != false) {
                                            $i = 1;
                                            $saldo = 0;
                                            foreach ($datas as $row) {
                                                $saldo = $saldo + ($row->DK == 'K' ? $row->sumTsale : ($row->sumTsale * -1))
                                        ?>
                                                <tr>
                                                    <th scope="row"><?= $i++ ?></th>
                                                    <td><?= $row->Ket ?></td>
                                                    <td><?= $row->tsaleDate ?></td>
                                                    <td><?= $row->DK ?></td>
                                                    <td align="right"><?= number_format($row->sumTsale, 2) ?></td>
                                                    <td align="right"><?= number_format($saldo, 2) ?></td>
                                                </tr>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--end::Section-->
                </div>
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->