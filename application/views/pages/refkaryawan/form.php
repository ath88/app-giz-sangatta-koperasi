<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?= $save_url ?>" method="post" id="form_form">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="krynIdOld" value="<?= $datas != false ? $datas->krynId : '' ?>">

                        <div class="form-group">
                            <label>Nomor Induk Kependudukan</label>
                            <input type="text" class="form-control" name="krynNIK" placeholder="Nomor Induk Kependudukan" aria-describedby="Nomor Induk Kependudukan" value="<?= $datas != false ? $datas->krynNIK : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" class="form-control" name="krynNama" placeholder="Nama" aria-describedby="Nama" value="<?= $datas != false ? $datas->krynNama : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Jenis Kelamin</label>
                            <div class="m-radio-inline">
                                <label class="m-radio">
                                    <input type="radio" name="krynGender" value="L" <?= $datas !== false ? $datas->krynGender == 'L' ? 'checked' : '' : '' ?>>
                                    Laki-laki
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="krynGender" value="P" <?= $datas !== false ? $datas->krynGender == 'P' ? 'checked' : '' : '' ?>>
                                    Perempuan
                                    <span></span>
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Tempat Lahir</label>
                            <select class="form-control m-select2" name="krynTempatLahir">
                                <option value=""></option>
                                <?php
                                foreach ($ref_wilayah as $row) :
                                    echo '<option value="' . $row->ID_WILAYAH . '" ' . ($datas != false ? $row->ID_WILAYAH == $datas->krynTempatLahir ? 'selected' : '' : '') . '>' . $row->NAMA_WILAYAH . '</option>';
                                endforeach;
                                ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Tanggal Lahir</label>
                            <input type="text" class="form-control" name="krynTanggalLahir" id="kt_datepicker_1" placeholder="Tanggal Lahir" aria-describedby="Tanggal Lahir" value="<?= $datas != false ? $datas->krynTanggalLahir : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Alamat</label>
                            <input type="text" class="form-control" name="krynAlamat" placeholder="Alamat" aria-describedby="Alamat" value="<?= $datas != false ? $datas->krynAlamat : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Jabatan</label>
                            <input type="text" class="form-control" name="krynJabatan" placeholder="Jabatan" aria-describedby="Jabatan" value="<?= $datas != false ? $datas->krynJabatan : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Tanggal Masuk</label>
                            <input type="text" class="form-control" name="krynTanggalMasuk" id="kt_datepicker_2" placeholder="Tanggal Masuk" aria-describedby="Tanggal Masuk" value="<?= $datas != false ? $datas->krynTanggalMasuk : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Tanggal Keluar</label>
                            <input type="text" class="form-control" name="krynTanggalKeluar" id="kt_datepicker_3" placeholder="Tanggal Keluar" aria-describedby="Tanggal Keluar" value="<?= $datas != false ? $datas->krynTanggalKeluar : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Status Kawin</label>
                            <div class="m-radio-inline">
                                <label class="m-radio">
                                    <input type="radio" name="krynStatusKawin" value="BELUM KAWIN" <?= $datas !== false ? $datas->krynStatusKawin == 'BELUM KAWIN' ? 'checked' : '' : '' ?>>
                                    BELUM KAWIN
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="krynStatusKawin" value="KAWIN" <?= $datas !== false ? $datas->krynStatusKawin == 'KAWIN' ? 'checked' : '' : '' ?>>
                                    KAWIN
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="krynStatusKawin" value="DUDA" <?= $datas !== false ? $datas->krynStatusKawin == 'DUDA' ? 'checked' : '' : '' ?>>
                                    DUDA
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="krynStatusKawin" value="JANDA" <?= $datas !== false ? $datas->krynStatusKawin == 'JANDA' ? 'checked' : '' : '' ?>>
                                    JANDA
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label>Kategori</label>
                            <input type="text" class="form-control" name="krynKategori" placeholder="Kategori" aria-describedby="Kategori" value="<?= $datas != false ? $datas->krynKategori : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Status</label>
                            <input type="text" class="form-control" name="krynStatus" placeholder="Status" aria-describedby="Status" value="<?= $datas != false ? $datas->krynStatus : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Keterangan</label>
                            <input type="text" class="form-control" name="krynKeterangan" placeholder="Keterangan" aria-describedby="Keterangan" value="<?= $datas != false ? $datas->krynKeterangan : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>
                                File SK
                            </label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="berkas" id="custom-file-input">
                                <label class="custom-file-label" for="customFile">
                                    Pilih Berkas
                                </label>
                                <span class="m-form__help">
                                    Filetipe: *.pdf,*.jpg | Maks. Size: 15MB <?= $datas != false ? ' | File: '. $datas->krynSK : '' ?>
                                </span>
                            </div>
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->