<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?= $save_url ?>" method="post" id="form_form">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="conflictIdOld" value="<?= $datas != false ? $datas->conflictId : '' ?>">

                        <div class="form-group">
                            <label>Persil Petani</label>
                            <select class="form-control m-select2" name="conflictParcelId">
                                <option value=""></option>
                                <?php
                                foreach ($d_farmer_parcel as $row) :
                                    echo '<option value="' . $row->parcelId . '" ' . ($datas != false ? $datas->conflictParcelId == $row->parcelId ? 'selected' : '' : '') . '>' . $row->farmerName . ' - ' . $row->parcelTotalArea . '</option>';
                                endforeach;
                                ?>
                            </select>

                        </div>

                        <div class="form-group">
                            <label>Tanggal</label>
                            <div class="input-group date">
                                <input type="text" class="form-control" name="conflictDate" placeholder="Tanggal" id="kt_datepicker_1" aria-describedby="Tanggal" value="<?= $datas != false ? $datas->conflictDate : '' ?>">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Pihak Terlibat</label>
                            <input type="text" class="form-control" name="conflictParties" placeholder="Parties" aria-describedby="Parties" value="<?= $datas != false ? $datas->conflictParties : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Deskripsi</label>
                            <input type="text" class="form-control" name="conflictDescription" placeholder="Deskripsi" aria-describedby="Deskripsi" value="<?= $datas != false ? $datas->conflictDescription : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Selesai ?</label>
                            <div class="m-radio-inline">
                                <label class="m-radio">
                                    <input type="radio" name="confilctStatus" value="1" <?= $datas !== false ? $datas->confilctStatus == 1 ? 'checked' : '' : '' ?>>
                                    Selesai
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="confilctStatus" value="0" <?= $datas !== false ? $datas->confilctStatus == 0 ? 'checked' : '' : '' ?>>
                                    Belum Selesai
                                    <span></span>
                                </label>
                            </div>
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->