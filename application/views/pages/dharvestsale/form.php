<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?= $save_url ?>" method="post" id="form_form">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="hsaleIdOld" value="<?= $datas != false ? $datas->hsaleId : '' ?>">

                        <div class="form-group">
                            <label>Tanggal Jual</label>
                            <div class="input-group date">
                                <input type="text" class="form-control" name="hsaleDate" placeholder="Tanggal" id="kt_datepicker_1" aria-describedby="Tanggal" value="<?= $datas != false ? $datas->hsaleDate : '' ?>">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label>Petani | Persil | Tanggal Panen</label>
                            <select class="form-control m-select2" name="hsaleHarvestId" id="hsaleHarvestId">
                                <option value=""></option>
                                <?php
                                foreach ($d_harvest as $row) :
                                    echo '<option value="' . $row->harvestId . '" ' . ($datas != false ? $row->harvestId == $datas->hsaleHarvestId ? 'selected' : '' : '') . '>' . $row->farmerName . ' | ' . $row->parcelTotalArea . ' | ' . $row->harvestDate . '</option>';
                                endforeach;
                                ?>
                            </select>

                        </div>

                        <div class="form-group">
                            <label>Transport</label>
                            <select class="form-control m-select2" name="hsaleTruckId">
                                <option value=""></option>
                                <?php
                                foreach ($ref_truck as $row) :
                                    echo '<option value="' . $row->truckId . '" ' . ($datas != false ? $row->truckId == $datas->hsaleTruckId ? 'selected' : '' : '') . '>' . $row->truckName . ' | ' . $row->truckNumber . '</option>';
                                endforeach;
                                ?>
                            </select>

                        </div>

                        <div class="form-group">
                            <label>Pabrik | Tanggal PKS</label>
                            <select class="form-control m-select2" name="hsaleContId" id="hsaleContId">
                                <option value=""></option>
                                <?php
                                foreach ($d_contract as $row) :
                                    echo '<option value="' . $row->contId . '" ' . ($datas != false ? $row->contId == $datas->hsaleContId ? 'selected' : '' : '') . '>' . $row->millName . ' | ' . $row->contDate . '</option>';
                                endforeach;
                                ?>
                            </select>

                        </div>

                        <div class="form-group">
                            <label>Biaya Transport</label>
                            <input type="text" class="form-control" name="hsaleTransport" placeholder="Biaya Transport" aria-describedby="Biaya Transport" value="<?= $datas != false ? $datas->hsaleTransport : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Panen JJG</label>
                            <input type="text" class="form-control" id="panen_jjg" readonly value="<?= $datas != false ? $datas->harvestJJG : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Panen Brondol</label>
                            <input type="text" class="form-control" id="panen_brondol" readonly value="<?= $datas != false ? $datas->harvestBrondol : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Total Panen</label>
                            <input type="text" class="form-control" id="total_panen" readonly value="<?= $datas != false ? ($datas->harvestJJG + $datas->harvestBrondol) : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Timbang JJG</label>
                            <input type="text" class="form-control" name="hsaleTJJG" id="hsaleTJJG" placeholder="Timbang JJG" aria-describedby="Timbang JJG" value="<?= $datas != false ? $datas->hsaleTJJG : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Timbang Brondol</label>
                            <input type="text" class="form-control" name="hsaleTBrondol" id="hsaleTBrondol" placeholder="Timbang Brondol" aria-describedby="Timbang Brondol" value="<?= $datas != false ? $datas->hsaleTBrondol : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Total Timbang</label>
                            <input type="text" class="form-control" id="total_timbang" readonly value="<?= $datas != false ? ($datas->hsaleTJJG + $datas->hsaleTBrondol) : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Selisih JJG</label>
                            <input type="text" class="form-control" name="hsaleTSJJG" id="hsaleTSJJG" placeholder="Selisih JJG" aria-describedby="Selisih JJG" value="<?= $datas != false ? $datas->hsaleTSJJG : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Selisih Brondol</label>
                            <input type="text" class="form-control" name="hsaleTSBrondol" id="hsaleTSBrondol" placeholder="Selisih Brondol" aria-describedby="Selisih Brondol" value="<?= $datas != false ? $datas->hsaleTSBrondol : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Total Panen</label>
                            <input type="text" class="form-control" id="total_selisih" readonly value="<?= $datas != false ? ($datas->hsaleTSJJG + $datas->hsaleTSBrondol) : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Netto JJG</label>
                            <input type="text" class="form-control" name="hsaleNJJG" id="hsaleNJJG" placeholder="Netto JJG" aria-describedby="Netto JJG" value="<?= $datas != false ? $datas->hsaleNJJG : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Netto Brondol</label>
                            <input type="text" class="form-control" name="hsaleNBrondol" id="hsaleNBrondol" placeholder="Netto Brondol" aria-describedby="Netto Brondol" value="<?= $datas != false ? $datas->hsaleNBrondol : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Total Panen</label>
                            <input type="text" class="form-control" id="total_netto" readonly value="<?= $datas != false ? ($datas->hsaleNJJG + $datas->hsaleNBrondol) : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Harga</label>
                            <input type="text" class="form-control" id="harga_kontrak" readonly value="<?= $datas != false ? $datas->contPrice : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Hasil Penjualan</label>
                            <input type="text" class="form-control" id="hasil_penjualan" readonly value="<?= $datas != false ? (($datas->hsaleNJJG + $datas->hsaleNBrondol) * $datas->contPrice) : '' ?>">
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->