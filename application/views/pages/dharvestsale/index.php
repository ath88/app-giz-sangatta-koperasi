<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-actions">
                            <a href="<?= $create_url ?>" class="btn btn-outline-primary">
                                <span>
                                    <i class="flaticon2-plus"></i>
                                    <span>Create</span>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="kt-portlet__body">

                    <!--begin::Section-->
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>No</th>
                                            <th>Tanggal Jual</th>
                                            <th>Panen</th>
                                            <th>Transport</th>
                                            <th>Kontrak</th>
                                            <th>Biaya Transport</th>
                                            <th nowrap>Panen</th>
                                            <th nowrap>Timbang</th>
                                            <th nowrap>Selisih</th>
                                            <th nowrap>Netto</th>
                                            <th>Harga</th>
                                            <th>Total Penjualan</th>

                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if ($datas != false) {
                                            $i = 1;
                                            foreach ($datas as $row) {
                                                $key = $this->encryptions->encode($row->hsaleId, $this->config->item('encryption_key'));
                                        ?>
                                                <tr>
                                                    <th scope="row"><?= $i++ ?></th>
                                                    <td><?= $row->hsaleDate ?></td>
                                                    <td><?= $row->farmerName . ' | ' . $row->parcelTotalArea . ' | ' . $row->harvestDate ?></td>
                                                    <td><?= $row->truckName . ' | ' . $row->truckNumber ?></td>
                                                    <td><?= $row->millName . ' | ' . $row->contDate ?></td>
                                                    <td><?= number_format($row->hsaleTransport, 0) ?></td>
                                                    <td>
                                                        JJG: <?= $row->harvestJJG ?><br />
                                                        Brondol: <?= $row->harvestBrondol ?><br />
                                                        Total: <?= $row->harvestJJG + $row->harvestBrondol ?><br />
                                                    </td>
                                                    <td>
                                                        JJG: <?= $row->hsaleTJJG ?><br />
                                                        Brondol: <?= $row->hsaleTBrondol ?><br />
                                                        Total: <?= $row->hsaleTJJG + $row->hsaleTBrondol ?><br />
                                                    </td>
                                                    <td>
                                                        JJG: <?= $row->hsaleTSJJG ?><br />
                                                        Brondol: <?= $row->hsaleTSBrondol ?><br />
                                                        Total: <?= $row->hsaleTSJJG + $row->hsaleTSBrondol ?><br />
                                                    </td>
                                                    <td>
                                                        JJG: <?= $row->hsaleNJJG ?><br />
                                                        Brondol: <?= $row->hsaleNBrondol ?><br />
                                                        Total: <?= $row->hsaleNJJG + $row->hsaleNBrondol ?><br />
                                                    </td>
                                                    <td><?= number_format($row->contPrice, 0) ?></td>
                                                    <td><?= number_format((($row->hsaleNJJG + $row->hsaleNBrondol) * $row->contPrice), 0) ?></td>
                                                    <td>
                                                        <a href="<?= $update_url . $key ?>" title="Update" class="btn btn-sm btn-outline-primary btn-elevate btn-circle btn-icon">
                                                            <span>
                                                                <i class="fa fa-pencil-alt"></i>
                                                            </span>
                                                        </a>
                                                        <a href="<?= $delete_url . $key ?>" title="Delete" id='ts_remove_row<?= $i; ?>' class="ts_remove_row btn btn-sm btn-outline-danger btn-elevate btn-circle btn-icon">
                                                            <span>
                                                                <i class="fa fa-trash-alt"></i>
                                                            </span>
                                                        </a>
                                                    </td>
                                                </tr>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <!--end::Section-->
                </div>
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->