<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?= $save_url ?>" method="post" id="form_form">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="saprodibIdOld" value="<?= $datas != false ? $datas->saprodibId : '' ?>">

                        <div class="form-group">
                            <label>Barang</label>
                            <select class="form-control m-select2" name="saprodibHsaprodiId">
                                <option value=""></option>
                                <?php
                                foreach ($ref_saprodi_harga as $row) :
                                    echo '<option value="' . $row->hsaprodiId . '" ' . ($datas != false ? $row->hsaprodiId == $datas->saprodibHsaprodiId ? 'selected' : '' : '') . '>' . $row->hsaprodiNama . '</option>';
                                endforeach;
                                ?>
                            </select>

                        </div>

                        <div class="form-group">
                            <label>Tanggal Beli</label>
                            <input type="text" class="form-control" name="saprodibDate" placeholder="Tanggal Beli" id="kt_datepicker_1" aria-describedby="Tanggal Beli" value="<?= $datas != false ? $datas->saprodibDate : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>No Pemesanan</label>
                            <input type="text" class="form-control" name="saprodibNoPesan" placeholder="No Pemesanan" aria-describedby="No Pemesanan" value="<?= $datas != false ? $datas->saprodibNoPesan : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Toko/Produsen/Distributor</label>
                            <input type="text" class="form-control" name="saprodibToko" placeholder="Toko/Produsen/Distributor" aria-describedby="Toko/Produsen/Distributor" value="<?= $datas != false ? $datas->saprodibToko : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Mata Uang</label>
                            <input type="text" class="form-control" name="saprodibCurr" placeholder="Mata Uang" aria-describedby="Mata Uang" value="<?= $datas != false ? $datas->saprodibCurr : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Harga Satuan</label>
                            <input type="text" class="form-control" name="saprodibHarga" placeholder="Harga Satuan" aria-describedby="Harga Satuan" value="<?= $datas != false ? $datas->saprodibHarga : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Jumlah Barang</label>
                            <input type="text" class="form-control" name="saprodibJum" placeholder="Jumlah Barang" aria-describedby="Jumlah Barang" value="<?= $datas != false ? $datas->saprodibJum : '' ?>">
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->