<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?= $save_url ?>" method="post" id="form_form" enctype="multipart/form-data">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="priceIdOld" value="<?= $datas != false ? $datas->priceId : '' ?>">

                        <div class="form-group">
                            <label>Tahun</label>
                            <input type="text" class="form-control" name="priceTahun" placeholder="Tahun" aria-describedby="Tahun" value="<?= $datas != false ? $datas->priceTahun : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Bulan</label>
                            <select class="form-control m-select2" name="priceBulan">
                                <option value=""></option>
                                <?php
                                for ($i = 1; $i <= 12; $i++) :
                                    echo "<option value='" . $i . "'>" . monthtoindo(substr(('0' . $i), -2)) . "</option>";
                                endfor;
                                ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Harga Umur 3 Tahun</label>
                            <input type="text" class="form-control" name="price3" placeholder="Harga Umur 3 Tahun" aria-describedby="Harga Umur 3 Tahun" value="<?= $datas != false ? $datas->price3 : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Harga Umur 4 Tahun</label>
                            <input type="text" class="form-control" name="price4" placeholder="Harga Umur 4 Tahun" aria-describedby="Harga Umur 4 Tahun" value="<?= $datas != false ? $datas->price4 : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Harga Umur 5 Tahun</label>
                            <input type="text" class="form-control" name="price5" placeholder="Harga Umur 5 Tahun" aria-describedby="Harga Umur 5 Tahun" value="<?= $datas != false ? $datas->price5 : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Harga Umur 6 Tahun</label>
                            <input type="text" class="form-control" name="price6" placeholder="Harga Umur 6 Tahun" aria-describedby="Harga Umur 6 Tahun" value="<?= $datas != false ? $datas->price6 : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Harga Umur 7 Tahun</label>
                            <input type="text" class="form-control" name="price7" placeholder="Harga Umur 7 Tahun" aria-describedby="Harga Umur 7 Tahun" value="<?= $datas != false ? $datas->price7 : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Harga Umur 8 Tahun</label>
                            <input type="text" class="form-control" name="price8" placeholder="Harga Umur 8 Tahun" aria-describedby="Harga Umur 8 Tahun" value="<?= $datas != false ? $datas->price8 : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Harga Umur 9 Tahun</label>
                            <input type="text" class="form-control" name="price9" placeholder="Harga Umur 9 Tahun" aria-describedby="Harga Umur 9 Tahun" value="<?= $datas != false ? $datas->price9 : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Harga Umur 10 - 25 Tahun</label>
                            <input type="text" class="form-control" name="price10" placeholder="Harga Umur 10 - 25 Tahun" aria-describedby="Harga Umur 10 - 25 Tahun" value="<?= $datas != false ? $datas->price10 : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Tanggal SK</label>
                            <div class="input-group date">
                                <input type="text" class="form-control" name="priceSKDate" id="kt_datepicker_1" placeholder="Tanggal SK" aria-describedby="Tanggal SK" value="<?= $datas != false ? $datas->priceSKDate : '' ?>">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Nomor SK</label>
                            <input type="text" class="form-control" name="priceSK" placeholder="Nomor SK" aria-describedby="Nomor SK" value="<?= $datas != false ? $datas->priceSK : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>File</label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="berkas" id="custom-file-input">
                                <label class="custom-file-label" for="customFile">
                                    Pilih Berkas
                                </label>
                                <span class="m-form__help">
                                    Filetipe: *.pdf,*.jpg | Maks. Size: 15MB <?= $datas != false ? ' | File: ' . $datas->priceFile : '' ?>
                                </span>
                            </div>
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->