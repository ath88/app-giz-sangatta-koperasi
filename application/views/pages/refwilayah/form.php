<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?= $save_url ?>" method="post" id="form_form">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="ID_WILAYAHOld" value="<?= $datas != false ? $datas->ID_WILAYAH : '' ?>">

                        <div class="form-group">
                            <label>ID_WILAYAH</label>
                            <input type="text" class="form-control" name="ID_WILAYAH" placeholder="ID_WILAYAH" aria-describedby="ID_WILAYAH" value="<?= $datas != false ? $datas->ID_WILAYAH : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>IDPARENT</label>
                            <select class="form-control m-select2" name="IDPARENT">
                                <option value=""></option>
                                <?php
                                foreach ($ref_wilayah as $row) :
                                    echo '<option value="' . $row->ID_WILAYAH . '" ' . ($datas != false ? $datas->ID_WILAYAH == $row->IDPARENT ? 'selected' : '' : '') . '>' . $row->ID_WILAYAH . '</option>';
                                endforeach;
                                ?>
                            </select>

                        </div>

                        <div class="form-group">
                            <label>NAMA_WILAYAH</label>
                            <input type="text" class="form-control" name="NAMA_WILAYAH" placeholder="NAMA_WILAYAH" aria-describedby="NAMA_WILAYAH" value="<?= $datas != false ? $datas->NAMA_WILAYAH : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>LEVEL</label>
                            <input type="text" class="form-control" name="LEVEL" placeholder="LEVEL" aria-describedby="LEVEL" value="<?= $datas != false ? $datas->LEVEL : '' ?>">
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->