<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?= $save_url ?>" method="post" id="form_form">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="truckIdOld" value="<?= $datas != false ? $datas->truckId : '' ?>">

                        <div class="form-group">
                            <label>Nama Pemilik</label>
                            <input type="text" class="form-control" name="truckName" placeholder="Nama Pemilik" aria-describedby="Nama Pemilik" value="<?= $datas != false ? $datas->truckName : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Alamat Pemilik</label>
                            <input type="text" class="form-control" name="truckAddress" placeholder="Alamat Pemilik" aria-describedby="Alamat Pemilik" value="<?= $datas != false ? $datas->truckAddress : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Kapasitas</label>
                            <input type="text" class="form-control" name="truckCapacity" placeholder="Kapasitas" aria-describedby="Kapasitas" value="<?= $datas != false ? $datas->truckCapacity : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Nomor Kendaraan</label>
                            <input type="text" class="form-control" name="truckNumber" placeholder="Nomor Kendaraan" aria-describedby="Nomor Kendaraan" value="<?= $datas != false ? $datas->truckNumber : '' ?>">
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->