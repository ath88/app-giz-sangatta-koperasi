<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?= $save_url ?>" method="post" id="form_form">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="contIdOld" value="<?= $datas != false ? $datas->contId : '' ?>">

                        <div class="form-group">
                            <label>Pabrik</label>
                            <select class="form-control m-select2" name="contMillId">
                                <option value=""></option>
                                <?php
                                foreach ($ref_mill as $row) :
                                    echo '<option value="' . $row->millId . '" ' . ($datas != false ? $datas->contMillId == $row->millId ? 'selected' : '' : '') . '>' . $row->millName . '</option>';
                                endforeach;
                                ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Harga TBS</label>
                            <select class="form-control m-select2" name="contPriceId">
                                <option value=""></option>
                                <?php
                                foreach ($ref_price as $row) :
                                    echo '<option value="' . $row->priceId . '" ' . ($datas != false ? $datas->contPriceId == $row->priceId ? 'selected' : '' : '') . '>' . monthtoindo(substr(('0' . $row->priceBulan), -2)) . ' - ' . $row->priceTahun . '</option>';
                                endforeach;
                                ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Tanggal Kontrak</label>
                            <div class="input-group date">
                                <input type="text" class="form-control" name="contDate" placeholder="Tanggal PO" id="kt_datepicker_1" aria-describedby="Tanggal PO" value="<?= $datas != false ? $datas->contDate : '' ?>">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Periode</label>
                            <input type="text" class="form-control" name="contPeriod" placeholder="Periode" aria-describedby="Periode" value="<?= $datas != false ? $datas->contPeriod : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Volume</label>
                            <input type="text" class="form-control" name="contVolume" placeholder="Volume" aria-describedby="Volume" value="<?= $datas != false ? $datas->contVolume : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Harga Aktual</label>
                            <input type="text" class="form-control" name="contPrice" placeholder="Harga Aktual" aria-describedby="Harga Aktual" value="<?= $datas != false ? $datas->contPrice : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>
                                File Kontrak
                            </label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="berkas" id="custom-file-input">
                                <label class="custom-file-label" for="customFile">
                                    Pilih Berkas
                                </label>
                                <span class="m-form__help">
                                    Filetipe: *.pdf,*.jpg | Maks. Size: 15MB <?= $datas != false ? ' | File: ' . $datas->contFile : '' ?>
                                </span>
                            </div>
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->