<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?= $save_url ?>" method="post" id="form_form">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="saprodisIdOld" value="<?= $datas != false ? $datas->saprodisId : '' ?>">

                        <div class="form-group">
                            <label>Tanggal Pembelian</label>
                            <div class="input-group date">
                                <input type="text" class="form-control" name="saprodisTanggal" placeholder="Tanggal" id="kt_datepicker_1" aria-describedby="Tanggal" value="<?= $datas != false ? $datas->saprodisTanggal : '' ?>">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Petani</label>
                            <select class="form-control m-select2" name="saprodisFarmerId">
                                <option value=""></option>
                                <?php
                                foreach ($farmer as $row) :
                                    echo '<option value="' . $row->farmerId . '" ' . ($datas != false ? $datas->saprodisFarmerId == $row->farmerId ? 'selected' : '' : '') . '>' . $row->farmerName . '</option>';
                                endforeach;
                                ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Nama (Non Anggota)</label>
                            <input type="text" class="form-control" name="saprodisNama" placeholder="Non Farmer" aria-describedby="Non Farmer" value="<?= $datas != false ? $datas->saprodisNama : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Barang</label>
                            <select class="form-control m-select2" id="saprodisHsaprodiId" name="saprodisHsaprodiId">
                                <option value=""></option>
                                <?php
                                foreach ($rsaprodi as $row) :
                                    echo '<option value="' . $row->hsaprodiId . '" ' . ($datas != false ? $datas->saprodisHsaprodiId == $row->hsaprodiId ? 'selected' : '' : '') . '>' . $row->hsaprodiNama . ' - ' . $row->hsaprodiHarga . '</option>';
                                endforeach;
                                ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Satuan</label>
                            <input type="text" class="form-control" readonly id="satuan" value="<?= $datas != false ? $datas->hsaprodiSatuan : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Harga Satuan</label>
                            <input type="text" class="form-control" readonly id="harga_satuan" value="<?= $datas != false ? $datas->saprodisHarSatuan : '0' ?>">
                        </div>

                        <div class="form-group">
                            <label>Jumlah</label>
                            <input type="text" class="form-control" id="saprodisJumlah" name="saprodisJumlah" placeholder="Jumlah" aria-describedby="Jumlah" value="<?= $datas != false ? $datas->saprodisJumlah : '0' ?>">
                        </div>

                        <div class="form-group">
                            <label>Total</label>
                            <input type="text" class="form-control" readonly id="total" value="<?= $datas != false ? $datas->saprodisTotal : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Pembayaran Kontan</label>
                            <input type="text" class="form-control" name="saprodisKontan" value="<?= $datas != false ? $datas->saprodisKontan : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Pembayaran Kredit</label>
                            <input type="text" class="form-control" name="saprodisKredit" value="<?= $datas != false ? $datas->saprodisKredit : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Angsuran 1</label>
                            <input type="text" class="form-control" name="saprodisAngsuran1" value="<?= $datas != false ? $datas->saprodisAngsuran1 : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Tanggal Bayar Angsuran 1</label>
                            <input type="text" class="form-control" id="kt_datepicker_2" name="saprodisAngsuranTgl1" value="<?= $datas != false ? $datas->saprodisAngsuranTgl1 : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Angsuran 2</label>
                            <input type="text" class="form-control" name="saprodisAngsuran2" value="<?= $datas != false ? $datas->saprodisAngsuran2 : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Tanggal Bayar Angsuran 2</label>
                            <input type="text" class="form-control" id="kt_datepicker_3" name="saprodisAngsuranTgl2" value="<?= $datas != false ? $datas->saprodisAngsuranTgl2 : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Angsuran 3</label>
                            <input type="text" class="form-control" name="saprodisAngsuran3" value="<?= $datas != false ? $datas->saprodisAngsuran3 : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Tanggal Bayar Angsuran 3</label>
                            <input type="text" class="form-control" id="kt_datepicker_3" name="saprodisAngsuranTgl3" value="<?= $datas != false ? $datas->saprodisAngsuranTgl3 : '' ?>">
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->