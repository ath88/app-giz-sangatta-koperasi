<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class refmitra extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/refmitra/';
        $this->_path_js = null;
        $this->_judul = 'Mitra & Rekanan';
        $this->_controller_name = 'refmitra';
        $this->_model_name = 'model_refmitra';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->get_ref_table('ref_mitra');
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['mitraId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->get_by_id('ref_mitra', $key);

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $mitraIdOld = $this->input->post('mitraIdOld');
        $this->form_validation->set_rules('mitraNama', 'Nama', 'trim|xss_clean|required');
        $this->form_validation->set_rules('mitraJenis', 'Jenis', 'trim|xss_clean|required');
        $this->form_validation->set_rules('mitraAlamat', 'Alamat', 'trim|xss_clean|required');
        $this->form_validation->set_rules('mitraKontak', 'Kontak', 'trim|xss_clean|required');
        $this->form_validation->set_rules('mitraEmail', 'Email', 'trim|xss_clean|valid_email');
        $this->form_validation->set_rules('mitraStatus', 'Status', 'trim|xss_clean');
        $this->form_validation->set_rules('mitraKet', 'Keterangan', 'trim|xss_clean');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $mitraNama = $this->input->post('mitraNama');
                $mitraJenis = $this->input->post('mitraJenis');
                $mitraAlamat = $this->input->post('mitraAlamat');
                $mitraKontak = $this->input->post('mitraKontak');
                $mitraEmail = $this->input->post('mitraEmail');
                $mitraStatus = $this->input->post('mitraStatus');
                $mitraKet = $this->input->post('mitraKet');


                $param = array(
                    'mitraNama' => $mitraNama,
                    'mitraJenis' => $mitraJenis,
                    'mitraAlamat' => $mitraAlamat,
                    'mitraKontak' => $mitraKontak,
                    'mitraEmail' => $mitraEmail,
                    'mitraStatus' => $mitraStatus,
                    'mitraKet' => $mitraKet,

                );

                if (empty($mitraIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('ref_mitra', $param);
                } else {
                    $key = array('mitraId' => $mitraIdOld);
                    $proses = $this->{$this->_model_name}->update('ref_mitra', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['mitraId' => $keyS];
        $proses = $this->{$this->_model_name}->delete('ref_mitra', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }
}
