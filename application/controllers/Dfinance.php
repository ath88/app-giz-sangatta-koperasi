<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class dfinance extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/dfinance/';
        $this->_path_js = null;
        $this->_judul = 'Keuangan';
        $this->_controller_name = 'dfinance';
        $this->_model_name = 'model_dfinance';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->all();
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $data['download_url'] = site_url($this->_controller_name . '/download') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;
        $data['ref_finance_type'] = $this->{$this->_model_name}->get_ref_table('ref_finance_type');
        $data['ref_farmer'] = $this->{$this->_model_name}->get_ref_table('ref_farmer');
        $data['ref_loan_type'] = $this->{$this->_model_name}->get_ref_table('ref_loan_type');
        $data['ref_saving_type'] = $this->{$this->_model_name}->get_ref_table('ref_saving_type');
        $data['ref_revenue_type'] = $this->{$this->_model_name}->get_ref_table('ref_revenue_type');
        $data['d_transportsale'] = $this->{$this->_model_name}->get_ref_table('d_transportsale');

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['finId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->by_id($key);
        $data['ref_finance_type'] = $this->{$this->_model_name}->get_ref_table('ref_finance_type');
        $data['ref_farmer'] = $this->{$this->_model_name}->get_ref_table('ref_farmer');
        $data['ref_loan_type'] = $this->{$this->_model_name}->get_ref_table('ref_loan_type');
        $data['ref_saving_type'] = $this->{$this->_model_name}->get_ref_table('ref_saving_type');
        $data['ref_revenue_type'] = $this->{$this->_model_name}->get_ref_table('ref_revenue_type');
        $data['d_transportsale'] = $this->{$this->_model_name}->get_ref_table('d_transportsale');

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $finIdOld = $this->input->post('finIdOld');
        $this->form_validation->set_rules('finFtId', 'Type Finance', 'trim|xss_clean|required');
        $this->form_validation->set_rules('finFarmerId', 'Farmer', 'trim|xss_clean|required');
        //$this->form_validation->set_rules('finLtId', 'Tipe Loan', 'trim|xss_clean');
        //$this->form_validation->set_rules('finSavetId', 'Tipe saving', 'trim|xss_clean');
        //$this->form_validation->set_rules('finRevtId', 'Tipe revenue', 'trim|xss_clean');
        //$this->form_validation->set_rules('finTsaleId', 'Penjualan', 'trim|xss_clean');
        //$this->form_validation->set_rules('finLoanId', 'Generate Loan ID', 'trim|xss_clean');
        $this->form_validation->set_rules('finDate', 'Tanggal', 'trim|xss_clean|required');
        $this->form_validation->set_rules('finDK', 'Debit/Kredit', 'trim|xss_clean|required');
        $this->form_validation->set_rules('finAmount', 'Nominal', 'trim|xss_clean|required');
        //$this->form_validation->set_rules('finLoanFile', 'Kontrak Loan', 'trim|xss_clean');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $finFtId = $this->input->post('finFtId');
                $finFarmerId = $this->input->post('finFarmerId');
                //$finLtId = $this->input->post('finLtId');
                //$finSavetId = $this->input->post('finSavetId');
                //$finRevtId = $this->input->post('finRevtId');
                //$finTsaleId = $this->input->post('finTsaleId');
                //$finLoanId = $this->input->post('finLoanId');
                $finDate = $this->input->post('finDate');
                $finDK = $this->input->post('finDK');
                $finAmount = $this->input->post('finAmount');
                $finLoanFile = $_FILES['finLoanFile']['name']; 

                $param = array(
                    'finFtId' => $finFtId,
                    'finFarmerId' => $finFarmerId,
                    'finDate' => $finDate,
                    'finDK' => $finDK,
                    'finAmount' => $finAmount
                );

                if (!empty($finLoanFile)) {
                    $inner_filename = preg_replace("/[^a-zA-Z0-9]/", "_", strtolower(substr(preg_replace("/[^a-zA-Z0-9\s]/", "", $finLoanFile), 0, 100)));
                    $konfig = array(
                        'url' => '../upload_file/',
                        'type' => 'pdf|jpg|jpeg',
                        'size' => '15360',
                        'namafile' => 'kontrak_pinjaman_' . $inner_filename . '_' . date("YmdHis")
                    );

                    $this->load->library('UploadArsip');
                    $file_name = $this->uploadarsip->arsip($konfig,'finLoanFile');

                    if (!empty($file_name))
                        $param['finLoanFile'] = $file_name;
                }

                if (empty($finIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('d_finance', $param);
                } else {
                    $key = array('finId' => $finIdOld);
                    $proses = $this->{$this->_model_name}->update('d_finance', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['finId' => $keyS];
        $datas = $this->{$this->_model_name}->get_by_id('d_finance', $key);
        if (file_exists('../upload_file/' . $datas->finLoanFile))
            @unlink('../upload_file/' . $datas->finLoanFile);
        $proses = $this->{$this->_model_name}->delete('d_finance', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }

    public function download()
    {
        $this->load->helper('download');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['finId' => $keyS];
        $datas = $this->{$this->_model_name}->get_by_id('d_finance', $key);
        force_download('../upload_file/' . $datas->finLoanFile, NULL);
    }
}
