<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class dsaprodibuy extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/dsaprodibuy/';
        $this->_path_js = null;
        $this->_judul = 'Pembelian Saprodi';
        $this->_controller_name = 'dsaprodibuy';
        $this->_model_name = 'model_dsaprodibuy';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->all();
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;
        $data['ref_saprodi_harga'] = $this->{$this->_model_name}->get_ref_table('ref_saprodi_harga');

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['saprodibId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->by_id($key);
        $data['ref_saprodi_harga'] = $this->{$this->_model_name}->get_ref_table('ref_saprodi_harga');

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $saprodibIdOld = $this->input->post('saprodibIdOld');
        $this->form_validation->set_rules('saprodibHsaprodiId', 'Barang', 'trim|xss_clean|required');
        $this->form_validation->set_rules('saprodibDate', 'Tanggal Beli', 'trim|xss_clean|required');
        $this->form_validation->set_rules('saprodibNoPesan', 'No Pemesanan', 'trim|xss_clean|required');
        $this->form_validation->set_rules('saprodibToko', 'Toko/Produsen/Distributor', 'trim|xss_clean|required');
        $this->form_validation->set_rules('saprodibCurr', 'Mata Uang', 'trim|xss_clean');
        $this->form_validation->set_rules('saprodibHarga', 'Harga Satuan', 'trim|xss_clean|required');
        $this->form_validation->set_rules('saprodibJum', 'Jumlah Barang', 'trim|xss_clean|required');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $saprodibHsaprodiId = $this->input->post('saprodibHsaprodiId');
                $saprodibDate = $this->input->post('saprodibDate');
                $saprodibNoPesan = $this->input->post('saprodibNoPesan');
                $saprodibToko = $this->input->post('saprodibToko');
                $saprodibCurr = $this->input->post('saprodibCurr');
                $saprodibHarga = $this->input->post('saprodibHarga');
                $saprodibJum = $this->input->post('saprodibJum');


                $param = array(
                    'saprodibHsaprodiId' => $saprodibHsaprodiId,
                    'saprodibDate' => $saprodibDate,
                    'saprodibNoPesan' => $saprodibNoPesan,
                    'saprodibToko' => $saprodibToko,
                    'saprodibCurr' => $saprodibCurr,
                    'saprodibHarga' => $saprodibHarga,
                    'saprodibJum' => $saprodibJum,

                );

                $param2 = array(
                    'hsaprodiModal' => $saprodibHarga,
                );

                if (empty($saprodibIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('d_saprodibuy', $param);
                } else {
                    $key = array('saprodibId' => $saprodibIdOld);
                    $proses = $this->{$this->_model_name}->update('d_saprodibuy', $param, $key);
                }

                if ($proses) {
                    $proses = $this->{$this->_model_name}->update('ref_saprodi_harga', $param2, array('hsaprodiId' => $saprodibHsaprodiId));
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                } else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['saprodibId' => $keyS];
        $proses = $this->{$this->_model_name}->delete('d_saprodibuy', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }
}
