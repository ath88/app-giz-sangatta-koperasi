<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class refprice extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/refprice/';
        $this->_path_js = null;
        $this->_judul = 'Harga';
        $this->_controller_name = 'refprice';
        $this->_model_name = 'model_refprice';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->get_ref_table('ref_price');
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $data['download_url'] = site_url($this->_controller_name . '/download') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['priceId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->get_by_id('ref_price', $key);

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $priceIdOld = $this->input->post('priceIdOld');
        $this->form_validation->set_rules('priceBulan', 'Bulan', 'trim|xss_clean|required');
        $this->form_validation->set_rules('priceTahun', 'Tahun', 'trim|xss_clean|required');
        $this->form_validation->set_rules('price3', 'Umur 3 Tahun', 'trim|xss_clean|required');
        $this->form_validation->set_rules('price4', 'Umur 4 Tahun', 'trim|xss_clean|required');
        $this->form_validation->set_rules('price5', 'Umur 5 Tahun', 'trim|xss_clean|required');
        $this->form_validation->set_rules('price6', 'Umur 6 Tahun', 'trim|xss_clean|required');
        $this->form_validation->set_rules('price7', 'Umur 7 Tahun', 'trim|xss_clean|required');
        $this->form_validation->set_rules('price8', 'Umur 8 Tahun', 'trim|xss_clean|required');
        $this->form_validation->set_rules('price9', 'Umur 9 Tahun', 'trim|xss_clean|required');
        $this->form_validation->set_rules('price10', 'Umur 10 - 25 Tahun', 'trim|xss_clean|required');
        $this->form_validation->set_rules('priceSK', 'Nomor SK', 'trim|xss_clean|required');
        $this->form_validation->set_rules('priceSKDate', 'Tanggal SK', 'trim|xss_clean|required');
        //$this->form_validation->set_rules('priceFile', 'File', 'trim|xss_clean|required');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $priceBulan = $this->input->post('priceBulan');
                $priceTahun = $this->input->post('priceTahun');
                $price3 = $this->input->post('price3');
                $price4 = $this->input->post('price4');
                $price5 = $this->input->post('price5');
                $price6 = $this->input->post('price6');
                $price7 = $this->input->post('price7');
                $price8 = $this->input->post('price8');
                $price9 = $this->input->post('price9');
                $price10 = $this->input->post('price10');
                $priceSK = $this->input->post('priceSK');
                $priceSKDate = $this->input->post('priceSKDate');
                $priceFile = $_FILES['berkas']['name'];

                $param = array(
                    'priceBulan' => $priceBulan,
                    'priceTahun' => $priceTahun,
                    'price3' => $price3,
                    'price4' => $price4,
                    'price5' => $price5,
                    'price6' => $price6,
                    'price7' => $price7,
                    'price8' => $price8,
                    'price9' => $price9,
                    'price10' => $price10,
                    'priceSK' => $priceSK,
                    'priceSKDate' => $priceSKDate,
                );

                if (!empty($priceFile)) {
                    $inner_filename = preg_replace("/[^a-zA-Z0-9]/", "_", strtolower(substr(preg_replace("/[^a-zA-Z0-9\s]/", "", $priceSKDate . '_' . $priceFile), 0, 100)));
                    $konfig = array(
                        'url' => '../upload_file/',
                        'type' => 'pdf|jpg|jpeg',
                        'size' => '15360',
                        'namafile' => 'price_' . $inner_filename . '_' . date("YmdHis")
                    );

                    $this->load->library('UploadArsip');
                    $file_name = $this->uploadarsip->arsip($konfig);

                    if (!empty($file_name))
                        $param['priceFile'] = $file_name;
                }

                if (empty($priceIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('ref_price', $param);
                } else {
                    $key = array('priceId' => $priceIdOld);
                    $proses = $this->{$this->_model_name}->update('ref_price', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! <br/>' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['priceId' => $keyS];
        $datas = $this->{$this->_model_name}->get_by_id('ref_price', $key);
        if (file_exists('../upload_file/' . $datas->priceFile))
            @unlink('../upload_file/' . $datas->priceFile);
        $proses = $this->{$this->_model_name}->delete('ref_price', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }

    public function download()
    {
        $this->load->helper('download');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['priceId' => $keyS];
        $datas = $this->{$this->_model_name}->get_by_id('ref_price', $key);
        force_download('../upload_file/' . $datas->priceFile, NULL);
    }
}
