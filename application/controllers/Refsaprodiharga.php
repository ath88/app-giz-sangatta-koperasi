<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class refsaprodiharga extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/refsaprodiharga/';
        $this->_path_js = null;
        $this->_judul = 'Stok Barang';
        $this->_controller_name = 'refsaprodiharga';
        $this->_model_name = 'model_refsaprodiharga';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->all();
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $data['download_url'] = site_url($this->_controller_name . '/download') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;
        $data['ref_saprodi_jenis'] = $this->{$this->_model_name}->get_ref_table('ref_saprodi_jenis');

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['hsaprodiId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->by_id($key);
        $data['ref_saprodi_jenis'] = $this->{$this->_model_name}->get_ref_table('ref_saprodi_jenis');

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $hsaprodiIdOld = $this->input->post('hsaprodiIdOld');
        $this->form_validation->set_rules('hsaprodiJId', 'Jenis Saprodi', 'trim|xss_clean|required');
        $this->form_validation->set_rules('hsaprodiDate', 'Tanggal', 'trim|xss_clean|required');
        $this->form_validation->set_rules('hsaprodiHarga', 'Harga Satuan', 'trim|xss_clean|required');
        $this->form_validation->set_rules('hsaprodiNama', 'Nama Barang', 'trim|xss_clean|required');
        $this->form_validation->set_rules('hsaprodiSatuan', 'Satuan', 'trim|xss_clean|required');
        $this->form_validation->set_rules('hsaprodiKet', 'Keterangan', 'trim|xss_clean');
        //$this->form_validation->set_rules('hsaprodiFile', 'File', 'trim|xss_clean|required');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $hsaprodiJId = $this->input->post('hsaprodiJId');
                $hsaprodiDate = $this->input->post('hsaprodiDate');
                $hsaprodiHarga = $this->input->post('hsaprodiHarga');
                $hsaprodiNama = $this->input->post('hsaprodiNama');
                $hsaprodiSatuan = $this->input->post('hsaprodiSatuan');
                $hsaprodiKet = $this->input->post('hsaprodiKet');
                $hsaprodiFile = $_FILES['hsaprodiFile']['name'];


                $param = array(
                    'hsaprodiJId' => $hsaprodiJId,
                    'hsaprodiDate' => $hsaprodiDate,
                    'hsaprodiHarga' => $hsaprodiHarga,
                    'hsaprodiNama' => $hsaprodiNama,
                    'hsaprodiSatuan' => $hsaprodiSatuan,
                    'hsaprodiKet' => $hsaprodiKet,
                );

                if (!empty($hsaprodiFile)) {
                    $inner_filename = preg_replace("/[^a-zA-Z0-9]/", "_", strtolower(substr(preg_replace("/[^a-zA-Z0-9\s]/", "", $hsaprodiFile), 0, 100)));
                    $konfig = array(
                        'url' => '../upload_file/',
                        'type' => 'pdf|jpg|jpeg',
                        'size' => '15360',
                        'namafile' => 'price_saprodi_' . $inner_filename . '_' . date("YmdHis")
                    );

                    $this->load->library('UploadArsip');
                    $file_name = $this->uploadarsip->arsip($konfig, 'hsaprodiFile');

                    if (!empty($file_name))
                        $param['hsaprodiFile'] = $file_name;
                }

                if (empty($hsaprodiIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('ref_saprodi_harga', $param);
                } else {
                    $key = array('hsaprodiId' => $hsaprodiIdOld);
                    $proses = $this->{$this->_model_name}->update('ref_saprodi_harga', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['hsaprodiId' => $keyS];
        $datas = $this->{$this->_model_name}->get_by_id('ref_saprodi_harga', $key);
        if (file_exists('../upload_file/' . $datas->hsaprodiFile))
            @unlink('../upload_file/' . $datas->hsaprodiFile);
        $proses = $this->{$this->_model_name}->delete('ref_saprodi_harga', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }

    public function download()
    {
        $this->load->helper('download');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['hsaprodiId' => $keyS];
        $datas = $this->{$this->_model_name}->get_by_id('ref_saprodi_harga', $key);
        force_download('../upload_file/' . $datas->hsaprodiFile, NULL);
    }
}
