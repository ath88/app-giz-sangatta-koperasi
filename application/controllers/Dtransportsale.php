<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class dtransportsale extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/dtransportsale/';
        $this->_path_js = 'transaksi/';
        $this->_judul = 'Penjualan';
        $this->_controller_name = 'dtransportsale';
        $this->_model_name = 'model_dtransportsale';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
        $this->load->model('model_dcontract', '', TRUE);
        $this->load->model('model_dharvest', '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [$this->_path_js . '/' . $this->_controller_name];
        $data['petani'] = $this->{$this->_model_name}->get_ref_table('ref_farmer');
        $data['show_url'] = site_url($this->_controller_name . '/response') . '/';
        $this->load->view($this->_template, $data);
    }

    public function response()
    {
        $this->form_validation->set_rules('farmerId', 'Petani', 'trim|required|xss_clean');
        $this->form_validation->set_rules('tahun', 'Tahun', 'trim|required|xss_clean');
        $this->form_validation->set_rules('bulan', 'Bulan', 'trim|required|xss_clean');
        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $data = $this->get_master($this->_path_page . $this->_page_index);
                $farmerId = $this->input->post('farmerId');
                $tahun = $this->input->post('tahun');
                $bulan = $this->input->post('bulan');
                $key = json_encode(['farmerId' => $farmerId, 'tahun' => $tahun, 'bulan' => $bulan]);
                $data['id'] = $this->encryptions->encode($key, $this->config->item('encryption_key'));

                $data['datas'] = $this->{$this->_model_name}->all($tahun, $bulan, $farmerId);
                $data['potongan'] = $this->{$this->_model_name}->get_potongan($tahun, $bulan, $farmerId);
                $data['angsuran1'] = $this->{$this->_model_name}->get_by_id('d_saprodisale', "saprodisFarmerId = '$farmerId' AND MONTH(saprodisAngsuranTgl1) LIKE '$bulan' AND YEAR(saprodisAngsuranTgl1) = '$tahun'");
                $data['angsuran2'] = $this->{$this->_model_name}->get_by_id('d_saprodisale', "saprodisFarmerId = '$farmerId' AND MONTH(saprodisAngsuranTgl2) LIKE '$bulan' AND YEAR(saprodisAngsuranTgl2) = '$tahun'");
                $data['angsuran3'] = $this->{$this->_model_name}->get_by_id('d_saprodisale', "saprodisFarmerId = '$farmerId' AND MONTH(saprodisAngsuranTgl3) LIKE '$bulan' AND YEAR(saprodisAngsuranTgl3) = '$tahun'");
                $data['export_url'] = site_url($this->_controller_name . '/expexcel') . '/' . $data['id'];

                //$data['download_url'] = site_url($this->_controller_name . '/download') . '/';
                $pages = $this->_path_page . 'response';
                $this->load->view($pages, $data);
            }
        } else {
            message('Ooops!! Something Wrong!!', 'error');
        }
    }
}
