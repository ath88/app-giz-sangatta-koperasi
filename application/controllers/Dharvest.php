<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

class dharvest extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/dharvest/';
        $this->_path_js = 'transaksi';
        $this->_judul = 'Panen';
        $this->_controller_name = 'dharvest';
        $this->_model_name = 'model_dharvest';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
        $this->load->model('model_dfarmerparcel', '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [$this->_path_js . '/' . $this->_controller_name];
        $data['petani'] = $this->{$this->_model_name}->get_ref_table('ref_farmer');
        $data['show_url'] = site_url($this->_controller_name . '/response') . '/';
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $this->load->view($this->_template, $data);
    }

    public function response()
    {
        $this->form_validation->set_rules('farmerId', 'Petani', 'trim|required|xss_clean');
        $this->form_validation->set_rules('tahun', 'Tahun', 'trim|required|xss_clean');
        $this->form_validation->set_rules('bulan', 'Bulan', 'trim|required|xss_clean');
        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $data = $this->get_master($this->_path_page . $this->_page_index);
                $farmerId = $this->input->post('farmerId');
                $tahun = $this->input->post('tahun');
                $bulan = $this->input->post('bulan');
                $key = json_encode(['farmerId' => $farmerId, 'tahun' => $tahun, 'bulan' => $bulan]);
                $data['id'] = $this->encryptions->encode($key, $this->config->item('encryption_key'));

                $where = "farmerId = '" . $farmerId . "' and YEAR(harvestDate) = '" . $tahun . "' and MONTH(harvestDate) LIKE '" . $bulan . "'";
                $data['datas'] = $this->{$this->_model_name}->all($where);
                $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
                $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
                $data['export_url'] = site_url($this->_controller_name . '/expexcel') . '/' . $data['id'];

                //$data['download_url'] = site_url($this->_controller_name . '/download') . '/';
                $pages = $this->_path_page . 'response';
                $this->load->view($pages, $data);
            }
        } else {
            message('Ooops!! Something Wrong!!', 'error');
        }
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;
        $data['d_farmer_parcel'] = $this->model_dfarmerparcel->all();
        $data['d_worker'] = $this->model_dfarmerparcel->get_ref_table('ref_worker');

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['harvestId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->by_id($key);
        $data['d_farmer_parcel'] = $this->model_dfarmerparcel->all();
        $data['d_worker'] = $this->model_dfarmerparcel->get_ref_table('ref_worker');

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $harvestIdOld = $this->input->post('harvestIdOld');
        $this->form_validation->set_rules('harvestParcelId', 'Parcel', 'trim|xss_clean|required');
        $this->form_validation->set_rules('harvestDate', 'Tanggal', 'trim|xss_clean|required');
        $this->form_validation->set_rules('harvestVolume', 'Volume', 'trim|xss_clean|required');

        $this->form_validation->set_rules('harvestRotasi', 'Rotasi', 'trim|xss_clean');
        $this->form_validation->set_rules('harvestWorkerNIK', 'Pekerja', 'trim|xss_clean');
        $this->form_validation->set_rules('harvestWorkerHK', 'Hari Kerja', 'trim|xss_clean');
        $this->form_validation->set_rules('harvestJJG', 'Berat JJG', 'trim|xss_clean');
        $this->form_validation->set_rules('harvestBrondol', 'Berat Brondol', 'trim|xss_clean');
        $this->form_validation->set_rules('harvestKirim', 'Berat Kirim', 'trim|xss_clean');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $harvestParcelId = $this->input->post('harvestParcelId');
                $harvestDate = $this->input->post('harvestDate');
                $harvestVolume = $this->input->post('harvestVolume');
                $harvestRotasi = $this->input->post('harvestRotasi');
                $harvestWorkerNIK = $this->input->post('harvestWorkerNIK');
                $harvestWorkerHK = $this->input->post('harvestWorkerHK');
                $harvestJJG = $this->input->post('harvestJJG');
                $harvestBrondol = $this->input->post('harvestBrondol');
                $harvestKirim = $this->input->post('harvestKirim');


                $param = array(
                    'harvestParcelId' => $harvestParcelId,
                    'harvestDate' => $harvestDate,
                    'harvestVolume' => $harvestVolume,
                    'harvestRotasi' => $harvestRotasi,
                    'harvestWorkerNIK' => $harvestWorkerNIK,
                    'harvestWorkerHK' => $harvestWorkerHK,
                    'harvestJJG' => $harvestJJG,
                    'harvestBrondol' => $harvestBrondol,
                    'harvestKirim' => $harvestKirim

                );

                if (empty($harvestIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('d_harvest', $param);
                } else {
                    $key = array('harvestId' => $harvestIdOld);
                    $proses = $this->{$this->_model_name}->update('d_harvest', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['harvestId' => $keyS];
        $proses = $this->{$this->_model_name}->delete('d_harvest', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }

    public function expexcel()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $id = json_decode($keyS);

        $where = "farmerId = '" . $id->farmerId . "' and YEAR(harvestDate) = '" . $id->tahun . "' and MONTH(harvestDate) LIKE '" . $id->bulan . "'";
        $datas = $this->{$this->_model_name}->all($where);
        $objPHPExcel = new Spreadsheet();

        $objPHPExcel->setActiveSheetIndex(0)
            //mengisikan value pada tiap-tiap cell, A1 itu alamat cellnya 
            ->setCellValue('A1', 'No.')
            ->setCellValue('B1', 'Petani Persil')
            ->setCellValue('C1', 'Tanggal Panen')
            ->setCellValue('D1', 'Rotasi')
            ->setCellValue('E1', 'Volume')
            ->setCellValue('F1', 'Pekerja')
            ->setCellValue('G1', 'Pekerja HK')
            ->setCellValue('H1', 'Berat JJG')
            ->setCellValue('I1', 'Berat Brondol')
            ->setCellValue('J1', 'Berat Kirim');

        if ($datas != false) {
            $i = 1;
            foreach ($datas as $row) {
                $birthDate = new DateTime($row->farmerDateBirth);
                $today = new DateTime("today");
                $objPHPExcel->setActiveSheetIndex(0)
                    //mengisikan value pada tiap-tiap cell, A1 itu alamat cellnya 
                    ->setCellValue('A' . ($i + 1), $i)
                    ->setCellValue('B' . ($i + 1), $row->farmerName . ' - ' . $row->parcelTotalArea)
                    ->setCellValue('C' . ($i + 1), $row->harvestDate)
                    ->setCellValue('D' . ($i + 1), $row->harvestRotasi)
                    ->setCellValue('E' . ($i + 1), $row->harvestVolume)
                    ->setCellValue('F' . ($i + 1), $row->workerName)
                    ->setCellValue('G' . ($i + 1), $row->harvestWorkerHK)
                    ->setCellValue('H' . ($i + 1), $row->harvestJJG)
                    ->setCellValue('I' . ($i + 1), $row->harvestBrondol)
                    ->setCellValue('J' . ($i + 1), $row->harvestKirim);
                $i++;
            }
        }

        //set title pada sheet (me rename nama sheet)
        $objPHPExcel->getActiveSheet()->setTitle('HARVEST');

        //mulai menyimpan excel format xlsx, kalau ingin xls ganti Excel2007 menjadi Excel5          
        $objWriter = IOFactory::createWriter($objPHPExcel, 'Xlsx');

        //sesuaikan headernya 
        ob_end_clean();
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        //ubah nama file saat diunduh
        header('Content-Disposition: attachment;filename="HARVEST_' . date('YmdHis') . '.xlsx"');
        //unduh file
        $objWriter->save("php://output");
    }
}
