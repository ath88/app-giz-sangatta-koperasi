<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

class dsaprodisale extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/dsaprodisale/';
        $this->_path_js = 'transaksi';
        $this->_judul = 'Penjualan Saprodi';
        $this->_controller_name = 'dsaprodisale';
        $this->_model_name = 'model_dsaprodisale';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
        $this->load->model('model_refsaprodiharga', '', TRUE);
    }

    public function getharga()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $id = $this->uri->segment(3);
        $datas = $this->{$this->_model_name}->get_by_id('ref_saprodi_harga', ['hsaprodiId' => $id]);
        echo json_encode($datas);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [$this->_path_js . '/' . $this->_controller_name];
        $data['show_url'] = site_url($this->_controller_name . '/response') . '/';
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $this->load->view($this->_template, $data);
    }

    public function response()
    {
        $this->form_validation->set_rules('tahun', 'Tahun', 'trim|required|xss_clean');
        $this->form_validation->set_rules('bulan', 'Bulan', 'trim|required|xss_clean');
        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $data = $this->get_master($this->_path_page . $this->_page_index);
                $tahun = $this->input->post('tahun');
                $bulan = $this->input->post('bulan');
                $key = json_encode(['tahun' => $tahun, 'bulan' => $bulan]);
                $data['id'] = $this->encryptions->encode($key, $this->config->item('encryption_key'));

                $where = "YEAR(saprodisTanggal) = '" . $tahun . "' and MONTH(saprodisTanggal) LIKE '" . $bulan . "'";
                $data['datas'] = $this->{$this->_model_name}->all($where);
                $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
                $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
                $data['export_url'] = site_url($this->_controller_name . '/expexcel') . '/' . $data['id'];

                //$data['download_url'] = site_url($this->_controller_name . '/download') . '/';
                $pages = $this->_path_page . 'response';
                $this->load->view($pages, $data);
            }
        } else {
            message('Ooops!! Something Wrong!!', 'error');
        }
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [$this->_path_js . '/' . $this->_controller_name];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;
        $data['rsaprodi'] = $this->model_refsaprodiharga->all();
        $data['farmer'] = $this->{$this->_model_name}->get_ref_table('ref_farmer');

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [$this->_path_js . '/' . $this->_controller_name];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['saprodisId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->by_id($key);
        $data['rsaprodi'] = $this->model_refsaprodiharga->all();
        $data['farmer'] = $this->{$this->_model_name}->get_ref_table('ref_farmer');

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $saprodisIdOld = $this->input->post('saprodisIdOld');
        $this->form_validation->set_rules('saprodisHsaprodiId', 'Barang', 'trim|xss_clean|required');
        $this->form_validation->set_rules('saprodisJumlah', 'Jumlah', 'trim|xss_clean|required');
        $this->form_validation->set_rules('saprodisTanggal', 'Tanggal Pembelian', 'trim|xss_clean|required');
        $this->form_validation->set_rules('saprodisFarmerId', 'Farmer', 'trim|xss_clean');
        $this->form_validation->set_rules('saprodisNama', 'Non Farmer', 'trim|xss_clean');
        $this->form_validation->set_rules('saprodisKontan', 'Pembayaran Kontan', 'trim|xss_clean');
        $this->form_validation->set_rules('saprodisKredit', 'Pembayaran Kredit', 'trim|xss_clean');
        $this->form_validation->set_rules('saprodisAngsuran1', 'Angsuran 1', 'trim|xss_clean');
        $this->form_validation->set_rules('saprodisAngsuranTgl1', 'Tanggal Bayar Angsuran 1', 'trim|xss_clean');
        $this->form_validation->set_rules('saprodisAngsuran2', 'Angsuran 2', 'trim|xss_clean');
        $this->form_validation->set_rules('saprodisAngsuranTgl2', 'Tanggal Bayar Angsuran 2', 'trim|xss_clean');
        $this->form_validation->set_rules('saprodisAngsuran3', 'Angsuran 3', 'trim|xss_clean');
        $this->form_validation->set_rules('saprodisAngsuranTgl3', 'Tanggal Bayar Angsuran 3', 'trim|xss_clean');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $saprodisHsaprodiId = $this->input->post('saprodisHsaprodiId');
                $saprodisJumlah = $this->input->post('saprodisJumlah');
                $saprodisIsBayar = $this->input->post('saprodisIsBayar');
                $saprodisTanggal = $this->input->post('saprodisTanggal');
                $saprodisFarmerId = $this->input->post('saprodisFarmerId');
                $saprodisNama = $this->input->post('saprodisNama');
                $saprodisKontan = $this->input->post('saprodisKontan');
                $saprodisKredit = $this->input->post('saprodisKredit');
                $saprodisAngsuran1 = $this->input->post('saprodisAngsuran1');
                $saprodisAngsuranTgl1 = $this->input->post('saprodisAngsuranTgl1');
                $saprodisAngsuran2 = $this->input->post('saprodisAngsuran2');
                $saprodisAngsuranTgl2 = $this->input->post('saprodisAngsuranTgl2');
                $saprodisAngsuran3 = $this->input->post('saprodisAngsuran3');
                $saprodisAngsuranTgl3 = $this->input->post('saprodisAngsuranTgl3');

                $saprodisHarSatuan = 0;
                $saprodisTotal = 0;
                $rsaprodi = $this->model_refsaprodiharga->by_id(['hsaprodiId' => $saprodisHsaprodiId]);
                if ($rsaprodi != false) {
                    $saprodisHarModal = $rsaprodi->hsaprodiModal;
                    $saprodisHarSatuan = $rsaprodi->hsaprodiHarga;
                    $saprodisTotal = $rsaprodi->hsaprodiHarga * $saprodisJumlah;
                }

                $param = array(
                    'saprodisHsaprodiId' => $saprodisHsaprodiId,
                    'saprodisJumlah' => $saprodisJumlah,
                    'saprodisHarSatuan' => $saprodisHarSatuan,
                    'saprodisHarModal' => $saprodisHarModal,
                    'saprodisTotal' => $saprodisTotal,
                    'saprodisTanggal' => $saprodisTanggal,
                    'saprodisNama' => $saprodisNama,
                    'saprodisKontan' => $saprodisKontan,
                    'saprodisKredit' => $saprodisKredit,
                    'saprodisAngsuran1' => $saprodisAngsuran1,
                    'saprodisAngsuranTgl1' => $saprodisAngsuranTgl1,
                    'saprodisAngsuran2' => $saprodisAngsuran2,
                    'saprodisAngsuranTgl2' => $saprodisAngsuranTgl2,
                    'saprodisAngsuran3' => $saprodisAngsuran3,
                    'saprodisAngsuranTgl3' => $saprodisAngsuranTgl3,

                );

                if (!empty($saprodisFarmerId)) {
                    $rfarmer = $this->{$this->_model_name}->get_by_id('ref_farmer', ['farmerId' => $saprodisFarmerId]);
                    if ($rfarmer != false) {
                        $param['saprodisFarmerId'] = $saprodisFarmerId;
                        $param['saprodisNama'] = $rfarmer->farmerName;
                    }
                }

                if (empty($saprodisIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('d_saprodisale', $param);
                } else {
                    $key = array('saprodisId' => $saprodisIdOld);
                    $proses = $this->{$this->_model_name}->update('d_saprodisale', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['saprodisId' => $keyS];
        $proses = $this->{$this->_model_name}->delete('d_saprodisale', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }

    public function expexcel()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $id = json_decode($keyS);

        $where = "YEAR(saprodisTanggal) = '" . $id->tahun . "' and MONTH(saprodisTanggal) LIKE '" . $id->bulan . "'";
        $datas = $this->{$this->_model_name}->all($where);
        $objPHPExcel = new Spreadsheet();

        $objPHPExcel->setActiveSheetIndex(0)
            //mengisikan value pada tiap-tiap cell, A1 itu alamat cellnya 
            ->setCellValue('A1', 'No.')
            ->setCellValue('B1', 'Tanggal')
            ->setCellValue('C1', 'ID Nota')
            ->setCellValue('D1', 'Nama Barang')
            ->setCellValue('E1', 'Satuan')
            ->setCellValue('F1', 'Jumlah')
            ->setCellValue('G1', 'Harga Satuan')
            ->setCellValue('H1', 'Total')
            ->setCellValue('I1', 'Nama Pembeli')
            ->setCellValue('J1', 'Kontan')
            ->setCellValue('K1', 'Kredit')
            ->setCellValue('L1', 'Angsuran 1')
            ->setCellValue('M1', 'Tanggal Bayar')
            ->setCellValue('N1', 'Angsuran 2')
            ->setCellValue('O1', 'Tanggal Bayar')
            ->setCellValue('P1', 'Angsuran 3')
            ->setCellValue('Q1', 'Tanggal Bayar')
            ->setCellValue('R1', 'Status Kredit')
            ->setCellValue('S1', 'Laba/Rugi');

        if ($datas != false) {
            $i = 1;
            foreach ($datas as $row) {
                $kredit = '';
                if (!empty($row->saprodisKredit)) {
                    if (($row->saprodisKredit - $row->saprodisAngsuran1 - $row->saprodisAngsuran2 - $row->saprodisAngsuran3) > 0)
                        $kredit = 'BELUM LUNAS';
                    else
                        $kredit = 'LUNAS';
                } else
                    $kredit = 'TIDAK';

                $keuntungan = $row->saprodisJumlah * ($row->saprodisHarSatuan - $row->saprodisHarModal);
                $objPHPExcel->setActiveSheetIndex(0)
                    //mengisikan value pada tiap-tiap cell, A1 itu alamat cellnya 
                    ->setCellValue('A' . ($i + 1), $i)
                    ->setCellValue('B' . ($i + 1), $row->saprodisTanggal)
                    ->setCellValue('C' . ($i + 1), $row->saprodisId)
                    ->setCellValue('D' . ($i + 1), $row->hsaprodiNama)
                    ->setCellValue('E' . ($i + 1), $row->hsaprodiSatuan)
                    ->setCellValue('F' . ($i + 1), $row->saprodisJumlah)
                    ->setCellValue('G' . ($i + 1), $row->saprodisHarSatuan)
                    ->setCellValue('H' . ($i + 1), $row->saprodisTotal)
                    ->setCellValue('I' . ($i + 1), $row->saprodisFarmerId)
                    ->setCellValue('J' . ($i + 1), $row->saprodisNama)
                    ->setCellValue('K' . ($i + 1), $row->saprodisKontan)
                    ->setCellValue('L' . ($i + 1), $row->saprodisKredit)
                    ->setCellValue('M' . ($i + 1), $row->saprodisAngsuran1)
                    ->setCellValue('N' . ($i + 1), $row->saprodisAngsuranTgl1)
                    ->setCellValue('O' . ($i + 1), $row->saprodisAngsuran2)
                    ->setCellValue('P' . ($i + 1), $row->saprodisAngsuranTgl2)
                    ->setCellValue('Q' . ($i + 1), $row->saprodisAngsuran3)
                    ->setCellValue('R' . ($i + 1), $kredit)
                    ->setCellValue('S' . ($i + 1), $keuntungan);
                $i++;
            }
        }

        //set title pada sheet (me rename nama sheet)
        $objPHPExcel->getActiveSheet()->setTitle('SAPRODISALE');

        //mulai menyimpan excel format xlsx, kalau ingin xls ganti Excel2007 menjadi Excel5          
        $objWriter = IOFactory::createWriter($objPHPExcel, 'Xlsx');

        //sesuaikan headernya 
        ob_end_clean();
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        //ubah nama file saat diunduh
        header('Content-Disposition: attachment;filename="SAPRODISALE_' . date('YmdHis') . '.xlsx"');
        //unduh file
        $objWriter->save("php://output");
    }
}
