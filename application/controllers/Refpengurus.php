<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

class refpengurus extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/refpengurus/';
        $this->_path_js = null;
        $this->_judul = 'Pengurus';
        $this->_controller_name = 'refpengurus';
        $this->_model_name = 'model_refpengurus';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->all();
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $data['excel_url'] = site_url($this->_controller_name . '/excel') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;
        $data['ref_wilayah'] = $this->{$this->_model_name}->get_wilayah(2, '64111');

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['urusId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->by_id($key);
        $data['ref_wilayah'] = $this->{$this->_model_name}->get_wilayah(2, '64111');

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $urusIdOld = $this->input->post('urusIdOld');
        $this->form_validation->set_rules('urusTempatLahir', 'Tempat Lahir', 'trim|xss_clean|required');
        $this->form_validation->set_rules('urusNama', 'Nama', 'trim|xss_clean|required');
        $this->form_validation->set_rules('urusJabatan', 'Jabatan', 'trim|xss_clean|required');
        $this->form_validation->set_rules('urusNIK', 'Nomor Induk Kependudukan', 'trim|xss_clean|required');
        $this->form_validation->set_rules('urusTanggalLahir', 'Tanggal Lahir', 'trim|xss_clean|required');
        $this->form_validation->set_rules('urusAlamat', 'Alamat', 'trim|xss_clean|required');
        $this->form_validation->set_rules('urusTanggalMasuk', 'Tanggal Masuk', 'trim|xss_clean|required');
        $this->form_validation->set_rules('urusTanggalKeluar', 'Tanggal Keluar', 'trim|xss_clean');
        $this->form_validation->set_rules('urusStatusKawin', 'Status Kawin', 'trim|xss_clean|required');
        $this->form_validation->set_rules('urusKeterangan', 'Keterangan', 'trim|xss_clean');
        $this->form_validation->set_rules('urusGender', 'Gender', 'trim|xss_clean|required');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $urusTempatLahir = $this->input->post('urusTempatLahir');
                $urusNama = $this->input->post('urusNama');
                $urusJabatan = $this->input->post('urusJabatan');
                $urusNIK = $this->input->post('urusNIK');
                $urusTanggalLahir = $this->input->post('urusTanggalLahir');
                $urusAlamat = $this->input->post('urusAlamat');
                $urusTanggalMasuk = $this->input->post('urusTanggalMasuk');
                $urusTanggalKeluar = $this->input->post('urusTanggalKeluar');
                $urusStatusKawin = $this->input->post('urusStatusKawin');
                $urusKeterangan = $this->input->post('urusKeterangan');
                $urusGender = $this->input->post('urusGender');


                $param = array(
                    'urusTempatLahir' => $urusTempatLahir,
                    'urusNama' => $urusNama,
                    'urusJabatan' => $urusJabatan,
                    'urusNIK' => $urusNIK,
                    'urusTanggalLahir' => $urusTanggalLahir,
                    'urusAlamat' => $urusAlamat,
                    'urusTanggalMasuk' => $urusTanggalMasuk,
                    'urusTanggalKeluar' => $urusTanggalKeluar,
                    'urusStatusKawin' => $urusStatusKawin,
                    'urusKeterangan' => $urusKeterangan,
                    'urusGender' => $urusGender,

                );

                if (empty($urusIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('ref_pengurus', $param);
                } else {
                    $key = array('urusId' => $urusIdOld);
                    $proses = $this->{$this->_model_name}->update('ref_pengurus', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['urusId' => $keyS];
        $proses = $this->{$this->_model_name}->delete('ref_pengurus', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }

    public function excel()
    {
        $datas = $this->{$this->_model_name}->all();
        $objPHPExcel = new Spreadsheet();

        $objPHPExcel->setActiveSheetIndex(0)
            //mengisikan value pada tiap-tiap cell, A1 itu alamat cellnya 
            ->setCellValue('A1', 'No.')
            ->setCellValue('B1', 'NIK')
            ->setCellValue('C1', 'Nama')
            ->setCellValue('D1', 'Jenis Kelamin')
            ->setCellValue('E1', 'Tempat Lahir')
            ->setCellValue('F1', 'Tanggal Lahir')
            ->setCellValue('G1', 'Alamat')
            ->setCellValue('H1', 'Jabatan')
            ->setCellValue('I1', 'Tanggal Masuk')
            ->setCellValue('J1', 'Tanggal Keluar')
            ->setCellValue('K1', 'Status Kawin')
            ->setCellValue('L1', 'Keterangan');

        if ($datas != false) {
            $i = 1;
            foreach ($datas as $row) {
                $objPHPExcel->setActiveSheetIndex(0)
                    //mengisikan value pada tiap-tiap cell, A1 itu alamat cellnya 
                    ->setCellValue('A' . ($i + 1), $i)
                    ->setCellValue('B' . ($i + 1), $row->urusNIK)
                    ->setCellValue('C' . ($i + 1), $row->urusNama)
                    ->setCellValue('D' . ($i + 1), $row->urusGender)
                    ->setCellValue('E' . ($i + 1), $row->NAMA_WILAYAH)
                    ->setCellValue('F' . ($i + 1), $row->urusTanggalLahir)
                    ->setCellValue('G' . ($i + 1), $row->urusAlamat)
                    ->setCellValue('H' . ($i + 1), $row->urusJabatan)
                    ->setCellValue('I' . ($i + 1), $row->urusTanggalMasuk)
                    ->setCellValue('J' . ($i + 1), $row->urusTanggalKeluar)
                    ->setCellValue('K' . ($i + 1), $row->urusStatusKawin)
                    ->setCellValue('L' . ($i + 1), $row->urusKeterangan);
                $i++;
            }
        }

        //set title pada sheet (me rename nama sheet)
        $objPHPExcel->getActiveSheet()->setTitle('PENGURUS');

        //mulai menyimpan excel format xlsx, kalau ingin xls ganti Excel2007 menjadi Excel5          
        $objWriter = IOFactory::createWriter($objPHPExcel, 'Xlsx');

        //sesuaikan headernya 
        ob_end_clean();
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        //ubah nama file saat diunduh
        header('Content-Disposition: attachment;filename="PENGURUS_' . date('YmdHis') . '.xlsx"');
        //unduh file
        $objWriter->save("php://output");
    }
}
