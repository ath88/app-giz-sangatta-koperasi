<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

class dtbsreport extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/dtbsreport/';
        $this->_path_js = 'transaksi';
        $this->_judul = 'Laporan Penjualan TBS';
        $this->_controller_name = 'dtbsreport';
        $this->_model_name = 'model_dtbsreport';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
        $this->load->model('model_dfarmerparcel', '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [$this->_path_js . '/' . $this->_controller_name];
        $data['show_url'] = site_url($this->_controller_name . '/response') . '/';
        $this->load->view($this->_template, $data);
    }

    public function response()
    {
        $this->form_validation->set_rules('tahun', 'Tahun', 'trim|required|xss_clean');
        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $data = $this->get_master($this->_path_page . $this->_page_index);
                $tahun = $this->input->post('tahun');
                $key = json_encode(['tahun' => $tahun]);
                $data['id'] = $this->encryptions->encode($key, $this->config->item('encryption_key'));

                $data['datas'] = $this->{$this->_model_name}->all($tahun);
                $data['export_url'] = site_url($this->_controller_name . '/expexcel') . '/' . $data['id'];

                //$data['download_url'] = site_url($this->_controller_name . '/download') . '/';
                $pages = $this->_path_page . 'response';
                $this->load->view($pages, $data);
            }
        } else {
            message('Ooops!! Something Wrong!!', 'error');
        }
    }

    public function expexcel()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $id = json_decode($keyS);

        $datas = $this->{$this->_model_name}->all($id->tahun);
        $objPHPExcel = new Spreadsheet();

        $objPHPExcel->setActiveSheetIndex(0)
            //mengisikan value pada tiap-tiap cell, A1 itu alamat cellnya 
            ->setCellValue('A1', 'No.')
            ->setCellValue('B1', 'Bulan')
            ->setCellValue('C1', 'Harga')
            ->setCellValue('D1', 'Berat Bersih')
            ->setCellValue('E1', 'Total Potongan (Rp.30)')
            ->setCellValue('F1', 'Pendapatan Koperasi Desa (Rp.7)')
            ->setCellValue('G1', 'Infrastruktur (Rp.6)')
            ->setCellValue('H1', 'KOP. Induk (Rp.7)')
            ->setCellValue('I1', 'KOP. SSWJ (Rp.7)')
            ->setCellValue('J1', 'KTH Mandiri (Rp.3)')
            ->setCellValue('K1', 'Kelompok Tani (Rp.10)')
            ->setCellValue('L1', 'Jumlah');

        if ($datas != false) {
            $i = 1;
            foreach ($datas as $row) {
                $jumPot = $row->tPotong + $row->tPendapatan + $row->tInfra + $row->tKop + $row->tSSWJ + $row->tMandiri + $row->tKelompok;
                $objPHPExcel->setActiveSheetIndex(0)
                    //mengisikan value pada tiap-tiap cell, A1 itu alamat cellnya 
                    ->setCellValue('A' . ($i + 1), $i)
                    ->setCellValue('B' . ($i + 1), $row->naBulan)
                    ->setCellValue('C' . ($i + 1), $row->contPrice)
                    ->setCellValue('D' . ($i + 1), $row->jumNPanen)
                    ->setCellValue('E' . ($i + 1), $row->tPotong)
                    ->setCellValue('F' . ($i + 1), $row->tPendapatan)
                    ->setCellValue('G' . ($i + 1), $row->tInfra)
                    ->setCellValue('H' . ($i + 1), $row->tKop)
                    ->setCellValue('I' . ($i + 1), $row->tSSWJ)
                    ->setCellValue('J' . ($i + 1), $row->tMandiri)
                    ->setCellValue('K' . ($i + 1), $row->tKelompok)
                    ->setCellValue('L' . ($i + 1), $jumPot);
                $i++;
            }
        }

        //set title pada sheet (me rename nama sheet)
        $objPHPExcel->getActiveSheet()->setTitle('JUAL_TBS');

        //mulai menyimpan excel format xlsx, kalau ingin xls ganti Excel2007 menjadi Excel5          
        $objWriter = IOFactory::createWriter($objPHPExcel, 'Xlsx');

        //sesuaikan headernya 
        ob_end_clean();
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        //ubah nama file saat diunduh
        header('Content-Disposition: attachment;filename="JUAL_TBS_' . $id->tahun . '_' . date('YmdHis') . '.xlsx"');
        //unduh file
        $objWriter->save("php://output");
    }
}
