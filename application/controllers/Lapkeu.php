<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

class lapkeu extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/lapkeu/';
        $this->_path_js = null;
        $this->_path_js = 'laporan';
        $this->_judul = 'Buku Kas Petani';
        $this->_controller_name = 'lapkeu';
        $this->_model_name = 'model_lapkeu';
        $this->_page_index = 'index';
        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [$this->_path_js . '/' . $this->_controller_name];
        $data['petani'] = $this->{$this->_model_name}->get_ref_table('ref_farmer');
        $data['show_url'] = site_url($this->_controller_name . '/response') . '/';
        $this->load->view($this->_template, $data);
    }


    public function response()
    {
        $this->form_validation->set_rules('farmerId', 'Petani', 'trim|required|xss_clean');
        $this->form_validation->set_rules('tahun', 'Tahun', 'trim|required|xss_clean');
        $this->form_validation->set_rules('bulan', 'Bulan', 'trim|required|xss_clean');
        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $data = $this->get_master($this->_path_page . $this->_page_index);
                $farmerId = $this->input->post('farmerId');
                $tahun = $this->input->post('tahun');
                $bulan = $this->input->post('bulan');
                $key = json_encode(['farmerId' => $farmerId, 'tahun' => $tahun, 'bulan' => $bulan]);
                $data['id'] = $this->encryptions->encode($key, $this->config->item('encryption_key'));
                $data['saldo_awal'] = $this->{$this->_model_name}->get_saldo_awal($farmerId, $tahun, $bulan);
                $data['datas'] = $this->{$this->_model_name}->all($farmerId, $tahun, $bulan);
                $data['slip_url'] = site_url($this->_controller_name . '/slipexcel') . '/' . $data['id'];

                //$data['download_url'] = site_url($this->_controller_name . '/download') . '/';
                $pages = $this->_path_page . 'response';
                $this->load->view($pages, $data);
            }
        } else {
            message('Ooops!! Something Wrong!!', 'error');
        }
    }

    public function slipexcel()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $id = json_decode($keyS);

        $organisasi = $this->{$this->_model_name}->get_ref_table('ref_organization');

        $objPHPExcel = new Spreadsheet();

        if ($organisasi[0] != false) {
            $objPHPExcel->getActiveSheet(0)->setCellValue('A1', $organisasi[0]->organizationName);
            $objPHPExcel->getActiveSheet(0)->setCellValue('A2', 'SLIP PEMBAYARAN HASIL PANEN');
            $objPHPExcel->getActiveSheet(0)->setCellValue('A3', 'Bulan ' . MonthToIndo(substr(('0' . $id->bulan), -2)) . ' ' . $id->tahun);
        }

        $datas = $this->{$this->_model_name}->row_datas($id->farmerId, $id->tahun, $id->bulan);

        if ($datas != false) {
            $objPHPExcel->getActiveSheet(0)->setCellValue('A5', $datas[0]->farmerName);

            $objPHPExcel->setActiveSheetIndex(0)
                //mengisikan value pada tiap-tiap cell, A1 itu alamat cellnya 
                ->setCellValue('A7', 'No.')
                ->setCellValue('B7', 'Tanggal')
                ->setCellValue('C7', 'Berat Kirim')
                ->setCellValue('D7', 'Susut Lapangan')
                ->setCellValue('E7', 'Berat Diterima')
                ->setCellValue('F7', 'Susut Sortasi')
                ->setCellValue('G7', 'Berat Bersih')
                ->setCellValue('H7', 'Harga')
                ->setCellValue('I7', 'Diterima kotor')
                ->setCellValue('J7', 'Pot. PPH 0.25%')
                ->setCellValue('K7', 'Pot. Transport')
                ->setCellValue('L7', 'Pot. Kop')
                ->setCellValue('M7', 'Jum. Pot')
                ->setCellValue('N7', 'Diterima');

            $i = 1;
            $sDikirim = $sSusut = $sDiterima = $sSortasi = $sBbersih = $sHarga = $sKotor = $sPPH = $sTransport = $sPotKop = $sJmlPot = $sTerima = 0;
            foreach ($datas as $row) {
                $sDikirim += $row->tsaleDelVolume;
                $sSusut += ($row->tsaleDelVolume - $row->tsaleRecVolume);
                $sDiterima += $row->tsaleRecVolume;
                $sSortasi += ($row->tsaleRecVolume - $row->tsaleStaVolume);
                $sBbersih += $row->tsaleStaVolume;
                $sHarga += $row->tsaleHarga;
                $sKotor += $row->tsaleBruto;
                $sPPH += $row->tsalePot025;
                $sTransport += $row->tsalePotTranport;
                $sPotKop += $row->tsalePotKoperasi;
                $sJmlPot += ($row->tsalePot025 + $row->tsalePotTranport + $row->tsalePotKoperasi);
                $sTerima += ($row->tsaleBruto - ($row->tsalePot025 + $row->tsalePotTranport + $row->tsalePotKoperasi));

                $objPHPExcel->setActiveSheetIndex(0)
                    //mengisikan value pada tiap-tiap cell, A1 itu alamat cellnya 
                    ->setCellValue('A' . ($i + 7), $i)
                    ->setCellValue('B' . ($i + 7), $row->tsaleDate)
                    ->setCellValue('C' . ($i + 7), $row->tsaleDelVolume)
                    ->setCellValue('D' . ($i + 7), $row->tsaleDelVolume - $row->tsaleRecVolume)
                    ->setCellValue('E' . ($i + 7), $row->tsaleRecVolume)
                    ->setCellValue('F' . ($i + 7), $row->tsaleRecVolume - $row->tsaleStaVolume)
                    ->setCellValue('G' . ($i + 7), $row->tsaleStaVolume)
                    ->setCellValue('H' . ($i + 7), $row->tsaleHarga)
                    ->setCellValue('I' . ($i + 7), $row->tsaleBruto)
                    ->setCellValue('J' . ($i + 7), $row->tsalePot025)
                    ->setCellValue('K' . ($i + 7), $row->tsalePotTranport)
                    ->setCellValue('L' . ($i + 7), $row->tsalePotKoperasi)
                    ->setCellValue('M' . ($i + 7), $row->tsalePot025 + $row->tsalePotTranport + $row->tsalePotKoperasi)
                    ->setCellValue('N' . ($i + 7), $row->tsaleBruto - ($row->tsalePot025 + $row->tsalePotTranport + $row->tsalePotKoperasi));
                $i++;
            }

            $objPHPExcel->setActiveSheetIndex(0)
                //mengisikan value pada tiap-tiap cell, A1 itu alamat cellnya 
                ->setCellValue('A' . ($i + 8), 'Total : ')
                ->setCellValue('B' . ($i + 8), '')
                ->setCellValue('C' . ($i + 8), $sDikirim)
                ->setCellValue('D' . ($i + 8), $sSusut)
                ->setCellValue('E' . ($i + 8), $sDiterima)
                ->setCellValue('F' . ($i + 8), $sSortasi)
                ->setCellValue('G' . ($i + 8), $sBbersih)
                ->setCellValue('H' . ($i + 8), '')
                ->setCellValue('I' . ($i + 8), $sKotor)
                ->setCellValue('J' . ($i + 8), $sPPH)
                ->setCellValue('K' . ($i + 8), $sTransport)
                ->setCellValue('L' . ($i + 8), $sPotKop)
                ->setCellValue('M' . ($i + 8), $sJmlPot)
                ->setCellValue('N' . ($i + 8), $sTerima);

            
            $objPHPExcel->getActiveSheet(0)->setCellValue('A' . ($i + 10), 'Pemotongan Lain : ');

            $r_potlain = $this->{$this->_model_name}->row_sum($id->farmerId, $id->tahun, $id->bulan);
            $r_finance = $this->{$this->_model_name}->row_finance($id->farmerId, $id->tahun, $id->bulan);

            $objPHPExcel->getActiveSheet(0)->setCellValue('A' . ($i + 11), 'Administrasi : ');
            $objPHPExcel->getActiveSheet(0)->setCellValue('B' . ($i + 11), ($r_potlain != false ? $r_potlain->sumAdm : 0));
            $objPHPExcel->getActiveSheet(0)->setCellValue('A' . ($i + 12), 'Biaya Timbang : ');
            $objPHPExcel->getActiveSheet(0)->setCellValue('B' . ($i + 12), ($r_potlain != false ? $r_potlain->sumTimbang : 0));
            $objPHPExcel->getActiveSheet(0)->setCellValue('A' . ($i + 13), 'Zakat : ');
            $objPHPExcel->getActiveSheet(0)->setCellValue('B' . ($i + 13), ($r_potlain != false ? $r_potlain->sumZakat : 0));

            $sum_potlain = ($r_potlain != false ? $r_potlain->sumAdm : 0) + ($r_potlain != false ? $r_potlain->sumTimbang : 0) + ($r_potlain != false ? $r_potlain->sumZakat : 0);

            if ($r_finance != false) {
                foreach ($r_finance as $row) {
                    $objPHPExcel->setActiveSheetIndex(0)
                        //mengisikan value pada tiap-tiap cell, A1 itu alamat cellnya 
                        ->setCellValue('A' . ($i + 14), $row->Ket . ' : ')
                        ->setCellValue('B' . ($i + 14), $row->tsaleB);
                    $sum_potlain += $row->tsaleB;
                    $i++;
                }
            }
            $objPHPExcel->setActiveSheetIndex(0)
                //mengisikan value pada tiap-tiap cell, A1 itu alamat cellnya 
                ->setCellValue('A' . ($i + 15), 'Total Potongan : ')
                ->setCellValue('B' . ($i + 15), $sum_potlain);

            $objPHPExcel->setActiveSheetIndex(0)
                //mengisikan value pada tiap-tiap cell, A1 itu alamat cellnya 
                ->setCellValue('A' . ($i + 17), 'Pendapatan Bersih : ')
                ->setCellValue('B' . ($i + 17), ($sTerima - $sum_potlain));
        }
        //set title pada sheet (me rename nama sheet)
        $objPHPExcel->getActiveSheet()->setTitle('SLIP');

        //mulai menyimpan excel format xlsx, kalau ingin xls ganti Excel2007 menjadi Excel5          
        $objWriter = IOFactory::createWriter($objPHPExcel, 'Xlsx');

        //sesuaikan headernya 
        ob_end_clean();
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        //ubah nama file saat diunduh
        header('Content-Disposition: attachment;filename="SLIP_' . date('YmdHis') . '.xlsx"');
        //unduh file
        $objWriter->save("php://output");
    }
}
