<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class dkryngaji extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/dkryngaji/';
        $this->_path_js = null;
        $this->_judul = 'Upah Karyawan';
        $this->_controller_name = 'dkryngaji';
        $this->_model_name = 'model_dkryngaji';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->all();
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;
        $data['ref_karyawan'] = $this->{$this->_model_name}->get_ref_table('ref_karyawan');

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['gajikId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->by_id($key);
        $data['ref_karyawan'] = $this->{$this->_model_name}->get_ref_table('ref_karyawan');

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $gajikIdOld = $this->input->post('gajikIdOld');
        $this->form_validation->set_rules('gajikKrynId', 'Karyawan', 'trim|xss_clean|required');
        $this->form_validation->set_rules('gajikDate', 'Tanggal', 'trim|xss_clean|required');
        $this->form_validation->set_rules('gajikGapok', 'Gaji Pokok', 'trim|xss_clean|required');
        $this->form_validation->set_rules('gajikTjabatan', 'Tunjangan Jabatan', 'trim|xss_clean');
        $this->form_validation->set_rules('gajikTkonsumsi', 'Tunjangan Konsumsi', 'trim|xss_clean');
        $this->form_validation->set_rules('gajikTHarian', 'Tunjangan Harian', 'trim|xss_clean');
        $this->form_validation->set_rules('gajikTBonus', 'Bonus/Lembur', 'trim|xss_clean');
        $this->form_validation->set_rules('gajikPPajak', 'Pajak', 'trim|xss_clean');
        $this->form_validation->set_rules('gajikPAsuransi', 'Asuransi', 'trim|xss_clean');
        $this->form_validation->set_rules('gajikPHutang', 'Hutang', 'trim|xss_clean');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $gajikKrynId = $this->input->post('gajikKrynId');
                $karyawan = $this->{$this->_model_name}->get_by_id('ref_karyawan', ['krynId' => $gajikKrynId]);

                $gajikDate = $this->input->post('gajikDate');
                $gajikJabatan = ($karyawan != false ? $karyawan->krynJabatan : '');
                $gajikStatus = ($karyawan != false ? $karyawan->krynStatus : '');
                $gajikGapok = $this->input->post('gajikGapok');
                $gajikTjabatan = $this->input->post('gajikTjabatan');
                $gajikTkonsumsi = $this->input->post('gajikTkonsumsi');
                $gajikTHarian = $this->input->post('gajikTHarian');
                $gajikTBonus = $this->input->post('gajikTBonus');
                $gajikPPajak = $this->input->post('gajikPPajak');
                $gajikPAsuransi = $this->input->post('gajikPAsuransi');
                $gajikPHutang = $this->input->post('gajikPHutang');


                $param = array(
                    'gajikKrynId' => $gajikKrynId,
                    'gajikDate' => $gajikDate,
                    'gajikJabatan' => $gajikJabatan,
                    'gajikStatus' => $gajikStatus,
                    'gajikGapok' => $gajikGapok,
                    'gajikTjabatan' => $gajikTjabatan,
                    'gajikTkonsumsi' => $gajikTkonsumsi,
                    'gajikTHarian' => $gajikTHarian,
                    'gajikTBonus' => $gajikTBonus,
                    'gajikPPajak' => $gajikPPajak,
                    'gajikPAsuransi' => $gajikPAsuransi,
                    'gajikPHutang' => $gajikPHutang,

                );

                if (empty($gajikIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('d_karyawan_gaji', $param);
                } else {
                    $key = array('gajikId' => $gajikIdOld);
                    $proses = $this->{$this->_model_name}->update('d_karyawan_gaji', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['gajikId' => $keyS];
        $proses = $this->{$this->_model_name}->delete('d_karyawan_gaji', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }
}
