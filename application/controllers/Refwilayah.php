<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class refwilayah extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/refwilayah/';
        $this->_path_js = null;
        $this->_judul = 'Wilayah';
        $this->_controller_name = 'refwilayah';
        $this->_model_name = 'model_refwilayah';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->all();
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;
        $data['ref_wilayah'] = $this->{$this->_model_name}->get_ref_table('ref_wilayah');

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['ID_WILAYAH' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->by_id($key);
        $data['ref_wilayah'] = $this->{$this->_model_name}->get_ref_table('ref_wilayah');

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $ID_WILAYAHOld = $this->input->post('ID_WILAYAHOld');
        if (empty($ID_WILAYAHOld))
            $this->form_validation->set_rules('ID_WILAYAH', 'ID_WILAYAH', 'trim|xss_clean|required|is_unique[ref_wilayah.ID_WILAYAH]');
        else
            $this->form_validation->set_rules('ID_WILAYAH', 'ID_WILAYAH', 'trim|xss_clean|required');
        $this->form_validation->set_rules('IDPARENT', 'IDPARENT', 'trim|xss_clean');
        $this->form_validation->set_rules('NAMA_WILAYAH', 'NAMA_WILAYAH', 'trim|xss_clean|required');
        $this->form_validation->set_rules('LEVEL', 'LEVEL', 'trim|xss_clean|required');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $ID_WILAYAH = $this->input->post('ID_WILAYAH');
                $IDPARENT = $this->input->post('IDPARENT');
                $NAMA_WILAYAH = $this->input->post('NAMA_WILAYAH');
                $LEVEL = $this->input->post('LEVEL');


                $param = array(
                    'ID_WILAYAH' => $ID_WILAYAH,
                    'IDPARENT' => $IDPARENT,
                    'NAMA_WILAYAH' => $NAMA_WILAYAH,
                    'LEVEL' => $LEVEL,

                );

                if (empty($ID_WILAYAHOld)) {
                    $proses = $this->{$this->_model_name}->insert('ref_wilayah', $param);
                } else {
                    $key = array('ID_WILAYAH' => $ID_WILAYAHOld);
                    $proses = $this->{$this->_model_name}->update('ref_wilayah', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['ID_WILAYAH' => $keyS];
        $proses = $this->{$this->_model_name}->delete('ref_wilayah', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }
}
