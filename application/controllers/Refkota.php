<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class refkota extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/refkota/';
        $this->_path_js = null;
        $this->_judul = 'Kota';
        $this->_controller_name = 'refkota';
        $this->_model_name = 'model_refkota';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->all();
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;
        $data['ref_provinsi'] = $this->{$this->_model_name}->get_ref_table('ref_provinsi');

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['kotaId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->by_id($key);
        $data['ref_provinsi'] = $this->{$this->_model_name}->get_ref_table('ref_provinsi');

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $kotaIdOld = $this->input->post('kotaIdOld');
        $this->form_validation->set_rules('kotaProvId', 'Provinsi', 'trim|xss_clean|required');
        $this->form_validation->set_rules('kotaNama', 'Kota', 'trim|xss_clean|required');
        $this->form_validation->set_rules('kotaIsIbuProv', 'Ibu Kota Provinsi ?', 'trim|xss_clean|required');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $kotaProvId = $this->input->post('kotaProvId');
                $kotaNama = $this->input->post('kotaNama');
                $kotaIsIbuProv = $this->input->post('kotaIsIbuProv');


                $param = array(
                    'kotaProvId' => $kotaProvId,
                    'kotaNama' => $kotaNama,
                    'kotaIsIbuProv' => $kotaIsIbuProv,

                );

                if (empty($kotaIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('ref_kota', $param);
                } else {
                    $key = array('kotaId' => $kotaIdOld);
                    $proses = $this->{$this->_model_name}->update('ref_kota', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['kotaId' => $keyS];
        $proses = $this->{$this->_model_name}->delete('ref_kota', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }
}
