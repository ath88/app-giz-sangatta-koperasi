<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class reftruck extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/reftruck/';
        $this->_path_js = null;
        $this->_judul = 'Angkutan';
        $this->_controller_name = 'reftruck';
        $this->_model_name = 'model_reftruck';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->get_ref_table('ref_truck');
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['truckId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->get_by_id('ref_truck', $key);

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $truckIdOld = $this->input->post('truckIdOld');
        $this->form_validation->set_rules('truckName', 'Nama Pemilik', 'trim|xss_clean|required');
        $this->form_validation->set_rules('truckAddress', 'Alamat Pemilik', 'trim|xss_clean|required');
        $this->form_validation->set_rules('truckCapacity', 'Kapasitas', 'trim|xss_clean|required');
        $this->form_validation->set_rules('truckNumber', 'Nomor Kendaraan', 'trim|xss_clean|required');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $truckName = $this->input->post('truckName');
                $truckAddress = $this->input->post('truckAddress');
                $truckCapacity = $this->input->post('truckCapacity');
                $truckNumber = $this->input->post('truckNumber');


                $param = array(
                    'truckName' => $truckName,
                    'truckAddress' => $truckAddress,
                    'truckCapacity' => $truckCapacity,
                    'truckNumber' => $truckNumber,

                );

                if (empty($truckIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('ref_truck', $param);
                } else {
                    $key = array('truckId' => $truckIdOld);
                    $proses = $this->{$this->_model_name}->update('ref_truck', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['truckId' => $keyS];
        $proses = $this->{$this->_model_name}->delete('ref_truck', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }
}
