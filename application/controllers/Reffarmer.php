<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

class reffarmer extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/reffarmer/';
        $this->_path_js = null;
        $this->_judul = 'Petani';
        $this->_controller_name = 'reffarmer';
        $this->_model_name = 'model_reffarmer';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->all();
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['excel_url'] = site_url($this->_controller_name . '/excel') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $data['download_url'] = site_url($this->_controller_name . '/download') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;
        $data['ref_wilayah'] = $this->{$this->_model_name}->get_wilayah(2, '64111');

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['farmerId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->by_id($key);
        $data['ref_wilayah'] = $this->{$this->_model_name}->get_wilayah(2, '64111');

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $farmerIdOld = $this->input->post('farmerIdOld');
        $this->form_validation->set_rules('farmerNIK', 'NIK', 'trim|xss_clean|required');
        $this->form_validation->set_rules('farmerCityBirth', 'Kota Lahir', 'trim|xss_clean|required');
        $this->form_validation->set_rules('farmerCityAddress', 'Kota Domisili', 'trim|xss_clean|required');
        $this->form_validation->set_rules('farmerName', 'Nama', 'trim|xss_clean|required');
        $this->form_validation->set_rules('farmerGender', 'Jenis Kelamin', 'trim|xss_clean|required');
        $this->form_validation->set_rules('farmerDateBirth', 'Tanggal Lahir', 'trim|xss_clean|required');
        $this->form_validation->set_rules('farmerPlaceBirth', 'Tempat Lahir', 'trim|xss_clean|required');
        $this->form_validation->set_rules('farmerAddress', 'Alamat Domisili', 'trim|xss_clean|required');
        $this->form_validation->set_rules('farmerStatusKeluarga', 'Status Keluarga', 'trim|xss_clean|required');
        $this->form_validation->set_rules('farmetJumKeluarga', 'Jumlah Keluarga', 'trim|xss_clean|required');
        $this->form_validation->set_rules('farmerJenjangPendidikan', 'Jenjang Pendidikan', 'trim|xss_clean|required');
        //$this->form_validation->set_rules('farmerCommitment', 'File Komitment', 'trim|xss_clean|required');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $farmerNIK = $this->input->post('farmerNIK');
                $farmerCityBirth = $this->input->post('farmerCityBirth');
                $farmerCityAddress = $this->input->post('farmerCityAddress');
                $farmerName = $this->input->post('farmerName');
                $farmerGender = $this->input->post('farmerGender');
                $farmerDateBirth = $this->input->post('farmerDateBirth');
                $farmerPlaceBirth = $this->input->post('farmerPlaceBirth');
                $farmerAddress = $this->input->post('farmerAddress');
                $farmerStatusKeluarga = $this->input->post('farmerStatusKeluarga');
                $farmetJumKeluarga = $this->input->post('farmetJumKeluarga');
                $farmerJenjangPendidikan = $this->input->post('farmerJenjangPendidikan');
                $farmerCommitment = $_FILES['berkas']['name'];

                $param = array(
                    'farmerNIK' => $farmerNIK,
                    'farmerCityBirth' => $farmerCityBirth,
                    'farmerCityAddress' => $farmerCityAddress,
                    'farmerName' => $farmerName,
                    'farmerGender' => $farmerGender,
                    'farmerDateBirth' => $farmerDateBirth,
                    'farmerPlaceBirth' => $farmerPlaceBirth,
                    'farmerAddress' => $farmerAddress,
                    'farmerStatusKeluarga' => $farmerStatusKeluarga,
                    'farmetJumKeluarga' => $farmetJumKeluarga,
                    'farmerJenjangPendidikan' => $farmerJenjangPendidikan
                );

                if (!empty($farmerCommitment)) {
                    $inner_filename = preg_replace("/[^a-zA-Z0-9]/", "_", strtolower(substr(preg_replace("/[^a-zA-Z0-9\s]/", "", $farmerName . '_' . $farmerCommitment), 0, 100)));
                    $konfig = array(
                        'url' => '../upload_file/',
                        'type' => 'pdf|jpg|jpeg',
                        'size' => '15360',
                        'namafile' => 'komitmen_' . $inner_filename . '_' . date("YmdHis")
                    );

                    $this->load->library('UploadArsip');
                    $file_name = $this->uploadarsip->arsip($konfig);

                    if (!empty($file_name))
                        $param['farmerCommitment'] = $file_name;
                }

                if (empty($farmerIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('ref_farmer', $param);
                } else {
                    $key = array('farmerId' => $farmerIdOld);
                    $proses = $this->{$this->_model_name}->update('ref_farmer', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! <br/>' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['farmerId' => $keyS];
        $datas = $this->{$this->_model_name}->by_id($key);
        if (file_exists('../upload_file/' . $datas->farmerCommitment))
            @unlink('../upload_file/' . $datas->farmerCommitment);
        $proses = $this->{$this->_model_name}->delete('ref_farmer', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }

    public function download()
    {
        $this->load->helper('download');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['farmerId' => $keyS];
        $datas = $this->{$this->_model_name}->by_id($key);
        force_download('../upload_file/' . $datas->farmerCommitment, NULL);
    }

    public function excel()
    {
        $datas = $this->{$this->_model_name}->all();
        $objPHPExcel = new Spreadsheet();

        $objPHPExcel->setActiveSheetIndex(0)
            //mengisikan value pada tiap-tiap cell, A1 itu alamat cellnya 
            ->setCellValue('A1', 'No.')
            ->setCellValue('B1', 'NIK')
            ->setCellValue('C1', 'Nama')
            ->setCellValue('D1', 'Jenis Kelamin')
            ->setCellValue('E1', 'Tempat Lahir')
            ->setCellValue('F1', 'Kota Lahir')
            ->setCellValue('G1', 'Tangga Lahir')
            ->setCellValue('H1', 'Umur')
            ->setCellValue('I1', 'Alamat')
            ->setCellValue('J1', 'Kota Domisili')
            ->setCellValue('K1', 'Status Keluarga')
            ->setCellValue('L1', 'Jumlah Anggota Keluarga')
            ->setCellValue('M1', 'Pendidikan Terakhir');

        if ($datas != false) {
            $i = 1;
            foreach ($datas as $row) {
                $birthDate = new DateTime($row->farmerDateBirth);
                $today = new DateTime("today");
                $objPHPExcel->setActiveSheetIndex(0)
                    //mengisikan value pada tiap-tiap cell, A1 itu alamat cellnya 
                    ->setCellValue('A' . ($i + 1), $i)
                    ->setCellValue('B' . ($i + 1), $row->farmerNIK)
                    ->setCellValue('C' . ($i + 1), $row->farmerName)
                    ->setCellValue('D' . ($i + 1), $row->farmerGender)
                    ->setCellValue('E' . ($i + 1), $row->farmerPlaceBirth)
                    ->setCellValue('F' . ($i + 1), $row->lrWIL)
                    ->setCellValue('G' . ($i + 1), $row->farmerDateBirth)
                    ->setCellValue('H' . ($i + 1), $today->diff($birthDate)->y)
                    ->setCellValue('I' . ($i + 1), $row->farmerAddress)
                    ->setCellValue('J' . ($i + 1), $row->dmWIL)
                    ->setCellValue('K' . ($i + 1), $row->farmerStatusKeluarga)
                    ->setCellValue('L' . ($i + 1), $row->farmetJumKeluarga)
                    ->setCellValue('M' . ($i + 1), $row->farmerJenjangPendidikan);
                $i++;
            }
        }

        //set title pada sheet (me rename nama sheet)
        $objPHPExcel->getActiveSheet()->setTitle('FARMER');

        //mulai menyimpan excel format xlsx, kalau ingin xls ganti Excel2007 menjadi Excel5          
        $objWriter = IOFactory::createWriter($objPHPExcel, 'Xlsx');

        //sesuaikan headernya 
        ob_end_clean();
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        //ubah nama file saat diunduh
        header('Content-Disposition: attachment;filename="FARMER_' . date('YmdHis') . '.xlsx"');
        //unduh file
        $objWriter->save("php://output");
    }
}
