<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

class dparcelmaintenance extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/dparcelmaintenance/';
        $this->_path_js = 'transaksi';
        $this->_judul = 'Pemeliharaan Persil';
        $this->_controller_name = 'dparcelmaintenance';
        $this->_model_name = 'model_dparcelmaintenance';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
        $this->load->model('model_dfarmerparcel', '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [$this->_path_js . '/' . $this->_controller_name];
        $data['petani'] = $this->{$this->_model_name}->get_ref_table('ref_farmer');
        $data['show_url'] = site_url($this->_controller_name . '/response') . '/';
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $this->load->view($this->_template, $data);
    }

    public function response()
    {
        $this->form_validation->set_rules('farmerId', 'Petani', 'trim|required|xss_clean');
        $this->form_validation->set_rules('tahun', 'Tahun', 'trim|required|xss_clean');
        $this->form_validation->set_rules('bulan', 'Bulan', 'trim|required|xss_clean');
        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $data = $this->get_master($this->_path_page . $this->_page_index);
                $farmerId = $this->input->post('farmerId');
                $tahun = $this->input->post('tahun');
                $bulan = $this->input->post('bulan');
                $key = json_encode(['farmerId' => $farmerId, 'tahun' => $tahun, 'bulan' => $bulan]);
                $data['id'] = $this->encryptions->encode($key, $this->config->item('encryption_key'));

                $where = "farmerId = '" . $farmerId . "' and YEAR(pmDate) = '" . $tahun . "' and MONTH(pmDate) LIKE '" . $bulan . "'";
                $data['datas'] = $this->{$this->_model_name}->all($where);
                $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
                $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
                $data['export_url'] = site_url($this->_controller_name . '/expexcel') . '/' . $data['id'];

                //$data['download_url'] = site_url($this->_controller_name . '/download') . '/';
                $pages = $this->_path_page . 'response';
                $this->load->view($pages, $data);
            }
        } else {
            message('Ooops!! Something Wrong!!', 'error');
        }
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;
        $data['ref_maintenance'] = $this->{$this->_model_name}->get_ref_table('ref_maintenance');
        $data['d_farmer_parcel'] = $this->model_dfarmerparcel->all();
        $data['ref_fertilizer'] = $this->{$this->_model_name}->get_ref_table('ref_fertilizer');

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['pmId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->by_id($key);
        $data['ref_maintenance'] = $this->{$this->_model_name}->get_ref_table('ref_maintenance');
        $data['d_farmer_parcel'] = $this->model_dfarmerparcel->all();
        $data['ref_fertilizer'] = $this->{$this->_model_name}->get_ref_table('ref_fertilizer');

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $pmIdOld = $this->input->post('pmIdOld');
        $this->form_validation->set_rules('pmMaintainId', 'Tipe', 'trim|xss_clean|required');
        $this->form_validation->set_rules('pmParcelId', 'Parcel', 'trim|xss_clean|required');
        $this->form_validation->set_rules('pmFertId', 'Jenis Fertilizer', 'trim|xss_clean');
        $this->form_validation->set_rules('pmDate', 'Tanggal', 'trim|xss_clean|required');
        $this->form_validation->set_rules('pmActivity', 'Aktivitas', 'trim|xss_clean');
        $this->form_validation->set_rules('pmArea', 'Jumlah Area', 'trim|xss_clean');
        $this->form_validation->set_rules('pmBrand', 'Merk', 'trim|xss_clean');
        $this->form_validation->set_rules('pmVolume', 'Volume', 'trim|xss_clean');
        $this->form_validation->set_rules('pmSOP', 'SOP', 'trim|xss_clean');
        $this->form_validation->set_rules('pmPalms', 'Jumlah Pohon', 'trim|xss_clean');
        $this->form_validation->set_rules('pmRotate', 'Rotasi', 'trim|xss_clean');
        $this->form_validation->set_rules('pmHK', 'Pekerja HK', 'trim|xss_clean');
        $this->form_validation->set_rules('pmGaji', 'Total Upah Pekerja', 'trim|xss_clean');
        $this->form_validation->set_rules('pmSatuan', 'Satuan Bahan', 'trim|xss_clean');
        $this->form_validation->set_rules('pmNilaiTotal', 'Total Harga Bahan', 'trim|xss_clean');
        $this->form_validation->set_rules('pmTransport', 'Transportasi', 'trim|xss_clean');
        $this->form_validation->set_rules('pmIsAPD', 'Penggunaan APD', 'required|trim|xss_clean');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $pmMaintainId = $this->input->post('pmMaintainId');
                $pmParcelId = $this->input->post('pmParcelId');
                $pmFertId = $this->input->post('pmFertId');
                $pmDate = $this->input->post('pmDate');
                $pmActivity = $this->input->post('pmActivity');
                $pmArea = $this->input->post('pmArea');
                $pmBrand = $this->input->post('pmBrand');
                $pmVolume = $this->input->post('pmVolume');
                $pmSOP = $this->input->post('pmSOP');
                $pmPalms = $this->input->post('pmPalms');
                $pmRotate = $this->input->post('pmRotate');
                $pmHK = $this->input->post('pmHK');
                $pmGaji = $this->input->post('pmGaji');
                $pmSatuan = $this->input->post('pmSatuan');
                $pmNilaiTotal = $this->input->post('pmNilaiTotal');
                $pmTransport = $this->input->post('pmTransport');
                $pmIsAPD = $this->input->post('pmIsAPD');


                $param = array(
                    'pmMaintainId' => $pmMaintainId,
                    'pmParcelId' => $pmParcelId,
                    'pmFertId' => $pmFertId,
                    'pmDate' => $pmDate,
                    'pmActivity' => $pmActivity,
                    'pmArea' => $pmArea,
                    'pmBrand' => $pmBrand,
                    'pmVolume' => $pmVolume,
                    'pmSOP' => $pmSOP,
                    'pmPalms' => $pmPalms,
                    'pmRotate' => $pmRotate,
                    'pmHK' => $pmHK,
                    'pmGaji' => $pmGaji,
                    'pmSatuan' => $pmSatuan,
                    'pmNilaiTotal' => $pmNilaiTotal,
                    'pmTransport' => $pmTransport,
                    'pmIsAPD' => $pmIsAPD,

                );

                if (empty($pmIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('d_parcel_maintenance', $param);
                } else {
                    $key = array('pmId' => $pmIdOld);
                    $proses = $this->{$this->_model_name}->update('d_parcel_maintenance', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['pmId' => $keyS];
        $proses = $this->{$this->_model_name}->delete('d_parcel_maintenance', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }

    public function expexcel()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $id = json_decode($keyS);

        $where = "farmerId = '" . $id->farmerId . "' and YEAR(pmDate) = '" . $id->tahun . "' and MONTH(pmDate) LIKE '" . $id->bulan . "'";
        $datas = $this->{$this->_model_name}->all($where);
        $objPHPExcel = new Spreadsheet();

        $objPHPExcel->setActiveSheetIndex(0)
            //mengisikan value pada tiap-tiap cell, A1 itu alamat cellnya 
            ->setCellValue('A1', 'No.')
            ->setCellValue('B1', 'Jenis Pekerjaan')
            ->setCellValue('C1', 'Petani Persil')
            ->setCellValue('D1', 'Luas Area')
            ->setCellValue('E1', 'Tanggal Pelaksanaan')
            ->setCellValue('F1', 'Rotasi')
            ->setCellValue('G1', 'Pekerja HK')
            ->setCellValue('H1', 'Pekerja Upah')
            ->setCellValue('I1', 'Jenis biokimia')
            ->setCellValue('J1', 'Nama bahan')
            ->setCellValue('K1', 'Volume bahan')
            ->setCellValue('L1', 'Satuan')
            ->setCellValue('M1', 'Total Bahan')
            ->setCellValue('N1', 'Tranport')
            ->setCellValue('O1', 'SOP')
            ->setCellValue('P1', 'Aktivitas')
            ->setCellValue('Q1', 'Jumlah Pohon')
            ->setCellValue('R1', 'Penggunaan APD');

        if ($datas != false) {
            $i = 1;
            foreach ($datas as $row) {
                $birthDate = new DateTime($row->farmerDateBirth);
                $today = new DateTime("today");
                $objPHPExcel->setActiveSheetIndex(0)
                    //mengisikan value pada tiap-tiap cell, A1 itu alamat cellnya 
                    ->setCellValue('A' . ($i + 1), $i)
                    ->setCellValue('B' . ($i + 1), $row->maintainName)
                    ->setCellValue('C' . ($i + 1), $row->farmerName . ' - ' . $row->parcelTotalArea)
                    ->setCellValue('D' . ($i + 1), $row->pmArea)
                    ->setCellValue('E' . ($i + 1), $row->pmDate)
                    ->setCellValue('F' . ($i + 1), $row->pmRotate)
                    ->setCellValue('G' . ($i + 1), $row->pmHK)
                    ->setCellValue('H' . ($i + 1), $row->pmGaji)
                    ->setCellValue('I' . ($i + 1), $row->fertNama)
                    ->setCellValue('J' . ($i + 1), $row->pmBrand)
                    ->setCellValue('K' . ($i + 1), $row->pmVolume)
                    ->setCellValue('L' . ($i + 1), $row->pmSatuan)
                    ->setCellValue('M' . ($i + 1), $row->pmNilaiTotal)
                    ->setCellValue('N' . ($i + 1), $row->pmTransport)
                    ->setCellValue('O' . ($i + 1), $row->pmSOP)
                    ->setCellValue('P' . ($i + 1), $row->pmActivity)
                    ->setCellValue('Q' . ($i + 1), $row->pmPalms)
                    ->setCellValue('R' . ($i + 1), ($row->pmIsAPD==1?'YA':'TIDAK'));
                $i++;
            }
        }

        //set title pada sheet (me rename nama sheet)
        $objPHPExcel->getActiveSheet()->setTitle('PARCELMAINT');

        //mulai menyimpan excel format xlsx, kalau ingin xls ganti Excel2007 menjadi Excel5          
        $objWriter = IOFactory::createWriter($objPHPExcel, 'Xlsx');

        //sesuaikan headernya 
        ob_end_clean();
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        //ubah nama file saat diunduh
        header('Content-Disposition: attachment;filename="PARCELMAINT_' . date('YmdHis') . '.xlsx"');
        //unduh file
        $objWriter->save("php://output");
    }
}
