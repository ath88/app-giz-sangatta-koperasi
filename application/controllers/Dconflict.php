<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class dconflict extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/dconflict/';
        $this->_path_js = null;
        $this->_judul = 'Konflik';
        $this->_controller_name = 'dconflict';
        $this->_model_name = 'model_dconflict';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
        $this->load->model('model_dfarmerparcel', '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->all();
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;
        $data['d_farmer_parcel'] = $this->model_dfarmerparcel->all();

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['conflictId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->by_id($key);
        $data['d_farmer_parcel'] = $this->model_dfarmerparcel->all();

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $conflictIdOld = $this->input->post('conflictIdOld');
        $this->form_validation->set_rules('conflictParcelId', 'Parcel', 'trim|xss_clean|required');
        $this->form_validation->set_rules('conflictDate', 'Tanggal', 'trim|xss_clean|required');
        $this->form_validation->set_rules('conflictParties', 'Parties', 'trim|xss_clean');
        $this->form_validation->set_rules('conflictDescription', 'Deskripsi', 'trim|xss_clean|required');
        $this->form_validation->set_rules('confilctStatus', 'Clear ?', 'trim|xss_clean|required');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $conflictParcelId = $this->input->post('conflictParcelId');
                $conflictDate = $this->input->post('conflictDate');
                $conflictParties = $this->input->post('conflictParties');
                $conflictDescription = $this->input->post('conflictDescription');
                $confilctStatus = $this->input->post('confilctStatus');


                $param = array(
                    'conflictParcelId' => $conflictParcelId,
                    'conflictDate' => $conflictDate,
                    'conflictParties' => $conflictParties,
                    'conflictDescription' => $conflictDescription,
                    'confilctStatus' => $confilctStatus,

                );

                if (empty($conflictIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('d_conflict', $param);
                } else {
                    $key = array('conflictId' => $conflictIdOld);
                    $proses = $this->{$this->_model_name}->update('d_conflict', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! <br/>' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['conflictId' => $keyS];
        $proses = $this->{$this->_model_name}->delete('d_conflict', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }
}
