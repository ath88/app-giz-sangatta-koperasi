<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class refkaryawan extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/refkaryawan/';
        $this->_path_js = null;
        $this->_judul = 'Karyawan';
        $this->_controller_name = 'refkaryawan';
        $this->_model_name = 'model_refkaryawan';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->all();
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $data['download_url'] = site_url($this->_controller_name . '/download') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;
        $data['ref_wilayah'] = $this->{$this->_model_name}->get_wilayah(2, '64111');

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['krynId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->by_id($key);
        $data['ref_wilayah'] = $this->{$this->_model_name}->get_wilayah(2, '64111');

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $krynIdOld = $this->input->post('krynIdOld');
        $this->form_validation->set_rules('krynTempatLahir', 'Tempat Lahir', 'trim|xss_clean|required');
        $this->form_validation->set_rules('krynNama', 'Nama', 'trim|xss_clean|required');
        $this->form_validation->set_rules('krynJabatan', 'Jabatan', 'trim|xss_clean|required');
        $this->form_validation->set_rules('krynNIK', 'Nomor Induk Kependudukan', 'trim|xss_clean|required');
        $this->form_validation->set_rules('krynTanggalLahir', 'Tanggal Lahir', 'trim|xss_clean|required');
        $this->form_validation->set_rules('krynAlamat', 'Alamat', 'trim|xss_clean|required');
        $this->form_validation->set_rules('krynTanggalMasuk', 'Tanggal Masuk', 'trim|xss_clean|required');
        $this->form_validation->set_rules('krynTanggalKeluar', 'Tanggal Keluar', 'trim|xss_clean');
        $this->form_validation->set_rules('krynStatusKawin', 'Status Kawin', 'trim|xss_clean|required');
        $this->form_validation->set_rules('krynKeterangan', 'Keterangan', 'trim|xss_clean');
        $this->form_validation->set_rules('krynGender', 'Gender', 'trim|xss_clean|required');
        $this->form_validation->set_rules('krynKategori', 'Kategori', 'trim|xss_clean|required');
        $this->form_validation->set_rules('krynStatus', 'Status', 'trim|xss_clean|required');
        //$this->form_validation->set_rules('krynSK', 'SK', 'trim|xss_clean|required');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $krynTempatLahir = $this->input->post('krynTempatLahir');
                $krynNama = $this->input->post('krynNama');
                $krynJabatan = $this->input->post('krynJabatan');
                $krynNIK = $this->input->post('krynNIK');
                $krynTanggalLahir = $this->input->post('krynTanggalLahir');
                $krynAlamat = $this->input->post('krynAlamat');
                $krynTanggalMasuk = $this->input->post('krynTanggalMasuk');
                $krynTanggalKeluar = $this->input->post('krynTanggalKeluar');
                $krynStatusKawin = $this->input->post('krynStatusKawin');
                $krynKeterangan = $this->input->post('krynKeterangan');
                $krynGender = $this->input->post('krynGender');
                $krynKategori = $this->input->post('krynKategori');
                $krynStatus = $this->input->post('krynStatus');
                $krynSK = $_FILES['berkas']['name'];

                $param = array(
                    'krynTempatLahir' => $krynTempatLahir,
                    'krynNama' => $krynNama,
                    'krynJabatan' => $krynJabatan,
                    'krynNIK' => $krynNIK,
                    'krynTanggalLahir' => $krynTanggalLahir,
                    'krynAlamat' => $krynAlamat,
                    'krynTanggalMasuk' => $krynTanggalMasuk,
                    'krynTanggalKeluar' => $krynTanggalKeluar,
                    'krynStatusKawin' => $krynStatusKawin,
                    'krynKeterangan' => $krynKeterangan,
                    'krynGender' => $krynGender,
                    'krynKategori' => $krynKategori,
                    'krynStatus' => $krynStatus

                );

                

                if (!empty($krynSK)) {
                    $inner_filename = preg_replace("/[^a-zA-Z0-9]/", "_", strtolower(substr(preg_replace("/[^a-zA-Z0-9\s]/", "", $krynNama . '_' . $krynTanggalMasuk), 0, 100)));
                    $konfig = array(
                        'url' => '../upload_file/',
                        'type' => 'pdf|jpg|jpeg',
                        'size' => '15360',
                        'namafile' => 'skkryn_' . $inner_filename . '_' . date("YmdHis")
                    );

                    $this->load->library('UploadArsip');
                    $file_name = $this->uploadarsip->arsip($konfig);

                    if (!empty($file_name))
                        $param['krynSK'] = $file_name;
                }

                if (empty($krynIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('ref_karyawan', $param);
                } else {
                    $key = array('krynId' => $krynIdOld);
                    $proses = $this->{$this->_model_name}->update('ref_karyawan', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['krynId' => $keyS];
        $proses = $this->{$this->_model_name}->delete('ref_karyawan', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }

    public function download()
    {
        $this->load->helper('download');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['krynId' => $keyS];
        $datas = $this->{$this->_model_name}->by_id($key);
        force_download('../upload_file/' . $datas->krynSK, NULL);
    }
}
