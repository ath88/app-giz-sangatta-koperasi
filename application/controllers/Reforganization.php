<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class reforganization extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/reforganization/';
        $this->_path_js = null;
        $this->_judul = 'Organisasi';
        $this->_controller_name = 'reforganization';
        $this->_model_name = 'model_reforganization';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->get_ref_table('ref_organization');
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $data['download_url'] = site_url($this->_controller_name . '/download') . '/';
        $data['download2_url'] = site_url($this->_controller_name . '/download2') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['organizationId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->get_by_id('ref_organization', $key);

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $organizationIdOld = $this->input->post('organizationIdOld');
        $this->form_validation->set_rules('organizationName', 'Organisasi', 'trim|xss_clean|required');
        $this->form_validation->set_rules('organizationAddress', 'Alamat', 'trim|xss_clean|required');
        $this->form_validation->set_rules('organizationVision', 'Visi', 'trim|xss_clean');
        $this->form_validation->set_rules('organizationMision', 'Misi', 'trim|xss_clean');
        $this->form_validation->set_rules('organizationNIK', 'Nomor Organisasi', 'trim|xss_clean');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $organizationName = $this->input->post('organizationName');
                $organizationAddress = $this->input->post('organizationAddress');
                $organizationVision = $this->input->post('organizationVision');
                $organizationMision = $this->input->post('organizationMision');
                $organizationNIK = $this->input->post('organizationNIK');
                $organizationAkta = $_FILES['berkas']['name'];
                $organizationSO = $_FILES['berkasSO']['name'];


                $param = array(
                    'organizationName' => $organizationName,
                    'organizationAddress' => $organizationAddress,
                    'organizationVision' => $organizationVision,
                    'organizationMision' => $organizationMision,
                    'organizationNIK' => $organizationNIK,

                );

                if (!empty($organizationAkta)) {
                    $inner_filename = preg_replace("/[^a-zA-Z0-9]/", "_", strtolower(substr(preg_replace("/[^a-zA-Z0-9\s]/", "", $organizationName . ' ' . $organizationAkta), 0, 100)));
                    $konfig = array(
                        'url' => '../upload_file/',
                        'type' => 'pdf|jpg|jpeg',
                        'size' => '15360',
                        'namafile' => 'akta_' . $inner_filename . '_' . date("YmdHis")
                    );

                    $this->load->library('UploadArsip');
                    $file_name = $this->uploadarsip->arsip($konfig);

                    if (!empty($file_name))
                        $param['organizationAkta'] = $file_name;
                }

                if (!empty($organizationSO)) {
                    $inner_filename = preg_replace("/[^a-zA-Z0-9]/", "_", strtolower(substr(preg_replace("/[^a-zA-Z0-9\s]/", "", $organizationName . ' ' . $organizationAkta), 0, 100)));
                    $konfig = array(
                        'url' => '../upload_file/',
                        'type' => 'jpg|jpeg|png',
                        'size' => '15360',
                        'namafile' => 'so_' . $inner_filename . '_' . date("YmdHis")
                    );

                    $this->load->library('UploadArsip');
                    $file_name = $this->uploadarsip->arsip($konfig, 'berkasSO');

                    if (!empty($file_name))
                        $param['organizationSO'] = $file_name;
                }

                if (empty($organizationIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('ref_organization', $param);
                } else {
                    $key = array('organizationId' => $organizationIdOld);
                    $proses = $this->{$this->_model_name}->update('ref_organization', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['organizationId' => $keyS];
        $datas = $this->{$this->_model_name}->get_by_id('ref_organization', $key);
        if (file_exists('../upload_file/' . $datas->organizationAkta))
            @unlink('../upload_file/' . $datas->organizationAkta);
        $proses = $this->{$this->_model_name}->delete('ref_organization', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }

    public function download()
    {
        $this->load->helper('download');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['organizationId' => $keyS];
        $datas = $this->{$this->_model_name}->get_by_id('ref_organization', $key);
        force_download('../upload_file/' . $datas->organizationAkta, NULL);
    }

    public function download2()
    {
        $this->load->helper('download');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['organizationId' => $keyS];
        $datas = $this->{$this->_model_name}->get_by_id('ref_organization', $key);
        force_download('../upload_file/' . $datas->organizationSO, NULL);
    }
}
