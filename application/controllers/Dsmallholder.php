<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class dsmallholder extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/dsmallholder/';
        $this->_path_js = null;
        $this->_judul = 'Penjualan Kredit';
        $this->_controller_name = 'dsmallholder';
        $this->_model_name = 'model_dsmallholder';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->get_ref_table('d_smallholder_credit');
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['screditId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->get_by_id('d_smallholder_credit', $key);

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $screditIdOld = $this->input->post('screditIdOld');
        $this->form_validation->set_rules('screditDate', 'Tanggal', 'trim|xss_clean|required');
        $this->form_validation->set_rules('screditVolume', 'Volume', 'trim|xss_clean|required');
        $this->form_validation->set_rules('screditDK', 'Debit/Kredit', 'trim|xss_clean|required');
        $this->form_validation->set_rules('screditBuyer', 'Pembeli', 'trim|xss_clean');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $screditDate = $this->input->post('screditDate');
                $screditVolume = $this->input->post('screditVolume');
                $screditDK = $this->input->post('screditDK');
                $screditBuyer = $this->input->post('screditBuyer');


                $param = array(
                    'screditDate' => $screditDate,
                    'screditVolume' => $screditVolume,
                    'screditDK' => $screditDK,
                    'screditBuyer' => $screditBuyer,

                );

                if (empty($screditIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('d_smallholder_credit', $param);
                } else {
                    $key = array('screditId' => $screditIdOld);
                    $proses = $this->{$this->_model_name}->update('d_smallholder_credit', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['screditId' => $keyS];
        $proses = $this->{$this->_model_name}->delete('d_smallholder_credit', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }
}
