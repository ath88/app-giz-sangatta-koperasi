<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class ddecision extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/ddecision/';
        $this->_path_js = null;
        $this->_judul = 'Keputusan Organisasi';
        $this->_controller_name = 'ddecision';
        $this->_model_name = 'model_ddecision';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->all();
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $data['download1_url'] = site_url($this->_controller_name . '/download1') . '/';
        $data['download2_url'] = site_url($this->_controller_name . '/download2') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;
        $data['ref_organization'] = $this->{$this->_model_name}->get_ref_table('ref_organization');

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['doId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->by_id($key);
        $data['ref_organization'] = $this->{$this->_model_name}->get_ref_table('ref_organization');

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $doIdOld = $this->input->post('doIdOld');
        $this->form_validation->set_rules('doOrganizationId', 'Organization', 'trim|xss_clean|required');
        $this->form_validation->set_rules('doDate', 'Tanggal', 'trim|xss_clean|required');
        $this->form_validation->set_rules('doDescription', 'Deskripsi', 'trim|xss_clean|required');
        $this->form_validation->set_rules('doNumber', 'Nomor Keputusan', 'trim|xss_clean|required');
        //$this->form_validation->set_rules('doFile', 'Salinan', 'trim|xss_clean');
        //$this->form_validation->set_rules('doDocumentation', 'Dokumentasi', 'trim|xss_clean');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $doOrganizationId = $this->input->post('doOrganizationId');
                $doDate = $this->input->post('doDate');
                $doDescription = $this->input->post('doDescription');
                $doNumber = $this->input->post('doNumber');
                $doFile = $_FILES['doFile']['name']; 
                $doDocumentation = $_FILES['doDocumentation']['name']; 

                $param = array(
                    'doOrganizationId' => $doOrganizationId,
                    'doNumber' => $doNumber,
                    'doDate' => $doDate,
                    'doDescription' => $doDescription
                );

                if (!empty($doFile)) {
                    $inner_filename = preg_replace("/[^a-zA-Z0-9]/", "_", strtolower(substr(preg_replace("/[^a-zA-Z0-9\s]/", "", $doFile), 0, 100)));
                    $konfig = array(
                        'url' => '../upload_file/',
                        'type' => 'pdf|jpg|jpeg',
                        'size' => '15360',
                        'namafile' => 'keputusan_organisasi_' . $inner_filename . '_' . date("YmdHis")
                    );

                    $this->load->library('UploadArsip');
                    $file_name = $this->uploadarsip->arsip($konfig,'doFile');

                    if (!empty($file_name))
                        $param['doFile'] = $file_name;
                }

                if (!empty($doDocumentation)) {
                    $inner_filename = preg_replace("/[^a-zA-Z0-9]/", "_", strtolower(substr(preg_replace("/[^a-zA-Z0-9\s]/", "", $doDocumentation), 0, 100)));
                    $konfig = array(
                        'url' => '../upload_file/',
                        'type' => 'pdf|jpg|jpeg',
                        'size' => '15360',
                        'namafile' => 'dokumentasi_organisasi_' . $inner_filename . '_' . date("YmdHis")
                    );

                    $this->load->library('UploadArsip');
                    $file_name = $this->uploadarsip->arsip($konfig,'doDocumentation');

                    if (!empty($file_name))
                        $param['doDocumentation'] = $file_name;
                }

                if (empty($doIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('d_decision_organization', $param);
                } else {
                    $key = array('doId' => $doIdOld);
                    $proses = $this->{$this->_model_name}->update('d_decision_organization', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['doId' => $keyS];
        $datas = $this->{$this->_model_name}->get_by_id('d_decision_organization', $key);
        if (file_exists('../upload_file/' . $datas->doFile))
            @unlink('../upload_file/' . $datas->doFile);
        if (file_exists('../upload_file/' . $datas->doDocumentation))
            @unlink('../upload_file/' . $datas->doDocumentation);
        $proses = $this->{$this->_model_name}->delete('d_decision_organization', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }

    public function download1()
    {
        $this->load->helper('download');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['doId' => $keyS];
        $datas = $this->{$this->_model_name}->get_by_id('d_decision_organization', $key);
        force_download('../upload_file/' . $datas->doFile, NULL);
    }

    public function download2()
    {
        $this->load->helper('download');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['doId' => $keyS];
        $datas = $this->{$this->_model_name}->get_by_id('d_decision_organization', $key);
        force_download('../upload_file/' . $datas->doDocumentation, NULL);
    }
}
