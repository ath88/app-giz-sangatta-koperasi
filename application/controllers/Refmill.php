<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class refmill extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/refmill/';
        $this->_path_js = null;
        $this->_judul = 'Pabrik';
        $this->_controller_name = 'refmill';
        $this->_model_name = 'model_refmill';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->get_ref_table('ref_mill');
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['millId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->get_by_id('ref_mill', $key);

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $millIdOld = $this->input->post('millIdOld');
        $this->form_validation->set_rules('millName', 'Nama Perusahaan', 'trim|xss_clean|required');
        $this->form_validation->set_rules('millParent', 'Perusahaan Utama', 'trim|xss_clean|required');
        $this->form_validation->set_rules('millAddress', 'Alamat', 'trim|xss_clean|required');
        $this->form_validation->set_rules('millCoordinate', 'Koordinat', 'trim|xss_clean|required');
        $this->form_validation->set_rules('millCapacity', 'Total Kapasitas', 'trim|xss_clean|required');
        $this->form_validation->set_rules('millUsedCapacity', 'Kapasitas Digunakan', 'trim|xss_clean|required');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $millName = $this->input->post('millName');
                $millParent = $this->input->post('millParent');
                $millAddress = $this->input->post('millAddress');
                $millCoordinate = $this->input->post('millCoordinate');
                $millCapacity = $this->input->post('millCapacity');
                $millUsedCapacity = $this->input->post('millUsedCapacity');


                $param = array(
                    'millName' => $millName,
                    'millParent' => $millParent,
                    'millAddress' => $millAddress,
                    'millCoordinate' => $millCoordinate,
                    'millCapacity' => $millCapacity,
                    'millUsedCapacity' => $millUsedCapacity,

                );

                if (empty($millIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('ref_mill', $param);
                } else {
                    $key = array('millId' => $millIdOld);
                    $proses = $this->{$this->_model_name}->update('ref_mill', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['millId' => $keyS];
        $proses = $this->{$this->_model_name}->delete('ref_mill', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }
}
