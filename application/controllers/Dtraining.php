<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class dtraining extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/dtraining/';
        $this->_path_js = null;
        $this->_judul = 'Pelatihan';
        $this->_controller_name = 'dtraining';
        $this->_model_name = 'model_dtraining';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->all();
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $data['download_url'] = site_url($this->_controller_name . '/download') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['trainId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->get_by_id('d_training', $key);

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $trainIdOld = $this->input->post('trainIdOld');
        $this->form_validation->set_rules('trainDate', 'Tanggal', 'trim|xss_clean|required');
        $this->form_validation->set_rules('trainTopics', 'Topik', 'trim|xss_clean|required');
        //$this->form_validation->set_rules('trainDocument', 'Dokumentasi', 'trim|xss_clean|required');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $trainDate = $this->input->post('trainDate');
                $trainTopics = $this->input->post('trainTopics');
                $trainDocument = $_FILES['trainDocument']['name']; 

                $param = array(
                    'trainDate' => $trainDate,
                    'trainTopics' => $trainTopics,

                );

                if (!empty($trainDocument)) {
                    $inner_filename = preg_replace("/[^a-zA-Z0-9]/", "_", strtolower(substr(preg_replace("/[^a-zA-Z0-9\s]/", "", $trainDocument), 0, 100)));
                    $konfig = array(
                        'url' => '../upload_file/',
                        'type' => 'pdf|jpg|jpeg',
                        'size' => '15360',
                        'namafile' => 'dokumentasi_pelatihan_' . $inner_filename . '_' . date("YmdHis")
                    );

                    $this->load->library('UploadArsip');
                    $file_name = $this->uploadarsip->arsip($konfig,'trainDocument');

                    if (!empty($file_name))
                        $param['trainDocument'] = $file_name;
                }

                if (empty($trainIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('d_training', $param);
                } else {
                    $key = array('trainId' => $trainIdOld);
                    $proses = $this->{$this->_model_name}->update('d_training', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['trainId' => $keyS];
        $datas = $this->{$this->_model_name}->get_by_id('d_training', $key);
        if (file_exists('../upload_file/' . $datas->trainDocument))
            @unlink('../upload_file/' . $datas->trainDocument);
        $proses = $this->{$this->_model_name}->delete('d_training', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }

    public function download()
    {
        $this->load->helper('download');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['trainId' => $keyS];
        $datas = $this->{$this->_model_name}->get_by_id('d_training', $key);
        force_download('../upload_file/' . $datas->trainDocument, NULL);
    }
}
