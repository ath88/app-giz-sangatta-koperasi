<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class dtrainingparticipant extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/dtrainingparticipant/';
        $this->_path_js = null;
        $this->_judul = 'Peserta Pelatihan';
        $this->_controller_name = 'dtrainingparticipant';
        $this->_model_name = 'model_dtrainingparticipant';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->all();
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;
        $data['ref_farmer'] = $this->{$this->_model_name}->get_ref_table('ref_farmer');
        $data['d_training'] = $this->{$this->_model_name}->get_ref_table('d_training');

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['trainpId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->by_id($key);
        $data['ref_farmer'] = $this->{$this->_model_name}->get_ref_table('ref_farmer');
        $data['d_training'] = $this->{$this->_model_name}->get_ref_table('d_training');

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $trainpIdOld = $this->input->post('trainpIdOld');
        $this->form_validation->set_rules('trainpFarmerId', 'Farmer', 'trim|xss_clean');
        $this->form_validation->set_rules('trainpNama', 'Nama Peserta', 'trim|xss_clean');
        $this->form_validation->set_rules('trainpOrganization', 'Organisasi', 'trim|xss_clean');
        $this->form_validation->set_rules('trainpTrainId', 'Training', 'trim|xss_clean|required');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $trainpFarmerId = $this->input->post('trainpFarmerId');
                $trainpTrainId = $this->input->post('trainpTrainId');
                $trainpNama = $this->input->post('trainpNama');
                $trainpOrganization = $this->input->post('trainpOrganization');


                $param = array(
                    'trainpFarmerId' => (empty($trainpFarmerId)?null:$trainpFarmerId),
                    'trainpTrainId' => $trainpTrainId,
                    'trainpNama' => $trainpNama,
                    'trainpOrganization' => $trainpOrganization,

                );

                if (empty($trainpIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('d_training_participant', $param);
                } else {
                    $key = array('trainpId' => $trainpIdOld);
                    $proses = $this->{$this->_model_name}->update('d_training_participant', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['trainpId' => $keyS];
        $proses = $this->{$this->_model_name}->delete('d_training_participant', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }
}
