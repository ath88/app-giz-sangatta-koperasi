<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class dparcelworker extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/dparcelworker/';
        $this->_path_js = null;
        $this->_judul = 'Pekerja Persil';
        $this->_controller_name = 'dparcelworker';
        $this->_model_name = 'model_dparcelworker';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
        $this->load->model('model_dfarmerparcel', '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->all();
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;
        $data['d_farmer_parcel'] = $this->model_dfarmerparcel->all();
        $data['ref_worker'] = $this->{$this->_model_name}->get_ref_table('ref_worker');
        $data['ref_worker_category'] = $this->{$this->_model_name}->get_ref_table('ref_worker_category');
        $data['ref_worker_wage'] = $this->{$this->_model_name}->get_ref_table('ref_worker_wage');

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['parcelwId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->by_id($key);
        $data['d_farmer_parcel'] = $this->model_dfarmerparcel->all();
        $data['ref_worker'] = $this->{$this->_model_name}->get_ref_table('ref_worker');
        $data['ref_worker_category'] = $this->{$this->_model_name}->get_ref_table('ref_worker_category');
        $data['ref_worker_wage'] = $this->{$this->_model_name}->get_ref_table('ref_worker_wage');

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $parcelwIdOld = $this->input->post('parcelwIdOld');
        $this->form_validation->set_rules('parcelwParcelId', 'Parcel', 'trim|xss_clean|required');
        $this->form_validation->set_rules('parcelwWorkerNIK', 'NIK', 'trim|xss_clean|required');
        $this->form_validation->set_rules('parcelwCatId', 'Kategori', 'trim|xss_clean|required');
        $this->form_validation->set_rules('parcelwWageId', 'Jenis Gaji', 'trim|xss_clean|required');
        $this->form_validation->set_rules('parcelwWage', 'Gaji', 'trim|xss_clean|required');
        $this->form_validation->set_rules('parcelwTglAwal', 'TMT Kerja', 'trim|xss_clean|required');
        $this->form_validation->set_rules('parcelwTglStatus', 'TMT Status', 'trim|xss_clean');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $parcelwParcelId = $this->input->post('parcelwParcelId');
                $parcelwWorkerNIK = $this->input->post('parcelwWorkerNIK');
                $parcelwCatId = $this->input->post('parcelwCatId');
                $parcelwWageId = $this->input->post('parcelwWageId');
                $parcelwWage = $this->input->post('parcelwWage');
                $parcelwTglAwal = $this->input->post('parcelwTglAwal');
                $parcelwTglStatus = $this->input->post('parcelwTglStatus');


                $param = array(
                    'parcelwParcelId' => $parcelwParcelId,
                    'parcelwWorkerNIK' => $parcelwWorkerNIK,
                    'parcelwCatId' => $parcelwCatId,
                    'parcelwWageId' => $parcelwWageId,
                    'parcelwWage' => $parcelwWage,
                    'parcelwTglAwal' => $parcelwTglAwal,
                    'parcelwTglStatus' => $parcelwTglStatus,

                );

                if (empty($parcelwIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('d_parcel_worker', $param);
                } else {
                    $key = array('parcelwId' => $parcelwIdOld);
                    $proses = $this->{$this->_model_name}->update('d_parcel_worker', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['parcelwId' => $keyS];
        $proses = $this->{$this->_model_name}->delete('d_parcel_worker', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }
}
