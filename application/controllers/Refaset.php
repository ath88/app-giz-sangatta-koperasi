<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

class refaset extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/refaset/';
        $this->_path_js = null;
        $this->_judul = 'Aset';
        $this->_controller_name = 'refaset';
        $this->_model_name = 'model_refaset';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->get_ref_table('ref_aset');
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $data['excel_url'] = site_url($this->_controller_name . '/excel') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['asetId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->get_by_id('ref_aset', $key);

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $asetIdOld = $this->input->post('asetIdOld');
        $this->form_validation->set_rules('asetNo', 'Nomor Aset', 'trim|xss_clean|required');
        $this->form_validation->set_rules('asetNama', 'Nama Barang', 'trim|xss_clean|required');
        $this->form_validation->set_rules('asetTahun', 'Tahun Perolehan', 'trim|xss_clean|required');
        $this->form_validation->set_rules('asetNilaiAwal', 'Nilai Awal', 'trim|xss_clean');
        $this->form_validation->set_rules('asetNilaiAkhir', 'Nilai Saat ini', 'trim|xss_clean');
        $this->form_validation->set_rules('asetKondisi', 'Kondisi', 'trim|xss_clean|required');
        $this->form_validation->set_rules('asetPIC', 'Pemakai', 'trim|xss_clean|required');
        $this->form_validation->set_rules('asetKet', 'Keterangan', 'trim|xss_clean');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $asetNo = $this->input->post('asetNo');
                $asetNama = $this->input->post('asetNama');
                $asetTahun = $this->input->post('asetTahun');
                $asetNilaiAwal = $this->input->post('asetNilaiAwal');
                $asetNilaiAkhir = $this->input->post('asetNilaiAkhir');
                $asetKondisi = $this->input->post('asetKondisi');
                $asetPIC = $this->input->post('asetPIC');
                $asetKet = $this->input->post('asetKet');


                $param = array(
                    'asetNo' => $asetNo,
                    'asetNama' => $asetNama,
                    'asetTahun' => $asetTahun,
                    'asetNilaiAwal' => $asetNilaiAwal,
                    'asetNilaiAkhir' => $asetNilaiAkhir,
                    'asetKondisi' => $asetKondisi,
                    'asetPIC' => $asetPIC,
                    'asetKet' => $asetKet,

                );

                if (empty($asetIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('ref_aset', $param);
                } else {
                    $key = array('asetId' => $asetIdOld);
                    $proses = $this->{$this->_model_name}->update('ref_aset', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['asetId' => $keyS];
        $proses = $this->{$this->_model_name}->delete('ref_aset', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }

    public function excel()
    {
        $datas = $this->{$this->_model_name}->get_ref_table('ref_aset');
        $objPHPExcel = new Spreadsheet();

        $objPHPExcel->setActiveSheetIndex(0)
            //mengisikan value pada tiap-tiap cell, A1 itu alamat cellnya 
            ->setCellValue('A1', 'No.')
            ->setCellValue('B1', 'Nomor Aset')
            ->setCellValue('C1', 'Nama Barang')
            ->setCellValue('D1', 'Tahun Perolehan')
            ->setCellValue('E1', 'Nilai Awal')
            ->setCellValue('F1', 'Nilai saat ini')
            ->setCellValue('G1', 'Kondisi')
            ->setCellValue('H1', 'Pemakai')
            ->setCellValue('I1', 'Keterangan');

        if ($datas != false) {
            $i = 1;
            foreach ($datas as $row) {
                $objPHPExcel->setActiveSheetIndex(0)
                    //mengisikan value pada tiap-tiap cell, A1 itu alamat cellnya 
                    ->setCellValue('A' . ($i + 1), $i)
                    ->setCellValue('B' . ($i + 1), $row->asetNo)
                    ->setCellValue('C' . ($i + 1), $row->asetNama)
                    ->setCellValue('D' . ($i + 1), $row->asetTahun)
                    ->setCellValue('E' . ($i + 1), $row->asetNilaiAwal)
                    ->setCellValue('F' . ($i + 1), $row->asetNilaiAkhir)
                    ->setCellValue('G' . ($i + 1), $row->asetKondisi)
                    ->setCellValue('H' . ($i + 1), $row->asetPIC)
                    ->setCellValue('I' . ($i + 1), $row->asetKet);
                $i++;
            }
        }

        //set title pada sheet (me rename nama sheet)
        $objPHPExcel->getActiveSheet()->setTitle('ASET');

        //mulai menyimpan excel format xlsx, kalau ingin xls ganti Excel2007 menjadi Excel5          
        $objWriter = IOFactory::createWriter($objPHPExcel, 'Xlsx');

        //sesuaikan headernya 
        ob_end_clean();
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        //ubah nama file saat diunduh
        header('Content-Disposition: attachment;filename="ASET_' . date('YmdHis') . '.xlsx"');
        //unduh file
        $objWriter->save("php://output");
    }
}
