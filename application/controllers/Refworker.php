<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class refworker extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/refworker/';
        $this->_path_js = null;
        $this->_judul = 'Pekerja';
        $this->_controller_name = 'refworker';
        $this->_model_name = 'model_refworker';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->all();
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['ref_wilayah'] = $this->{$this->_model_name}->get_wilayah(2, '64111');
        $data['datas'] = false;

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['workerNIK' => $keyS];
        $data['ref_wilayah'] = $this->{$this->_model_name}->get_wilayah(2, '64111');
        $data['datas'] = $this->{$this->_model_name}->get_by_id('ref_worker', $key);

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $workerNIKOld = $this->input->post('workerNIKOld');
        if (empty($workerNIKOld))
            $this->form_validation->set_rules('workerNIK', 'NIK', 'trim|xss_clean|required|is_unique[ref_worker.workerNIK]');
        else
            $this->form_validation->set_rules('workerNIK', 'NIK', 'trim|xss_clean|required');
        $this->form_validation->set_rules('workerName', 'Nama', 'trim|xss_clean|required');
        $this->form_validation->set_rules('workerAddress', 'Alamat', 'trim|xss_clean|required');
        $this->form_validation->set_rules('workerKotaLahir', 'Kota Lahir', 'trim|xss_clean|required');
        $this->form_validation->set_rules('workerTglLahir', 'Tanggal Lahir', 'trim|xss_clean|required');
        $this->form_validation->set_rules('workerGender', 'Jenis Kelamin', 'trim|xss_clean|required');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $workerNIK = $this->input->post('workerNIK');
                $workerName = $this->input->post('workerName');
                $workerAddress = $this->input->post('workerAddress');
                $workerKotaLahir = $this->input->post('workerKotaLahir');
                $workerTglLahir = $this->input->post('workerTglLahir');
                $workerGender = $this->input->post('workerGender');


                $param = array(
                    'workerNIK' => $workerNIK,
                    'workerName' => $workerName,
                    'workerAddress' => $workerAddress,
                    'workerKotaLahir' => $workerKotaLahir,
                    'workerTglLahir' => $workerTglLahir,
                    'workerGender' => $workerGender,

                );

                if (empty($workerNIKOld)) {
                    $proses = $this->{$this->_model_name}->insert('ref_worker', $param);
                } else {
                    $key = array('workerNIK' => $workerNIKOld);
                    $proses = $this->{$this->_model_name}->update('ref_worker', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['workerNIK' => $keyS];
        $proses = $this->{$this->_model_name}->delete('ref_worker', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }
}
