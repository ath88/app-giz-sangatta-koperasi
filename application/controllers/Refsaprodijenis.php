<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class refsaprodijenis extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/refsaprodijenis/';
        $this->_path_js = null;
        $this->_judul = 'Jenis Saprodi';
        $this->_controller_name = 'refsaprodijenis';
        $this->_model_name = 'model_refsaprodijenis';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->get_ref_table('ref_saprodi_jenis');
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['jsaprodiId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->get_by_id('ref_saprodi_jenis', $key);

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $jsaprodiIdOld = $this->input->post('jsaprodiIdOld');
        $this->form_validation->set_rules('jsaprodiNama', 'Jenis Saprodi', 'trim|xss_clean|required');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $jsaprodiNama = $this->input->post('jsaprodiNama');


                $param = array(
                    'jsaprodiNama' => $jsaprodiNama,

                );

                if (empty($jsaprodiIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('ref_saprodi_jenis', $param);
                } else {
                    $key = array('jsaprodiId' => $jsaprodiIdOld);
                    $proses = $this->{$this->_model_name}->update('ref_saprodi_jenis', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['jsaprodiId' => $keyS];
        $proses = $this->{$this->_model_name}->delete('ref_saprodi_jenis', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }
}
