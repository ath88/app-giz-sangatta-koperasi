<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

class dfarmerparcel extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/dfarmerparcel/';
        $this->_path_js = null;
        $this->_judul = 'Persil Petani';
        $this->_controller_name = 'dfarmerparcel';
        $this->_model_name = 'model_dfarmerparcel';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->all();
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $data['excel_url'] = site_url($this->_controller_name . '/excel') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;
        $data['ref_wilayah'] = $this->{$this->_model_name}->get_wilayah(4,65315);
        $data['ref_landstatus'] = $this->{$this->_model_name}->get_ref_table('ref_landstatus');
        $data['ref_landbeforeplanting'] = $this->{$this->_model_name}->get_ref_table('ref_landbeforeplanting');
        $data['ref_species'] = $this->{$this->_model_name}->get_ref_table('ref_species');
        $data['ref_cropping'] = $this->{$this->_model_name}->get_ref_table('ref_cropping');
        $data['ref_soiltype'] = $this->{$this->_model_name}->get_ref_table('ref_soiltype');
        $data['ref_farmer'] = $this->{$this->_model_name}->get_ref_table('ref_farmer');

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['parcelId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->by_id($key);
        $data['ref_wilayah'] = $this->{$this->_model_name}->get_wilayah(4,65315);
        $data['ref_landstatus'] = $this->{$this->_model_name}->get_ref_table('ref_landstatus');
        $data['ref_landbeforeplanting'] = $this->{$this->_model_name}->get_ref_table('ref_landbeforeplanting');
        $data['ref_species'] = $this->{$this->_model_name}->get_ref_table('ref_species');
        $data['ref_cropping'] = $this->{$this->_model_name}->get_ref_table('ref_cropping');
        $data['ref_soiltype'] = $this->{$this->_model_name}->get_ref_table('ref_soiltype');
        $data['ref_farmer'] = $this->{$this->_model_name}->get_ref_table('ref_farmer');

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $parcelIdOld = $this->input->post('parcelIdOld');
        $this->form_validation->set_rules('parcelVillage', 'Desa', 'trim|xss_clean|required');
        $this->form_validation->set_rules('parcelLsId', 'Status tanah', 'trim|xss_clean|required');
        $this->form_validation->set_rules('parcelLbpId', 'Area sebelum tanam', 'trim|xss_clean|required');
        $this->form_validation->set_rules('parcelSpecId', 'Jenis species', 'trim|xss_clean|required');
        $this->form_validation->set_rules('parcelCropId', 'Model cropping', 'trim|xss_clean|required');
        $this->form_validation->set_rules('parcelStId', 'Tipe tanah', 'trim|xss_clean|required');
        $this->form_validation->set_rules('parcelFarmerId', 'Petani', 'trim|xss_clean|required');
        $this->form_validation->set_rules('parcelNumberXY', 'NumberXY', 'trim|xss_clean');
        $this->form_validation->set_rules('parcelGeoLang', 'Geo-Langitude', 'trim|xss_clean');
        $this->form_validation->set_rules('parcelGeoLat', 'Geo-Latitude', 'trim|xss_clean');
        $this->form_validation->set_rules('parcelAddress', 'Alamat', 'trim|xss_clean|required');
        $this->form_validation->set_rules('parcelTotalArea', 'Total area', 'trim|xss_clean|required');
        $this->form_validation->set_rules('parcelPlantedArea', 'Total area tanaman', 'trim|xss_clean|required');
        $this->form_validation->set_rules('parcelPlatingYear', 'Tahun tanam', 'trim|xss_clean|required');
        $this->form_validation->set_rules('parcelProductivity', 'Jumlah produksi', 'trim|xss_clean|required');
        $this->form_validation->set_rules('parcelOriginMaterial', 'Produsen benih', 'trim|xss_clean|required');
        $this->form_validation->set_rules('parcelTree', 'Jumlah pohon/tanaman', 'trim|xss_clean|required');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $parcelVillage = $this->input->post('parcelVillage');
                $parcelLsId = $this->input->post('parcelLsId');
                $parcelLbpId = $this->input->post('parcelLbpId');
                $parcelSpecId = $this->input->post('parcelSpecId');
                $parcelCropId = $this->input->post('parcelCropId');
                $parcelStId = $this->input->post('parcelStId');
                $parcelFarmerId = $this->input->post('parcelFarmerId');
                $parcelNumberXY = $this->input->post('parcelNumberXY');
                $parcelGeoLang = $this->input->post('parcelGeoLang');
                $parcelGeoLat = $this->input->post('parcelGeoLat');
                $parcelAddress = $this->input->post('parcelAddress');
                $parcelTotalArea = $this->input->post('parcelTotalArea');
                $parcelPlantedArea = $this->input->post('parcelPlantedArea');
                $parcelPlatingYear = $this->input->post('parcelPlatingYear');
                $parcelProductivity = $this->input->post('parcelProductivity');
                $parcelOriginMaterial = $this->input->post('parcelOriginMaterial');
                $parcelTree = $this->input->post('parcelTree');
                $parcelOtherPangan = $this->input->post('parcelOtherPangan');
                $parcelOtherSawit = $this->input->post('parcelOtherSawit');
                $parcelOtherKaret = $this->input->post('parcelOtherKaret');
                $parcelOtherKakao = $this->input->post('parcelOtherKakao');
                $parcelOtherLada = $this->input->post('parcelOtherLada');
                $parcelOtherKelapa = $this->input->post('parcelOtherKelapa');
                $parcelOtherAren = $this->input->post('parcelOtherAren');
                $parcelOtherOther = $this->input->post('parcelOtherOther');


                $param = array(
                    'parcelVillage' => $parcelVillage,
                    'parcelLsId' => $parcelLsId,
                    'parcelLbpId' => $parcelLbpId,
                    'parcelSpecId' => $parcelSpecId,
                    'parcelCropId' => $parcelCropId,
                    'parcelStId' => $parcelStId,
                    'parcelFarmerId' => $parcelFarmerId,
                    'parcelNumberXY' => $parcelNumberXY,
                    'parcelGeoLang' => $parcelGeoLang,
                    'parcelGeoLat' => $parcelGeoLat,
                    'parcelAddress' => $parcelAddress,
                    'parcelTotalArea' => $parcelTotalArea,
                    'parcelPlantedArea' => $parcelPlantedArea,
                    'parcelPlatingYear' => $parcelPlatingYear,
                    'parcelProductivity' => $parcelProductivity,
                    'parcelOriginMaterial' => $parcelOriginMaterial,
                    'parcelTree' => $parcelTree,
                    'parcelOtherPangan' => $parcelOtherPangan,
                    'parcelOtherSawit' => $parcelOtherSawit,
                    'parcelOtherKaret' => $parcelOtherKaret,
                    'parcelOtherKakao' => $parcelOtherKakao,
                    'parcelOtherLada' => $parcelOtherLada,
                    'parcelOtherKelapa' => $parcelOtherKelapa,
                    'parcelOtherAren' => $parcelOtherAren,
                    'parcelOtherOther' => $parcelOtherOther,

                );

                if (empty($parcelIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('d_farmer_parcel', $param);
                } else {
                    $key = array('parcelId' => $parcelIdOld);
                    $proses = $this->{$this->_model_name}->update('d_farmer_parcel', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['parcelId' => $keyS];
        $proses = $this->{$this->_model_name}->delete('d_farmer_parcel', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }

    public function excel()
    {
        $datas = $this->{$this->_model_name}->all();
        $objPHPExcel = new Spreadsheet();

        $objPHPExcel->setActiveSheetIndex(0)
            //mengisikan value pada tiap-tiap cell, A1 itu alamat cellnya 
            ->setCellValue('A1', 'No.')
            ->setCellValue('B1', 'NIK')
            ->setCellValue('C1', 'Nama')
            ->setCellValue('D1', 'XY')
            ->setCellValue('E1', 'Latitude')
            ->setCellValue('F1', 'Langitude')
            ->setCellValue('G1', 'Alamat')
            ->setCellValue('H1', 'Desa')
            ->setCellValue('I1', 'Status Kepemilikan')
            ->setCellValue('J1', 'Total Area')
            ->setCellValue('K1', 'Total Area Tanam')
            ->setCellValue('L1', 'Tahun Tanam')
            ->setCellValue('M1', 'Tutupan Lahan')
            ->setCellValue('N1', 'Jenis Tanaman Utama')
            ->setCellValue('O1', 'Jumlah Produksi')
            ->setCellValue('P1', 'Produsen Benih')
            ->setCellValue('Q1', 'Jumlah Pohon/Tanaman')
            ->setCellValue('R1', 'Model Tanam')
            ->setCellValue('S1', 'Tanaman Lainnya (Pangan)')
            ->setCellValue('T1', 'Tanaman Lainnya (Sawit)')
            ->setCellValue('U1', 'Tanaman Lainnya (Karet)')
            ->setCellValue('V1', 'Tanaman Lainnya (Kakao)')
            ->setCellValue('W1', 'Tanaman Lainnya (Lada)')
            ->setCellValue('X1', 'Tanaman Lainnya (Kelapa)')
            ->setCellValue('Y1', 'Tanaman Lainnya (Aren)')
            ->setCellValue('Z1', 'Tanaman Lainnya (Lainnya)');

        if ($datas != false) {
            $i = 1;
            foreach ($datas as $row) {
                $birthDate = new DateTime($row->farmerDateBirth);
                $objPHPExcel->setActiveSheetIndex(0)
                    //mengisikan value pada tiap-tiap cell, A1 itu alamat cellnya 
                    ->setCellValue('A' . ($i + 1), $i)
                    ->setCellValue('B' . ($i + 1), $row->farmerNIK)
                    ->setCellValue('C' . ($i + 1), $row->farmerName)
                    ->setCellValue('D' . ($i + 1), $row->parcelNumberXY)
                    ->setCellValue('E' . ($i + 1), $row->parcelGeoLang)
                    ->setCellValue('F' . ($i + 1), $row->parcelGeoLat)
                    ->setCellValue('G' . ($i + 1), $row->parcelAddress)
                    ->setCellValue('H' . ($i + 1), $row->NAMA_WILAYAH)
                    ->setCellValue('I' . ($i + 1), $row->lsName)
                    ->setCellValue('J' . ($i + 1), $row->parcelTotalArea)
                    ->setCellValue('K' . ($i + 1), $row->parcelPlantedArea)
                    ->setCellValue('L' . ($i + 1), $row->parcelPlatingYear)
                    ->setCellValue('M' . ($i + 1), $row->lbpName)
                    ->setCellValue('N' . ($i + 1), $row->specName)
                    ->setCellValue('O' . ($i + 1), $row->parcelProductivity)
                    ->setCellValue('P' . ($i + 1), $row->parcelOriginMaterial)
                    ->setCellValue('Q' . ($i + 1), $row->parcelTree)
                    ->setCellValue('R' . ($i + 1), $row->cropName)
                    ->setCellValue('S' . ($i + 1), $row->parcelOtherPangan)
                    ->setCellValue('T' . ($i + 1), $row->parcelOtherSawit)
                    ->setCellValue('U' . ($i + 1), $row->parcelOtherKaret)
                    ->setCellValue('V' . ($i + 1), $row->parcelOtherKakao)
                    ->setCellValue('W' . ($i + 1), $row->parcelOtherLada)
                    ->setCellValue('X' . ($i + 1), $row->parcelOtherKelapa)
                    ->setCellValue('Y' . ($i + 1), $row->parcelOtherAren)
                    ->setCellValue('Z' . ($i + 1), $row->parcelOtherOther);
                $i++;
            }
        }

        //set title pada sheet (me rename nama sheet)
        $objPHPExcel->getActiveSheet()->setTitle('PARCEL');

        //mulai menyimpan excel format xlsx, kalau ingin xls ganti Excel2007 menjadi Excel5          
        $objWriter = IOFactory::createWriter($objPHPExcel, 'Xlsx');

        //sesuaikan headernya 
        ob_end_clean();
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        //ubah nama file saat diunduh
        header('Content-Disposition: attachment;filename="FARMER_PARCEL_' . date('YmdHis') . '.xlsx"');
        //unduh file
        $objWriter->save("php://output");
    }
}
