<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class refprovinsi extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/refprovinsi/';
        $this->_path_js = null;
        $this->_judul = 'Provinsi';
        $this->_controller_name = 'refprovinsi';
        $this->_model_name = 'model_refprovinsi';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->get_ref_table('ref_provinsi');
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['provId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->get_by_id('ref_provinsi', $key);

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $provIdOld = $this->input->post('provIdOld');
        $this->form_validation->set_rules('provNama', 'Provinsi', 'trim|xss_clean|required');
        $this->form_validation->set_rules('provIsIbuNegara', 'Ibu Negara ?', 'trim|xss_clean|required');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $provNama = $this->input->post('provNama');
                $provIsIbuNegara = $this->input->post('provIsIbuNegara');


                $param = array(
                    'provNama' => $provNama,
                    'provIsIbuNegara' => $provIsIbuNegara,

                );

                if (empty($provIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('ref_provinsi', $param);
                } else {
                    $key = array('provId' => $provIdOld);
                    $proses = $this->{$this->_model_name}->update('ref_provinsi', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['provId' => $keyS];
        $proses = $this->{$this->_model_name}->delete('ref_provinsi', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }
}
