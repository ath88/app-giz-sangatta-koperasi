<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class dharvestsale extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/dharvestsale/';
        $this->_path_js = 'transaksi';
        $this->_judul = 'Penjualan TBS dan Transportasi';
        $this->_controller_name = 'dharvestsale';
        $this->_model_name = 'model_dharvestsale';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
        $this->load->model('model_dharvest', '', TRUE);
        $this->load->model('model_dcontract', '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->all();
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $this->load->view($this->_template, $data);
    }

    public function getpanen()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $id = $this->uri->segment(3);
        $datas = $this->{$this->_model_name}->get_by_id('d_harvest', ['harvestId' => $id]);
        echo json_encode($datas);
    }

    public function getharga()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $id = $this->uri->segment(3);
        $datas = $this->{$this->_model_name}->get_by_id('d_contract', ['contId' => $id]);
        echo json_encode($datas);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [$this->_path_js . '/' . $this->_controller_name];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;
        $data['d_harvest'] = $this->model_dharvest->all();
        $data['ref_truck'] = $this->{$this->_model_name}->get_ref_table('ref_truck');
        $data['d_contract'] = $this->model_dcontract->all();

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [$this->_path_js . '/' . $this->_controller_name];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['hsaleId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->by_id($key);
        $data['d_harvest'] = $this->model_dharvest->all();
        $data['ref_truck'] = $this->{$this->_model_name}->get_ref_table('ref_truck');
        $data['d_contract'] = $this->model_dcontract->all();

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $hsaleIdOld = $this->input->post('hsaleIdOld');
        $this->form_validation->set_rules('hsaleHarvestId', 'Panen', 'trim|xss_clean|required');
        $this->form_validation->set_rules('hsaleTruckId', 'Transport', 'trim|xss_clean|required');
        $this->form_validation->set_rules('hsaleContId', 'Kontrak', 'trim|xss_clean|required');
        $this->form_validation->set_rules('hsaleTransport', 'Biaya Transport', 'trim|xss_clean|required');
        $this->form_validation->set_rules('hsaleTJJG', 'Timbang JJG', 'trim|xss_clean|required');
        $this->form_validation->set_rules('hsaleTBrondol', 'Timbang Brondol', 'trim|xss_clean|required');
        $this->form_validation->set_rules('hsaleTSJJG', 'Selisih JJG', 'trim|xss_clean|required');
        $this->form_validation->set_rules('hsaleTSBrondol', 'Selisih Brondol', 'trim|xss_clean|required');
        $this->form_validation->set_rules('hsaleNJJG', 'Netto JJG', 'trim|xss_clean|required');
        $this->form_validation->set_rules('hsaleNBrondol', 'Netto Brondol', 'trim|xss_clean|required');
        $this->form_validation->set_rules('hsaleDate', 'Tangga Jual', 'trim|xss_clean|required');
        $this->form_validation->set_rules('hsaleFile', 'File', 'trim|xss_clean');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $hsaleHarvestId = $this->input->post('hsaleHarvestId');
                $hsaleTruckId = $this->input->post('hsaleTruckId');
                $hsaleContId = $this->input->post('hsaleContId');
                $hsaleTransport = $this->input->post('hsaleTransport');
                $hsaleTJJG = $this->input->post('hsaleTJJG');
                $hsaleTBrondol = $this->input->post('hsaleTBrondol');
                $hsaleTSJJG = $this->input->post('hsaleTSJJG');
                $hsaleTSBrondol = $this->input->post('hsaleTSBrondol');
                $hsaleNJJG = $this->input->post('hsaleNJJG');
                $hsaleNBrondol = $this->input->post('hsaleNBrondol');
                $hsaleFile = $this->input->post('hsaleFile');
                $hsaleDate = $this->input->post('hsaleDate');


                $param = array(
                    'hsaleHarvestId' => $hsaleHarvestId,
                    'hsaleTruckId' => $hsaleTruckId,
                    'hsaleContId' => $hsaleContId,
                    'hsaleTransport' => $hsaleTransport,
                    'hsaleTJJG' => $hsaleTJJG,
                    'hsaleTBrondol' => $hsaleTBrondol,
                    'hsaleTSJJG' => $hsaleTSJJG,
                    'hsaleTSBrondol' => $hsaleTSBrondol,
                    'hsaleNJJG' => $hsaleNJJG,
                    'hsaleNBrondol' => $hsaleNBrondol,
                    'hsaleFile' => $hsaleFile,
                    'hsaleDate' => $hsaleDate,

                );

                if (empty($hsaleIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('d_harvest_sale', $param);
                } else {
                    $key = array('hsaleId' => $hsaleIdOld);
                    $proses = $this->{$this->_model_name}->update('d_harvest_sale', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['hsaleId' => $keyS];
        $proses = $this->{$this->_model_name}->delete('d_harvest_sale', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }
}
