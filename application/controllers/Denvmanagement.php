<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class denvmanagement extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/denvmanagement/';
        $this->_path_js = null;
        $this->_judul = 'Pengelolaan Lingkungan';
        $this->_controller_name = 'denvmanagement';
        $this->_model_name = 'model_denvmanagement';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
        $this->load->model('model_dfarmerparcel', '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->all();
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $data['download_url'] = site_url($this->_controller_name . '/download') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;
        $data['d_farmer_parcel'] = $this->model_dfarmerparcel->all();

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['envmId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->by_id($key);
        $data['d_farmer_parcel'] = $this->model_dfarmerparcel->all();

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $envmIdOld = $this->input->post('envmIdOld');
        $this->form_validation->set_rules('envmParcelId', 'Parcel', 'trim|xss_clean|required');
        //$this->form_validation->set_rules('envmFile', 'File SPPL', 'trim|xss_clean|required');
        $this->form_validation->set_rules('envmDate', 'Tanggal', 'trim|xss_clean|required');
        $this->form_validation->set_rules('envmActivity', 'Aktivitas', 'trim|xss_clean|required');
        $this->form_validation->set_rules('envmLocLang', 'Langitude', 'trim|xss_clean');
        $this->form_validation->set_rules('envmLocLat', 'Latitude', 'trim|xss_clean');
        $this->form_validation->set_rules('envmOrganization', 'envmOrganization', 'trim|xss_clean|required');
        $this->form_validation->set_rules('envmIsFirePrevention', 'Khusus pencegahan api ?', 'trim|xss_clean|required');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $envmParcelId = $this->input->post('envmParcelId');
                $envmFile = $_FILES['envmFile']['name']; 
                $envmDate = $this->input->post('envmDate');
                $envmActivity = $this->input->post('envmActivity');
                $envmLocLang = $this->input->post('envmLocLang');
                $envmLocLat = $this->input->post('envmLocLat');
                $envmOrganization = $this->input->post('envmOrganization');
                $envmIsFirePrevention = $this->input->post('envmIsFirePrevention');


                $param = array(
                    'envmParcelId' => $envmParcelId,
                    'envmDate' => $envmDate,
                    'envmActivity' => $envmActivity,
                    'envmLocLang' => $envmLocLang,
                    'envmLocLat' => $envmLocLat,
                    'envmOrganization' => $envmOrganization,
                    'envmIsFirePrevention' => $envmIsFirePrevention,
                );

                if (!empty($envmFile)) {
                    $inner_filename = preg_replace("/[^a-zA-Z0-9]/", "_", strtolower(substr(preg_replace("/[^a-zA-Z0-9\s]/", "", $envmFile), 0, 100)));
                    $konfig = array(
                        'url' => '../upload_file/',
                        'type' => 'pdf|jpg|jpeg',
                        'size' => '15360',
                        'namafile' => 'environment_' . $inner_filename . '_' . date("YmdHis")
                    );

                    $this->load->library('UploadArsip');
                    $file_name = $this->uploadarsip->arsip($konfig,'envmFile');

                    if (!empty($file_name))
                        $param['envmFile'] = $file_name;
                }

                if (empty($envmIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('d_envmanagement', $param);
                } else {
                    $key = array('envmId' => $envmIdOld);
                    $proses = $this->{$this->_model_name}->update('d_envmanagement', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['envmId' => $keyS];
        $datas = $this->{$this->_model_name}->get_by_id('d_envmanagement', $key);
        if (file_exists('../upload_file/' . $datas->envmFile))
            @unlink('../upload_file/' . $datas->envmFile);
        $proses = $this->{$this->_model_name}->delete('d_envmanagement', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }

    public function download()
    {
        $this->load->helper('download');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['envmId' => $keyS];
        $datas = $this->{$this->_model_name}->get_by_id('d_envmanagement', $key);
        force_download('../upload_file/' . $datas->envmFile, NULL);
    }
}
