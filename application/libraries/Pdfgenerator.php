<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

use Dompdf\Dompdf;

class PdfGenerator
{
  public function generate($html, $filename, $orientation, $size = 'Legal')
  {
    ini_set('max_execution_time', 0);
    ini_set('memory_limit', '256M');
    $dompdf = new Dompdf();
    $dompdf->loadHtml($html);
    $dompdf->setPaper($size, $orientation);
    $dompdf->render();
    $dompdf->stream($filename . '.pdf', array("Attachment" => 0));
  }

  public function mpdf_generate($html, $filename, $orientation, $size = 'Legal', $footer = '')
  {
    ini_set("pcre.backtrack_limit", "5000000");
    ini_set('max_execution_time', 0);
    ini_set('memory_limit', '406M');

    $tempDir = FCPATH . '../vendor/mpdf/mpdf/tmp';
    if (!empty($orientation))
      $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => $size, 'orientation' => 'L', 'tempDir' =>  $tempDir]);
    else
      $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => $size, 'tempDir' =>  $tempDir]);

    if (!empty($footer))
      $mpdf->SetHTMLFooter($footer);
    $mpdf->simpleTables = true;
    $mpdf->packTableData = true;
    $mpdf->shrink_tables_to_fit = 1;
    $mpdf->WriteHTML($html);
    $mpdf->Output($filename . '.pdf', 'I');
  }
}
