"use strict";

// Class Definition
var FormCustom = function () {
    var handleSubmit = function (form) {
        $('#response').html('');
        var button = $('#btn_save');
        var button_text = button.text();
        button.prop("disabled", true);
        button.addClass('disabled');
        button.text('Sedang Memproses...');
        $.ajax({
            type: $(form).attr('method'),
            url: $(form).attr('action'),
            data: $(form).serialize(),
            success: function (data) {
                try {
                    var res = $.parseJSON(data);
                    $('#response').fadeIn('slow').html(res.response);
                    swal.fire({
                        position: "top-right",
                        type: res.status,
                        title: res.message,
                        showConfirmButton: !1,
                        timer: 1500
                    });
                } catch (err) {
                    $('#response').fadeIn('slow').html(data);
                }
                button.prop("disabled", false);
                button.removeClass('disabled');
                button.text(button_text);
                FormCustom.init();
            }
        })
    }

    var handleSubmitFormShow = function () {
        $("#form_show").validate({
            rules: {
                farmerId: {
                    required: true
                }
            },
            submitHandler: function (e) {
                handleSubmit(e);
                return false
            }
        });
    }

    var hitung = function() {
        var tsaleDelVolume = $("#tsaleDelVolume").val();
        var tsaleStaVolume = $("#tsaleStaVolume").val();
        var tsaleHarga = $("#tsaleHarga").val();

        var tsaleBruto = tsaleStaVolume * tsaleHarga;
        var tsalePotAdministrasi = tsaleStaVolume;
        var tsalePot025 = tsaleBruto * (0.25 / 100);
        var tsalePotTranport = tsaleDelVolume * 105;
        var tsalePotKoperasi = tsaleDelVolume * 30;
        var tsalePotTimbang = tsaleDelVolume * 20;
        var tsaleZakat = (tsaleBruto - tsalePotAdministrasi - tsalePot025 - tsalePotTranport - tsalePotKoperasi - tsalePotTimbang) * (2.5 / 100);
        var tsaleNetto = tsaleBruto - tsalePotAdministrasi - tsalePot025 - tsalePotTranport - tsalePotKoperasi - tsalePotTimbang - tsaleZakat;

        $("#tsaleBruto").val(tsaleBruto);
        $("#tsalePot025").val(tsalePot025);
        $("#tsalePotTranport").val(tsalePotTranport);
        $("#tsalePotKoperasi").val(tsalePotKoperasi);
        $("#tsalePotAdministrasi").val(tsalePotAdministrasi);
        $("#tsalePotTimbang").val(tsalePotTimbang);
        $("#tsaleZakat").val(tsaleZakat);
        $("#tsaleNetto").val(tsaleNetto);
    }

    var hitung_netto = function() {
        var tsalePotTimbang = $("#tsalePotTimbang").val();
        var tsaleZakat = $("#tsaleZakat").val();
        
        var tsaleBruto = $("#tsaleBruto").val();
        var tsalePot025 = $("#tsalePot025").val();
        var tsalePotTranport = $("#tsalePotTranport").val();
        var tsalePotKoperasi = $("#tsalePotKoperasi").val();
        var tsalePotAdministrasi = $("#tsalePotAdministrasi").val();

        var tsaleNetto = tsaleBruto - tsalePotAdministrasi - tsalePot025 - tsalePotTranport - tsalePotKoperasi - tsalePotTimbang - tsaleZakat;
        $("#tsaleNetto").val(tsaleNetto);
    }

    var changeDataHitung = function () {
        $("#tsaleDelVolume").change(function (e) {
            hitung();
        });
        $("#tsaleStaVolume").change(function (e) {
            hitung();
        });
        $("#tsaleHarga").change(function (e) {
            hitung();
        });
        $("#tsalePotTimbang").change(function (e) {
            hitung_netto();
        });
        $("#tsaleZakat").change(function (e) {
            hitung_netto();
        });
    }

    var keyupDataHitung = function () {
        $("#tsaleDelVolume").keyup(function (e) {
            hitung();
        });
        $("#tsaleStaVolume").keyup(function (e) {
            hitung();
        });
        $("#tsaleHarga").keyup(function (e) {
            hitung();
        });
        $("#tsalePotTimbang").keyup(function (e) {
            hitung_netto();
        });
        $("#tsaleZakat").keyup(function (e) {
            hitung_netto();
        });
    }

    var changeKontrak = function () {
        $("#tsaleContractId").change(function (e) {
            $('#tsaleHarga').val('');
            var id = $("#tsaleContractId").val();
            $.ajax({
                type: 'post',
                url: '/dtransportsale/loadharga/',
                data: 'id=' + id,
                success: function (data) {
                    try {
                        var res = $.parseJSON(data);
                        $('#tsaleHarga').val(res.contPrice);
                    } catch (err) {
                        $('#response').fadeIn('slow').html(data);
                    }
                }
            });
        });

        $("#tsaleContractId").click(function (e) {
            $('#tsaleHarga').val('');
            var id = $("#tsaleContractId").val();
            $.ajax({
                type: 'post',
                url: '/dtransportsale/loadharga/',
                data: 'id=' + id,
                success: function (data) {
                    try {
                        var res = $.parseJSON(data);
                        $('#tsaleHarga').val(res.contPrice);
                    } catch (err) {
                        $('#response').fadeIn('slow').html(data);
                    }
                }
            });
        });
    }

    return {
        // public functions
        init: function () {
            handleSubmitFormShow();
        }
    };
}();

jQuery(document).ready(function () {
    FormCustom.init()
});