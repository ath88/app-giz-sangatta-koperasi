"use strict";

// Class Definition
var FormCustom = function () {

    var handleClickDelete = function () {
        $(".ts_remove_row").click(function (e) {
            e.preventDefault();
            var idLink = '#' + $(this).attr('id');

            swal.fire({
                title: "Apakah Anda Yakin Akan Hapus Data?",
                text: "Data Tidak Dapat Dikembalikan!!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes, Hapus!"
            }).then(function (e) {
                e.value &&
                    $.ajax({
                        url: $(idLink).attr('href'),
                        success: function (data) {
                            var res = $.parseJSON(data);
                            $('#response').fadeIn('slow').html(res.response);
                            swal.fire({
                                title: "Deleted!",
                                text: res.message,
                                type: res.status
                            }).then(
                                function () {
                                    location.reload();
                                }
                            );
                        }
                    });
            })
        });
    }

    var handleSubmit = function (form) {
        $('#response').html('');
        var button = $('#btn_save');
        var button_text = button.text();
        button.prop("disabled", true);
        button.addClass('disabled');
        button.text('Sedang Memproses...');
        $.ajax({
            type: $(form).attr('method'),
            url: $(form).attr('action'),
            data: $(form).serialize(),
            success: function (data) {
                try {
                    var res = $.parseJSON(data);
                    $('#response').fadeIn('slow').html(res.response);
                    swal.fire({
                        position: "top-right",
                        type: res.status,
                        title: res.message,
                        showConfirmButton: !1,
                        timer: 1500
                    });
                } catch (err) {
                    $('#response').fadeIn('slow').html(data);
                }
                button.prop("disabled", false);
                button.removeClass('disabled');
                button.text(button_text);
                FormCustom.init();
            }
        })
    }

    var handleSubmitFormShow = function () {
        $("#form_show").validate({
            rules: {
                farmerId: {
                    required: true
                }
            },
            submitHandler: function (e) {
                handleSubmit(e);
                return false
            }
        });
    }

    var pilih = function () {
        $(".m-select2").select2({
            placeholder: "Pilih...",
            allowClear: !0
        });
    }

    var handleHitung = function () {
        var hsaleTJJG = $("#hsaleTJJG").val();
        var hsaleTBrondol = $("#hsaleTBrondol").val();

        var total_timbang = parseFloat(hsaleTJJG) + parseFloat(hsaleTBrondol);
        $("#total_timbang").val(total_timbang.toString());

        var hsaleTSJJG = $("#hsaleTSJJG").val();
        var hsaleTSBrondol = $("#hsaleTSBrondol").val();

        var total_selisih = parseFloat(hsaleTSJJG) + parseFloat(hsaleTSBrondol);
        $("#total_selisih").val(total_selisih.toString());

        var hsaleNJJG = $("#hsaleNJJG").val();
        var hsaleNBrondol = $("#hsaleNBrondol").val();

        var total_netto = parseFloat(hsaleNJJG) + parseFloat(hsaleNBrondol);
        $("#total_netto").val(total_netto.toString());
        
        var harga_kontrak = $("#harga_kontrak").val();
        var hasil_penjualan = total_netto * parseFloat(harga_kontrak);
        $("#hasil_penjualan").val(hasil_penjualan.toString());
    }

    var handleGetValue = function () {
        $("#hsaleHarvestId").change(function (e) {
            var saprodisHsaprodiId = $("#hsaleHarvestId").val();
            $.ajax({
                url: '/dharvestsale/getpanen/' + saprodisHsaprodiId,
                success: function (data) {
                    var res = $.parseJSON(data);
                    $("#panen_jjg").val(res.harvestJJG);
                    $("#panen_brondol").val(res.harvestBrondol);
                    var total_panen = parseFloat(res.harvestJJG) + parseFloat(res.harvestBrondol);
                    $("#total_panen").val(total_panen.toString());
                }
            });
        });

        $("#hsaleContId").change(function (e) {
            var hsaleContId = $("#hsaleContId").val();
            $.ajax({
                url: '/dharvestsale/getharga/' + hsaleContId,
                success: function (data) {
                    var res = $.parseJSON(data);
                    $("#harga_kontrak").val(res.contPrice);
                }
            });
        });

        $("#hsaleTJJG,#hsaleTBrondol,#hsaleTSJJG,#hsaleTSBrondol,#hsaleNJJG,#hsaleNBrondol").each(function () {
            $(this).change(function (e) {
                handleHitung();
            });
            $(this).keyup(function (e) {
                handleHitung();
            });
        });
    }

    return {
        // public functions
        init: function () {
            handleSubmitFormShow();
            handleClickDelete();
            pilih();
            handleGetValue();
        }
    };
}();

jQuery(document).ready(function () {
    FormCustom.init()
});