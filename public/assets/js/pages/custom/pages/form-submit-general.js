"use strict";

// Class Definition
var FormGeneral = function () {

    var handleClickDelete = function () {
        $(".ts_remove_row").click(function (e) {
            e.preventDefault();
            var idLink = '#' + $(this).attr('id');

            swal.fire({
                title: "Apakah Anda Yakin Akan Hapus Data?",
                text: "Data Tidak Dapat Dikembalikan!!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes, Hapus!"
            }).then(function (e) {
                e.value &&
                    $.ajax({
                        url: $(idLink).attr('href'),
                        success: function (data) {
                            var res = $.parseJSON(data);
                            $('#response').fadeIn('slow').html(res.response);
                            swal.fire({
                                title: "Deleted!",
                                text: res.message,
                                type: res.status
                            }).then(
                                function () {
                                    location.reload();
                                }
                            );
                        }
                    });
            })
        });
    }

    var handleSubmit = function (form) {
        $('#response').html('');
        var button = $('#btn_save');
        var button_text = button.text();
        button.prop("disabled", true);
        button.addClass('disabled');
        button.text('Sedang Memproses...');
        $.ajax({
            type: $(form).attr('method'),
            url: $(form).attr('action'),
            data: $(form).serialize(),
            success: function (data) {
                try {
                    var res = $.parseJSON(data);
                    $('#response').fadeIn('slow').html(res.response);
                    swal.fire({
                        position: "top-right",
                        type: res.status,
                        title: res.message,
                        showConfirmButton: !1,
                        timer: 1500
                    });
                } catch (err) {
                    $('#response').fadeIn('slow').html(data);
                }


                button.prop("disabled", false);
                button.removeClass('disabled');
                button.text(button_text);
            }
        })
    }

    var handleSubmitForm = function () {
        $("#form_form").validate({
            rules: {

            },
            submitHandler: function (e) {
                $('#response').html('');
                var button = $('#btn_save');
                var button_text = button.text();
                button.prop("disabled", true);
                button.addClass('disabled');
                button.text('Sedang Memproses...');
                var forms = $("#form_form")[0];
                var form_data = new FormData(forms);
                $.ajax({
                    type: $("#form_form").attr('method'),
                    url: $("#form_form").attr('action'),
                    data: form_data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        try {
                            var res = $.parseJSON(data);
                            $('#response').fadeIn('slow').html(res.response);
                            swal.fire({
                                position: "top-right",
                                type: res.status,
                                title: res.message,
                                showConfirmButton: !1,
                                timer: 1500
                            });
                        } catch (err) {
                            $('#response').fadeIn('slow').html(data);
                        }


                        button.prop("disabled", false);
                        button.removeClass('disabled');
                        button.text(button_text);
                    }
                })
                return false
            }
        });
    }

    var handleSubmitFile = function () {
        $("#form_form").validate({
            rules: {

            },
            submitHandler: function (e) {
                handleSubmit(e);
                return false
            }
        });
    }

    var datePicker = function () {
        var arrows;
        if (KTUtil.isRTL()) {
            arrows = {
                leftArrow: '<i class="la la-angle-right"></i>',
                rightArrow: '<i class="la la-angle-left"></i>'
            }
        } else {
            arrows = {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        }
        $('#kt_datepicker_1,#kt_datepicker_2,#kt_datepicker_3,#kt_datepicker_4').datepicker({
            format: "yyyy-mm-dd",
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            autoclose: true,
            orientation: "bottom left",
            templates: arrows
        });
    }

    return {
        // public functions
        init: function () {
            handleSubmitForm();
            handleClickDelete();
            datePicker();
        }
    };
}();

jQuery(document).ready(function () {
    FormGeneral.init()
});